var gulp = require('gulp'),
  minifycss = require('gulp-minify-css'),
  rename = require('gulp-rename'),
  concat = require('gulp-concat'),
  compass = require('gulp-compass'),
  watch = require('gulp-watch');

gulp.task('styles', function() {
  return gulp.src('www/css/*.css')
    .pipe(concat('main.css'))
    .pipe(rename({suffix: '.min'}))
    .pipe(minifycss())
    .pipe(gulp.dest('www/css'));
});

gulp.task('cs', function() {
  gulp.src('app/assets/sass/*.scss')
    .pipe(compass({
      config_file: './app/assets/config.rb',
      css: './app/assets/css',
      sass: './app/assets/sass'
    }))
    .pipe(concat('test.css'))
//    .pipe(minifycss())
    .pipe(gulp.dest('app/assets/tmp'));
});
gulp.task('watch', function() {
  gulp.watch({glob:'app/assets/sass/*.scss'}, ['cs']);
});

gulp.task('default', ['cs', 'watch']);