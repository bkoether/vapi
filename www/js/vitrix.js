

$(function() {


  if($('#jobs-select-all').length > 0) {
    vitrixHelpers.reportsTable();
  }

  // Load maps
  if($('#gmap').length > 0) {
    vitrixHelpers.map();
  }

  // Load calendar
  if($('#calendar').length > 0) {
    vitrixHelpers.calendar();
  }

  // Popup feature
  if($('#info-modal-template').length > 0) {
    var templateSource = $('#info-modal-template').html();
    vitrixHelpers.popUpTemplate = Handlebars.compile(templateSource);
    vitrixHelpers.modalContainer = $('#info-modal');
    $('.pop-up-link').click(function(e){
      e.preventDefault();
      vitrixHelpers.modalInfo($(this).data('popup-type'), $(this).data('id'));
    });
  }

  // Description update
  $('#info-modal').on('click', '.desc-up-btn', function(button){
    button.preventDefault();
    var text = $('#info-modal #desc-update').val();
    var url = '/api/description/';
    if ($(this).hasClass('add-client')){
      url = url + 'client/' + $(this).data('id');
    }
    else {
      url = url + 'service/' + $(this).data('id');
    }

    url = url + '/' + $('#info-modal .desc-update-wrapper').data('request-id');

    $.post(url, {text: text}, function(data){
      if (data.newText.length > 0) {
        $('#info-modal .description-container').html(data.newText);
        $('#info-modal #desc-update').val('');
      }
    });
  });




  var instance = $('a.lb').imageLightbox({
    onStart: 	 function() { vitrixHelpers.lightboxOn(instance); },
    onEnd:	 	 function() { vitrixHelpers.lightboxOff();}
  });

  $('.set-time').click(function(){
    $(this).prev().val(moment().format('h:mm a'));
  });

  $('.overdue-callout').click(function(e){
    e.preventDefault();
    //console.log('yes');
    $('#DataTables_Table_0_filter input').val('overdue').trigger('keyup');
  });

  // Load data tables
  $('.data-tables').each(function(){
    vitrixHelpers.tables($(this));
  });

  // Set datepickers
  $('.dpick').daterangepicker({
    singleDatePicker: true,
    format: 'YYYY-MM-DD',
    startDate: moment()
  });
  //console.log('in1');

  if($('.job-dashboard').length > 0) {
    vitrixHelpers.jobList();
  }


  if($('.copy-address').length > 0) {

    $('.copy-address').click(function(e){
      e.preventDefault();
      var id = $('select[name="customer_id"]').val();
      //console.log(id);
      $('textarea[name="address"], textarea[name="service[address]"]').val(vitrixSettings.customerAddresses[id]);
    });
  }


  $('.rangepick').daterangepicker({
    ranges: {
      'This Week': [moment().startOf('week'), moment().endOf('week')],
      'Last Week': [moment().subtract('week', 1).startOf('week'), moment().subtract('week', 1).endOf('week')],
      'This Month': [moment().startOf('month'), moment().endOf('month')],
      'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')],
      'This Year': [moment().startOf('year'), moment().endOf('year')]
    },
    showDropdowns: true,
    format: 'YYYY-MM-DD',
    startDate: moment().startOf('month'),
    endDate: moment().endOf('month')
  });


  $('.vacation-range').daterangepicker({
    showDropdowns: true,
    format: 'YYYY-MM-DD',
  });

  if(typeof vitrixSettings !== 'undefined' && typeof vitrixSettings.customerRates !== 'undefined') {
    vitrixHelpers.serviceRates();
//    vitrixHelpers.frequencyCalc(true);

//    $('select[name="multi"], select[name="factor"]').change(function(){
//      vitrixHelpers.frequencyCalc(false);
//    });
//
//    $('select[name="customer_id"], select[name="employee_id"]').change(function(){
//      vitrixHelpers.serviceRates();
//    });
  }

  $('#jump-form').on('click', 'button', function (a, b) {
    var form = $(this).parent();

    var month = $('select[name="month"]', form).val();
    if (month < 10) {
      month = "0" + month;
    }
    var year = $('select[name="year"]', form).val();
    console.log('date=' + year + '-' + month);

    window.location.search = '?date=' + year + '-' + month;
  });


  if($('#canvas-pad').length > 0) {
    vitrixHelpers.signaturePad();
  }

});

var vitrixHelpers = {

  lightboxOn: function(instance) {
    $( '<div id="imagelightbox-overlay"></div>' ).appendTo( 'body' );
    $( '<a href="#" id="imagelightbox-close">Close</a>' ).appendTo( 'body' ).on( 'click touchend', function(){ $( this ).remove();
      instance.quitImageLightbox();
      return false; });
  },

  lightboxOff: function() {
    $( '#imagelightbox-overlay' ).remove();
    $( '#imagelightbox-close' ).remove();
  },

  jobList: function() {
    $('.jobs .list-group-item').each(function(i, e){
      $(e).on('click', 'a.trigger', function(ev){
        ev.preventDefault();
        $('.job-report-comment', e).removeClass('hidden');
        $('a i', e).removeClass('fa-check-square-o').addClass('fa-square-o');
        $(this).find('i').removeClass('fa-square-o').addClass('fa-check-square-o');

        if ($(this).hasClass('complete')){
          $('.new-date-field', e).removeClass('hidden');
          $('.new-date-field label', e).text('Completed On:');
          $('.dpick', e).focus();

          $('.status-field', e).val(1);
        }
        if ($(this).hasClass('postpone')){
          $('.new-date-field', e).removeClass('hidden');
          $('.new-date-field label', e).text('New Date:');
          $('.dpick', e).focus();

          $('.status-field', e).val(2);
        }
        if ($(this).hasClass('cancel')){
          $('.new-date-field', e).addClass('hidden');
          $('textarea', e).focus();
          $('.status-field', e).val(3);
        }

        $('.submit-button').removeClass('hidden');
        //if (vitrixHelpers.checkReportSubmit()){
        //  $('.submit-button').removeClass('hidden');
        //}
      });
    });

    $("form.job-dashboard").submit(function( event ) {

      var count = 0;
      $('.status-field').each(function(i){
        if ( ($(this).val() == 1 || $(this).val() == 2) && $('.dpick').eq(i).val().length < 1) {
          count++;
        }
      });

      if (count < 1){
        return;
      }
      //var count = $('.status-field').length;
      //$('.status-field').each(function(i){
      //  if ($(this).val() < 3 && $('.dpick').eq(i).val().length < 1) {
      //    count--;
      //  }
      //});
      //
      //if (count == $('.status-field').length){
      //  return;
      //}

      alert('Make sure to set dates for all completed or postponed jobs.');
      event.preventDefault();
    });

    $('a.cash-invoice').click(function(e){
      e.preventDefault();

      if (!$(this).hasClass('sent')) {
        $(this).next().toggleClass('hidden');
      }
    });

    $('.cash-invoice-form a').click(function(e){
      e.preventDefault();

      var job_id = $(this).data('job-id');
      var emails = $(this).prev().val();

      $.post('/api/cash-invoice/' + job_id, {emails: emails}, function(data){
        if (data.status == 'success') {
          $(this).parent().prev().addClass('sent');
          $(this).parent().hide();
        }
        else {
          $(this).next().text(data.message);
        }
      }.bind(this));
    });


  },

  map: function(){

    var map = new GMaps({
      div: '#gmap',
      lat: -12.043333,
      lng: -77.028333
    });

    GMaps.geocode({
      address: $('#gmap').attr('data-address'),
      callback: function(results, status) {
        if (status == 'OK') {
          var latlng = results[0].geometry.location;
          map.setCenter(latlng.lat(), latlng.lng());
          map.addMarker({
            lat: latlng.lat(),
            lng: latlng.lng()
          });
        }
      }
    });
  },

  tables: function(elem) {
    var table;
    if ($(elem).hasClass('dt-jobs')) {
      table = $(elem).DataTable({
        "bPaginate": true,
        "bLengthChange": true,
        "iDisplayLength": 100,
        "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
        "bFilter": true,
        "bSort": true,
        "bInfo": true,
        "bAutoWidth": true,
        "aoColumnDefs": [ {
          "aTargets": [-1],
          "bSortable": false,
          "bSearchable": false
        } ]
      });
    }
    else if ($(elem).hasClass('dt-dash')) {
      table = $(elem).DataTable({
        "bPaginate": true,
        "iDisplayLength": 100,
        "bLengthChange": true,
        "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
        "bFilter": true,
        "bSort": true,
        "bInfo": true,
        "bAutoWidth": true,
        "aaSorting": [[3, 'asc']],
        "aoColumnDefs": [ {
          "aTargets": [-1],
          "bSortable": false,
          "bSearchable": false
        }]
      });
    }
    else if ($(elem).hasClass('dt-reports')){
      table = $(elem).DataTable({
        "bPaginate": true,
        "bLengthChange": true,
        "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
        "bFilter": true,
        "bSort": true,
        "bInfo": true,
        "iDisplayLength": -1,
        "bAutoWidth": true,
        "aaSorting": [[5, 'asc']],
        "aoColumnDefs": [
          { "width": "50px", "targets": 0},
          {
          "aTargets": [0, -1],
          "bSortable": false,
          "bSearchable": false
        } ],
        "fnFooterCallback": function ( nRow, aaData, iStart, iEnd, aiDisplay ) {
          // Remove the formatting to get integer data for summation
          var intVal = function ( i ) {
            return typeof i === 'string' ?
              i.replace(/[\$,]/g, '')*1 :
              typeof i === 'number' ?
                i : 0;
          };

          var iTotalGross = 0;
          var iTotalNet = 0;
          var iTotalSci = 0;
          var iFilterGross = 0;
          var iFilterlNet = 0;
          var iFilterlSci = 0;


          // Totals
          for ( var i=0 ; i<aaData.length ; i++ )
          {
            iTotalGross += intVal(aaData[i][7]) * 1;
            iTotalNet += intVal(aaData[i][8]) * 1;
            iTotalSci += intVal(aaData[i][9]) * 1;
          }

          // Filtered totals
          for ( var i=iStart ; i<iEnd ; i++ )
          {
            iFilterGross += intVal(aaData[aiDisplay[i]][7]) * 1;
            iFilterlNet += intVal(aaData[aiDisplay[i]][8]) * 1;
            iFilterlSci += intVal(aaData[aiDisplay[i]][9]) * 1;
          }

          $('.total-gross').html('$' + iTotalGross.toFixed(2));
          $('.total-net').html('$' + iTotalNet.toFixed(2));
          $('.total-sci').html('$' + iTotalSci.toFixed(2));
          $('.filter-gross').html('$' + iFilterGross.toFixed(2));
          $('.filter-net').html('$' + iFilterlNet.toFixed(2));
          $('.filter-sci').html('$' + iFilterlSci.toFixed(2));

        }
      });
    }
    else if ($(elem).hasClass('dt-reverse')) {
      table = $(elem).DataTable({
        "bPaginate": true,
        "bLengthChange": false,
        "iDisplayLength": 100,
        "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
        "bFilter": true,
        "bSort": true,
        "bInfo": true,
        "aaSorting": [[0, 'desc']],
        "bAutoWidth": true,
        "aoColumnDefs": [ {
          "aTargets": [-1],
          "bSortable": false,
          "bSearchable": false
        } ]
      });
    }

    else if ($(elem).hasClass('dt-big')) {
      jQuery.fn.dataTableExt.oPagination.iFullNumbersShowPages = 10;
      table = $(elem).DataTable({
        "sDom": '<"filter-top"fi><"clearfix"><"top"lp>rt<"bottom"ip><"clearfix"><"filter-bottom"f>',
        "bPaginate": true,
        "bLengthChange": true,
        "iDisplayLength": 100,
        "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
        "bFilter": true,
        "bSort": true,
        "bInfo": true,
        "bAutoWidth": true,
        "aoColumnDefs": [ {
          "aTargets": [-1],
          "bSortable": false,
          "bSearchable": false
        } ]
      });
    }
    else if ($(elem).hasClass('dt-big2')) {
      jQuery.fn.dataTableExt.oPagination.iFullNumbersShowPages = 10;
      table = $(elem).DataTable({
        "sDom": '<"filter-top"fi><"clearfix"><"top"lp>rt<"bottom"ip><"clearfix"><"filter-bottom"f>',
        "bPaginate": true,
        "bLengthChange": true,
        "iDisplayLength": 100,
        "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
        "bFilter": true,
        "bSort": true,
        "bInfo": true,
        "bAutoWidth": true,
        "aoColumnDefs": [ {
          "aTargets": [0,-1],
          "bSortable": false,
          "bSearchable": false
        } ]
      });
    }

    else if ($(elem).hasClass('dt-simple')) {
      table = $(elem).DataTable({
        "bPaginate": true,
        "bLengthChange": false,
        "bFilter": true,
        "bSort": true,
        "bInfo": true,
        "bAutoWidth": true,
        "iDisplayLength": 10
      });
    }
    else if ($(elem).hasClass('dt-min')) {
      table = $(elem).DataTable({
        "bPaginate": false,
        "bLengthChange": false,
        "bFilter": false,
        "bSort": true,
        "bInfo": false,
        "bAutoWidth": true,
        "iDisplayLength": 100
      });
    }

    else {
      table = $(elem).DataTable({
        "bPaginate": true,
        "bLengthChange": false,
        "bFilter": true,
        "bSort": true,
        "bInfo": true,
        "bAutoWidth": true,
        "iDisplayLength": 100,
        "aoColumnDefs": [ {
          "aTargets": [-1],
          "bSortable": false,
          "bSearchable": false
        } ]
      });
    }
  },

  serviceRates: function(){
    var customerSelect = $('select[name="customer_id"]');
    var employeeSelect = $('select[name="employee_id"]');

    $('#customerRate').text(vitrixSettings.customerRates[$(customerSelect).val()]);
    $('#employeeRate').text(vitrixSettings.employeeRates[$(employeeSelect).val()]);

    if($(customerSelect).val() > 0 && $('#site_name').val().length < 1) {

      $('#site_name').val($(customerSelect).find('option:selected').text());
    }

    $('#customer_id').change(function(){
      $('#site_name').val($(this).find('option:selected').text());
      $('#customerRate').text(vitrixSettings.customerRates[$(customerSelect).val()]);
    });
    $('#employee_id').change(function(){
      $('#employeeRate').text(vitrixSettings.employeeRates[$(employeeSelect).val()]);
    });
  },

  frequencyCalc: function(init){
    if (init) {
//      console.log('in2');
      var fVal = $('input[name="frequency"]').val().split('.');
      $('select[name="multi"]').val(fVal[0]);
      $('select[name="factor"]').val(fVal[1]);
    }
    else {
      var newVal = $('select[name="multi"]').val() + '.' + $('select[name="factor"]').val();
      $('input[name="frequency"]').val(newVal);
      //console.log($('input[name="frequency"]').val());
    }
  },

  checkScheduleSubmit: function() {
//    if ($('#job-box li').length < 1) {
      $('.new-jobs').removeClass('hidden');
//      $('#job-box .submit-jobs').removeClass('hidden');
//    }
  },

  checkReportSubmit: function() {
    var count = $('.status-field').length;
    $('.status-field').each(function(){
      if ($(this).val() == 0) {
        count--;
      }
    });
    return count == $('.status-field').length;
  },

  calendar: function() {
    var listOptions = {
      valueNames: ['name', 'route']
    };
    var listSort = new List('job-box', listOptions);


    $('#job-box li').each(function() {

      var eventObject = {
        title: $.trim($(this).data('job-title')),
        description: $(this).data('job-description'),
        id: $(this).data('job-id'),
        durationEditable: false,
        newEvent: true
      };

      // store the Event Object in the DOM element so we can get to it later
      $(this).data('eventObject', eventObject);

      // make the event draggable using jQuery UI
      $(this).draggable({
        zIndex: 999,
        revert: true,      // will cause the event to go back to its
        revertDuration: 0,  //  original position after the drag
        appendTo: "body",
        helper: "clone"
      });

      $(this).addTouch();
    });

    var scrollTop = 0;
    var scrollSpace = 80;
    $('#job-box .scroll').click(function(){

      if ($(this).hasClass('down')) {
          scrollTop = scrollTop + scrollSpace;
        //$('#job-box ul').animate({'top': '+=' + scrollTop + 'px'}, 500, 'linear', function(){});
      }
      else {
        scrollTop = scrollTop - scrollSpace;
        //$('#job-box ul').animate({'top': '-=' + scrollTop + 'px'}, 500, 'linear', function(){});
      }
      $('#job-box ul').scrollTop(scrollTop);

    });



    vitrixHelpers.cal = $('#calendar').fullCalendar({
      header: {
        left: 'prev,today,next',
        center: 'title',
        right: 'month,basicWeek,basicDay'
      },
      // buttonText: {//This is to add icons to the visible buttons
        // prev: "<span class='fa fa-caret-left'></span> prev",
        // next: "next <span class='fa fa-caret-right'></span>",
        // today: 'today'
      // },
      editable: true,
      droppable: true,

      drop: function(date, allDay) { // this function is called when something is dropped
        if(vitrixSettings.role == 'employee' && $(this).data('priority')) {
          alert('High Priority Jobs can only be scheduled by administrators.');
        }
        else {
          // Remove the check if the job falls within it's time frame
          // if( moment(date).isAfter($(this).data('due-start')) && moment(date).isBefore($(this).data('due-end')) ) {
            // retrieve the dropped element's stored Event Object
            var originalEventObject = $(this).data('eventObject');

            // we need to copy it, so that multiple events don't have a reference to the same object
            var copiedEventObject = $.extend({}, originalEventObject);

            // assign it the date that was reported
            copiedEventObject.start = date;
            copiedEventObject.allDay = allDay;

            var duration = $(this).data('job-duration');
            if (duration > 1) {
              copiedEventObject.end = moment(date).add(duration, 'd');
            }


            if($(this).data('priority')) {
              copiedEventObject.priority = 1;
            }

            $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

            $(this).hide();
            vitrixHelpers.checkScheduleSubmit();
          // }
        }
      },

      eventDrop: function(event,dayDelta,minuteDelta,allDay,revertFunc) {
        if(vitrixSettings.role == 'employee' && event.priority) {
          alert('High Priority Jobs can only be scheduled by administrators.');
          revertFunc();
        }
        else {
          if (!event.newEvent) {
            event.updatedEvent = true;
            vitrixHelpers.checkScheduleSubmit();
          }
        }
      },

      eventSources: [
        {
          events: vitrixSettings.jobs,
          color: '#09a7e5',     // an option!
          textColor: '#fff', // an option!
          editable: true,
          durationEditable: false
        },
        {
          events: vitrixSettings.completed,
          color: '#ccc',     // an option!
          textColor: '#333', // an option!
          editable: false,
          durationEditable: false
        },
        {
          events: vitrixSettings.vacations,
          color: '#E0FBE8',     // an option!
          textColor: '#333', // an option!
          editable: false,
          durationEditable: false
        }
      ],
      eventRender: function(event, element) {
        if(event.cancelled == 1){
          element.css('background-color', '#FEF356');
        }
        if(event.priority == 1){
          element.css('background-color', '#ff1f21');
        }

        $(element).addTouch();
        $(element).click(function(){
          if (event.newEvent) {
            vitrixHelpers.modalInfo('job', event.id, '/true');
          }
          else {
            vitrixHelpers.modalInfo('job', event.id);
          }
        });
        //element.popover({
        //  html:true,
        //  title: event.title,
        //  placement:'top',
        //  container:'body',
        //  trigger: 'click',
        //  content: event.description
        //});
      }
    });

    $('.submit-jobs').click(function(e){
      $('#loader-msg').removeClass('hidden');
      $(this).addClass('hidden');
      e.preventDefault();
      var data = [];
      var q = 'done=true';
      var events;

      events = vitrixHelpers.cal.fullCalendar('clientEvents', function(e){return e.newEvent || e.updatedEvent});

      $(events).each(function() {
        data.push({
          id: $(this)[0].id,
          date: $(this)[0].start
        })
      });
      $.post(
        vitrixSettings.postPath,
        JSON.stringify(data),
        function( ret ){
          window.location.assign('?' + q)
        }
      );

    });
  },

  modalInfo: function(type, id, del) {
    var del = del || "";
    var url = '/api/' + type + '/' + id + del;
    $.getJSON(url, function(data){
      $('.modal-content', vitrixHelpers.modalContainer).html(vitrixHelpers.popUpTemplate(data));
      $(vitrixHelpers.modalContainer).modal('show');

      $('.cal-remove', vitrixHelpers.modalContainer).click(function(){
        var id  = $(this).data('event-id');
        vitrixHelpers.cal.fullCalendar('removeEvents', id);
        $('#job-box li[data-job-id="' + id + '"]').show();
        $(vitrixHelpers.modalContainer).modal('hide');
      });
    });
  },

  reportsTable: function() {
    $('th input[type="checkbox"]').click(function() {
      if ($('th:first input:checked').length > 0) {
        $('td input[type="checkbox"]').not(':checked').trigger('click');
      }
      else {
        $('td input[type="checkbox"]:checked').trigger('click');
      }
    });
    //}, function(){
    //
    //})
    //  else {
    //    $('td .icheckbox_minimal.checked').find('ins').trigger('click');
    //  }
    //});

    $('#mark-as-invoiced').click(function(){
      vitrixHelpers.updateJobs('invoiced');
    });
    $('#mark-as-filed').click(function(){
      vitrixHelpers.updateJobs('filed');
    });
    $('#collect-signatures').click(function(){
      vitrixHelpers.updateJobs('signatures');
    });

  },

  updateJobs: function(type) {
    if ($('input[name="jobs[]"]:checked').length > 0) {
      $('.waiting').removeClass('hidden');
      var jobs = [];
      $('input[name="jobs[]"]:checked').each(function(){
        jobs.push($(this).val());
      });

      if (type == 'signatures') {
        var url = '/admin/reports/signatures?jobs[]=' + jobs.join('&jobs[]=');
        window.open(url, '_blank');
      }
      else {
        $.post('', {updateType: type, jobs: jobs}, function(data){
          if(data.status == 'success') {
            window.location.reload();
          }
        });
      }
    }
  },

  signaturePad: function() {
    var wrapper = document.getElementById("canvas-pad"),
        canvas = wrapper.querySelector("canvas"),
        clearButton = $('.clear-pad'),
        saveButton = $('.accept-pad'),
        stringContainer = $('.signature-string'),
        submitButton = $('.submit-signature'),
        signaturePad;

    window.onresize = vitrixHelpers.resizeCanvas(canvas);
    vitrixHelpers.resizeCanvas(canvas);

    signaturePad = new SignaturePad(canvas);
    signaturePad.minWidth = 1;
    signaturePad.maxWidth = 2;

    $(clearButton).click(function (event) {
      signaturePad.clear();
      $(submitButton).attr('disabled', true);
      $(stringContainer).val('');
    });

    $(saveButton).click(function (event) {
      if (signaturePad.isEmpty()) {
        alert("Please provide signature first.");
      } else {
        $(submitButton).attr('disabled', false);
        signaturePad.removeBlanks();
        $(stringContainer).val(signaturePad.toDataURL());
      }
    });
  },
  resizeCanvas: function(canvas){
    var ratio =  Math.max(window.devicePixelRatio || 1, 1);
    canvas.width = canvas.offsetWidth * ratio;
    canvas.height = canvas.offsetHeight * ratio;
    canvas.getContext("2d").scale(ratio, ratio);
  }
};


SignaturePad.prototype.removeBlanks = function () {
  var imgWidth = this._ctx.canvas.width;
  var imgHeight = this._ctx.canvas.height;
  var imageData = this._ctx.getImageData(0, 0, imgWidth, imgHeight),
    data = imageData.data,
    getAlpha = function (x, y) {
      return data[(imgWidth * y + x) * 4 + 3]
    },
    scanY = function (fromTop) {
      var offset = fromTop ? 1 : -1;

      // loop through each row
      for (var y = fromTop ? 0 : imgHeight - 1; fromTop ? (y < imgHeight) : (y > -1); y += offset) {

        // loop through each column
        for (var x = 0; x < imgWidth; x++) {
          if (getAlpha(x, y)) {
            return y;
          }
        }
      }
      return null; // all image is white
    },
    scanX = function (fromLeft) {
      var offset = fromLeft ? 1 : -1;

      // loop through each column
      for (var x = fromLeft ? 0 : imgWidth - 1; fromLeft ? (x < imgWidth) : (x > -1); x += offset) {

        // loop through each row
        for (var y = 0; y < imgHeight; y++) {
          if (getAlpha(x, y)) {
            return x;
          }
        }
      }
      return null; // all image is white
    };

  var cropTop = scanY(true),
    cropBottom = scanY(false),
    cropLeft = scanX(true),
    cropRight = scanX(false);

  var relevantData = this._ctx.getImageData(cropLeft, cropTop, cropRight - cropLeft, cropBottom - cropTop);
  this._canvas.width = cropRight - cropLeft;
  this._canvas.height = cropBottom - cropTop;
  this._ctx.clearRect(0, 0, cropRight - cropLeft, cropBottom - cropTop);
  this._ctx.putImageData(relevantData, 0, 0);
};