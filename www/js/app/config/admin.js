app.config(function ($routeProvider) {
  $routeProvider
    .when('/dashboard',
    {
      controller: 'DashboardController',
      templateUrl: 'js/app/partials/dashboard-admin.html'
    })
    .when('/accounts',
    {
      controller: 'UserListController',
      templateUrl: 'js/app/partials/user-list.html'
    })
    .when('/accounts/new',
    {
      controller: 'UserAddController',
      templateUrl: 'js/app/partials/user-edit.html'
    })

    .when('/accounts/:accountId',
    {
      controller: 'UserController',
      templateUrl: 'js/app/partials/user-edit.html'
    })


    .when('/customers',
    {
      controller: 'CustomerListController',
      templateUrl: 'js/app/partials/customer-list.html'
    })
//    .when('/employees/:employeeId',
//    {
//      controller: 'EmployeesController',
//      templateUrl: 'js/app/partials/employee-list.html'
//    })


//    .when('/schedule/:yearMonth?',
//    {
//      controller: 'ScheduleController',
//      templateUrl: 'js/app/partials/schedule.html'
//    })
//    .when('/services',
//    {
//      templateUrl: 'js/app/partials/services.html'
//    })
//    .when('/reports/:yearMonth?',
//    {
//      controller: 'ReportsController',
//      templateUrl: 'js/app/partials/reports.html'
//    })
//    .when('/account',
//    {
//      controller: 'AccountController',
//      templateUrl: 'js/app/partials/account.html'
//    })
//    .when('/service/:serviceID',
//    {
//      templateUrl: 'js/app/partials/schedule.html'
//    })
//    .when('/job/:jobID',
//    {
//      controller: 'JobController',
//      templateUrl: 'js/app/partials/job.html'
//    })

    .otherwise({ redirectTo: '/dashboard' });
});