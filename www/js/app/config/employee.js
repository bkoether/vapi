app.config(function ($routeProvider) {
  $routeProvider
    .when('/dashboard',
    {
      controller: 'DashboardController',
      templateUrl: 'js/app/partials/dashboard.html'
    })
    .when('/schedule/:yearMonth?',
    {
      controller: 'ScheduleController',
      templateUrl: 'js/app/partials/schedule.html'
    })
    .when('/services',
    {
      templateUrl: 'js/app/partials/services.html'
    })
    .when('/reports/:yearMonth?',
    {
      controller: 'ReportsController',
      templateUrl: 'js/app/partials/reports.html'
    })
    .when('/account',
    {
      controller: 'AccountController',
      templateUrl: 'js/app/partials/account.html'
    })
    .when('/service/:serviceID',
    {
      templateUrl: 'js/app/partials/schedule.html'
    })
    .when('/job/:jobID',
    {
      controller: 'JobController',
      templateUrl: 'js/app/partials/job.html'
    })

    .otherwise({ redirectTo: '/dashboard' });
});