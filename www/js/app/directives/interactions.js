﻿app.directive('vmaMenuToggle', function($document) {
  return function(scope, element, attr) {
    element.on('click', function(e){
      e.preventDefault();
      $('#main-nav, #content').toggleClass('nav-visible');
    })
  }
});

app.directive('vmaTodoToggle', function($document) {
  return function(scope, element, attr) {
    element.on('click', function(){
      element.find('i')
        .toggleClass('fa-toggle-up')
        .toggleClass('fa-toggle-down');
      element.next().toggleClass('hide');
    })
  }
});