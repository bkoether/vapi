app.controller('CustomerListController', function($scope, apiService, Restangular) {

  init();

  function init() {
    $scope.title = 'Customerd';
    apiService.customer.getList().then(function(r){
      $scope.customers = Restangular.copy(r);
      console.log($scope.customers);
    });
  }
});