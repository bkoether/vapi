/**
 * List
 */
app.controller('UserListController', function($scope, apiService, Restangular) {

  init();

  function init() {
    $scope.title = 'Admin Accounts';
    apiService.user.getList({admins: 'true'}).then(function(r){
      $scope.admins = Restangular.copy(r);
    });

  }
});

/**
 * Add
 */
app.controller('UserAddController', function($scope, $routeParams, apiService, notifyService, passwordMatch) {


  init();

  function init() {
    $scope.title = 'New Admin Account';
    $scope.passRequired = true;
    $scope.pass = '';
    $scope.pwdRepeat = '';
    $scope.pwdMatchErr = true;
    $scope.user = {};
    $scope.btnText = 'Add Admin Account';
    $scope.hideStatus = true;
  }

  $scope.checkMatch = function() {
    passwordMatch($scope.pass, $scope.pwdRepeat, $scope.pwdMatchErr, $scope.accountForm.pwdRepeat);
  }

  $scope.updateUser = function() {
    $scope.user.active = true;
    $scope.user.password = pass;
    $scope.user.role = 'admin';
    apiService.user.post($scope.user).then(
      function(){
        notifyService.addAlert({msg: 'Account created.'});
        $location.path('/accounts');
      },
      function(response) {
        notifyService.addAlert({msg: 'Error.'});
      }
    );
  }



});

/**
 * Edit
 */
app.controller('UserController', function($scope, $routeParams, apiService, notifyService, Restangular, $location) {

  init();

  function init() {
    $scope.passRequired = false;
    $scope.pass = '';
    $scope.pwdRepeat = '';
    $scope.pwdMatchErr = true;
    $scope.btnText = 'Save Updates';
    $scope.hideStatus = $routeParams.accountId == vitrixUid;
    apiService.user.get($routeParams.accountId).then(function(r){
      $scope.user = Restangular.copy(r);
      $scope.title = $scope.hideStatus ? 'My Account' : 'Admin Account: ' + r.first + ' ' + r.last;
    });
  }

  $scope.checkMatch = function() {
    passwordMatch($scope.pass, $scope.pwdRepeat, $scope.pwdMatchErr, $scope.accountForm.pwdRepeat);
  }


  $scope.updateUser = function() {
    if ($scope.pass.length > 0) {
      $scope.user.password = pass;
    }
    $scope.user.put().then(
      function(){
        notifyService.addAlert({msg: 'Account created.'});
        $location.path('/accounts');
      },
      function(response) {
        notifyService.addAlert({msg: 'Error.'});
      }
    );

  }


  $scope.deleteUser = function() {
    $scope.user.remove().then(
      function(){
        notifyService.addAlert({msg: 'Account deleted.'});
        $location.path('/accounts');
      },
      function(response) {
        notifyService.addAlert({msg: 'Error.'});
      }
    );
  }

});
