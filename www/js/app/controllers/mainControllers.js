﻿/**
 * Global Controller
 */
app.controller('DashboardAlertController', function($scope, $rootScope, notifyService) {
  $scope.alerts = notifyService.getAlerts();
  $scope.closeAlert = function(index) {
    notifyService.removeAlert(index);
  }

});

app.controller('NavbarController', function ($scope, $location, notifyService) {
  $scope.clearAlerts = function(){
    notifyService.clearAll();
  }
  $scope.getClass = function (path) {
//    console.log($location.path());
    if ($location.path().substr(1, path.length) === path) {
      return true
    } else {
      return false;
    }
  }
});