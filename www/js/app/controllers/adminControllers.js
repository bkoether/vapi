﻿/**
 * Dashboard Controller
 */
app.controller('DashboardController', function ($scope) {

});



/**
 * Accounts Controller
 */
app.controller('EmployeesController', function($scope, $routeParams, apiService, notifyService, Restangular) {

  init();

  function init() {
    $scope.title = 'Employees';
    $scope.employee = {};
    $scope.btnText = 'Add Employee';
    $scope.hideStatus = true;
  }


  // Add/Edit Form
  if ($routeParams.accountId != null) {

    // Edit account
    if ($routeParams.accountId > 0) {
      $scope.passRequired = false;
      apiService.getUser($routeParams.accountId).then(function(r){
        $scope.user = Restangular.copy(r);

        $scope.hideStatus = $routeParams.accountId == vitrixUid;
        $scope.title = $scope.hideStatus ? 'My Account' : 'Admin Account: ' + r.first + ' ' + r.last;
        $scope.btnText = 'Save Updates';
      });

    }
    // New Account
    else {
      $scope.title = 'New Admin Account';
    }

    $scope.checkMatch = function() {

      if ($scope.pwdRepeat.indexOf($scope.pass) > -1 && $scope.pass.length == $scope.pwdRepeat.length  ) {

        $scope.pwdMatchErr = false;
        $scope.accountForm.pwdRepeat.$setValidity('pwdRepeat', true);
      }
      else {

        $scope.pwdMatchErr = true;
        $scope.accountForm.pwdRepeat.$setValidity('pwdRepeat', false);

      }
    }

    $scope.updateUser = function() {
      // Update user
      if ($scope.user.id != null) {
        apiService.updateUser($scope.user, $scope.pass);
      }
      // Add new user
      else{
        apiService.addUser($scope.user, $scope.pass);
      }

    }
    $scope.deleteUser = function() {
      apiService.deleteUser($scope.user);
    }
  }
  // List Table
  else {
    apiService.getAdminList().then(function(r){
      $scope.admins = Restangular.copy(r);
    });

  }

});
