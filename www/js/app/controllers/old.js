﻿/**
 * Dashboard Controller
 */
app.controller('DashboardController', function ($scope, dummyService) {

  init();

  function init() {
    $scope.dirty = false;
    var jobsToday = dummyService.getJobs('today');
    $scope.jobsToday = angular.copy(jobsToday);

    var jobsUpcoming = dummyService.getJobs('upcoming');
    $scope.jobsUpcoming = angular.copy(jobsUpcoming);

    $scope.commentStatus = [];

  }

//  Example of custom filters in ngRepeat
//  $scope.today = function(job) {
//    if (job.scheduledDate == "2014-03-17") {
//      return true;
//    }
//  }
//  $scope.upcoming = function(job) {
//    return job.scheduledDate > '2014-03-17';
//  }

  $scope.toggleComplete = function(id) {
    $scope.dirty = true;
    for (var i = 0; i < $scope.jobsToday.length; i++) {
      if ($scope.jobsToday[i].id === id) {
        if ($scope.jobsToday[i].completed) {
          $scope.jobsToday[i].completed = false;
        }
        else {
         $scope.jobsToday[i].completed = true;
        }
        break;
      }
    }
  }

  $scope.setStatus  = function(jobId, status) {

    if (status) {
          $scope.dirty = true;
    }
    $scope.commentStatus[jobId] = status;
  }
});


/**
 * Account Controller
 */
app.controller('AccountController', function ($scope, apiService, dummyService, notifyService, Restangular) {
//  var user = dummyService.getUser();
  apiService.getUser(1).then(function(r){
    console.log(r);
    $scope.user = Restangular.copy(r);
  });


//  $scope.user = angular.copy(user);

  $scope.pass = '';
  $scope.pwdRepeat = '';
  $scope.pwdMatchErr = true;

  $scope.checkMatch = function() {
    if ($scope.pwdRepeat.indexOf($scope.pass) > -1 && $scope.pass.length == $scope.pwdRepeat.length  ) {
      $scope.pwdMatchErr = false;
    }
    else {
      $scope.pwdMatchErr = true;
    }
  }

  $scope.updateUser = function(){
    dummyService.updateUser($scope.user);
    notifyService.addAlert({type: 'success', msg: 'Changes are saved.'});
    $scope.accountForm.$setPristine();
  }

  $scope.updatePassword = function() {
    notifyService.addAlert({type: 'success', msg: 'Password updated.'});
    $scope.pass = '';
    $scope.pwdRepeat = '';
    $scope.passwordForm.$setPristine();
  }

});

app.controller('JobController', function($scope, $routeParams, $modal, dummyService){
  init();

  function init() {
    var jobID = ($routeParams.jobID) ? parseInt($routeParams.jobID) : 0;
    $scope.job = dummyService.getJob(jobID);
  }

  var imgModal = $scope.open = function (img) {
    $modal.open({
      template: '<img src="' + img + '" /><a class="close-reveal-modal" ng-click="$dismiss(\'cancel\')">&#215;</a>'
    });
  }
});

app.controller('ReportsController', function ($scope, $routeParams, dummyService) {
  $scope.total = 0;
  var current;
  init();


  function init() {
    var jobs = dummyService.getJobs('completed');
    $scope.jobs = angular.copy(jobs);


    var prev, next;
    if ($routeParams.yearMonth) {
      current = moment($routeParams.yearMonth, 'YYYY-MM');
    }
    else {
      current = moment();
    }

    $scope.monthHeader = current.format('MMMM YYYY');
    $scope.currentMonth = {
      link: '#/reports/' + moment().format('YYYY-MM')
    };
    next = moment(current).add('M',1);
    $scope.nextMonth = {
      text: next.format('MMM YYYY'),
      link: '#/reports/' + next.format('YYYY-MM')
    };
    prev = moment(current).subtract('M',1);
    $scope.prevMonth = {
      text: prev.format('MMM YYYY'),
      link: '#/reports/' + prev.format('YYYY-MM')
    };

    var total = 0;
    for (var i = 0; i < jobs.length; i++) {
      var sub = 20;
      total += sub;
    }
    $scope.total = total;
  }

  $scope.monthFilter = function(obj) {
    return current.isSame(obj.completeDate, 'month');
  }
});

app.controller('ScheduleController', function ($scope, $routeParams, dummyService) {
  var currentStatic;
  init();

  function init() {
    $scope.dirty = false;
    var jobs = dummyService.getJobs('schedule');
    $scope.jobs = angular.copy(jobs);

    $scope.showWeekends = false;

    var current, prev, next, dayCount;
    if ($routeParams.yearMonth) {
      current = moment($routeParams.yearMonth, 'YYYY-MM');
    }
    else {
      current = moment().add('M', 1).startOf('month');
    }
    currentStatic = moment(current);

    $scope.monthHeader = current.format('MMMM YYYY');

    var minMonth = moment(_.min(jobs, function(j){return moment(j.dueRange.start, 'YYYY-MM-DD').unix();}).dueRange.start, 'YYYY-MM-DD');
    var maxMonth = moment(_.max(jobs, function(j){return moment(j.dueRange.end, 'YYYY-MM-DD').unix();}).dueRange.end, 'YYYY-MM-DD');
    $scope.calNav = [];
    while(minMonth.isBefore(maxMonth, 'month') || minMonth.isSame(maxMonth, 'month')) {
      $scope.calNav.push({
        text: minMonth.format('MMM YYYY'),
        link: '#/schedule/' + minMonth.format('YYYY-MM')
      });
      minMonth.add('M', 1);
    }

    $scope.cal = [];
    dayCount = current.daysInMonth();
    for (var d = 1; d <= dayCount; d++) {
      $scope.cal.push({
        day: d,
        date: current.format('YYYY-MM-DD'),
        weekend: current.isoWeekday() > 5,
        title: current.format('ddd D'),
        events: []
      });
      current.add('d', 1);
    }
  }

  $scope.monthFirst = function(obj) {
    return obj.day < 16;
  }
  $scope.monthSecond = function(obj) {
    return obj.day > 15;
  }
  $scope.weekendToggle = function(obj) {
    return $scope.showWeekends || (!$scope.showWeekends && !obj.weekend);
  }
  $scope.dueRangeFilter = function(obj) {
    return currentStatic.isSame(obj.dueRange.start, 'month') || currentStatic.isSame(obj.dueRange.end, 'month');
  }

  $scope.dropCallback = function(event, ui) {
    $scope.dirty = true;
    var date = $(event.target).data('day-id');
    var jobId = $(ui.draggable[0]).data('job-id');

    var dayField = _.findWhere($scope.cal, {date: date});
    var job = _.findWhere($scope.jobs, {id: jobId});

    dayField.events.push({title: job.title, jobId: jobId});
    job.scheduled = true;
  };

  $scope.removeJob = function(date, lId, jobId) {
    var dayField = _.findWhere($scope.cal, {date: date});
    var job = _.findWhere($scope.jobs, {id: jobId});
    dayField.events.splice(lId, 1);
    job.scheduled = false;

  }

})

