﻿//This handles retrieving data and is used by controllers. 3 options (server, factory, provider) with
//each doing the same thing just structuring the functions/data differently.
//app.service('dummyService', function () {
//  this.getUser = function() {
//    return user;
//  }
//  this.updateUser = function(newUser) {
//    user = newUser;
//  }
//
//
//  this.getJobs = function (filter) {
//    switch(filter) {
//      case 'today':
//        return _.where(jobs, {scheduledDate: '2014-03-17'});
//        break;
//      case 'upcoming':
//        var uj = _.filter(jobs, function(j){return j.scheduledDate > '2014-03-17'});
//        return _.groupBy(uj, 'scheduledDate');
//        break;
//      case 'schedule':
//        return _.where(jobs, {scheduled: false});
//        break;
//      case 'completed':
//        return _.where(jobs, {completed: true});
//        break;
//
//      default:
//        return jobs;
//        break;
//    }
//
//  };
//
//
//  this.getJob = function (id) {
//    for (var i = 0; i < jobs.length; i++) {
//        if (jobs[i].id === id) {
//            return jobs[i];
//        }
//    }
//    return null;
//  };
//
//  var jobs = [
//      {
//        "id": 0,
//        "serviceID": 1,
//        "title": "Kneedles",
//        "completed": true,
//        "scheduled": true,
//        "scheduledDate": "2014-03-02",
//        "completeDate": "2014-03-02",
//        "dueRange": {
//          "start": "1990-03-26",
//          "end": "1988-01-02"
//        },
//        "employeeComment": "Sint veniam magna qui nisi enim duis adipisicing cillum. Adipisicing non dolore fugiat elit laboris consequat. Adipisicing sint ipsum aliqua incididunt exercitation deserunt consectetur quis deserunt. Elit non magna dolor dolore incididunt proident pariatur. Ea occaecat pariatur quis ex excepteur fugiat elit et duis non magna sit ad esse. Lorem aute nisi pariatur aliquip et sunt laboris id.\r\n",
//        "adminComment": "Ipsum et cillum occaecat fugiat exercitation veniam eiusmod pariatur culpa reprehenderit Lorem culpa labore dolore. Esse deserunt minim aliqua magna. Voluptate qui qui occaecat mollit id. Sunt magna cillum est do qui nisi reprehenderit et aliqua sint culpa.\r\n",
//        "invoiced": false,
//        "serviceInfo": {
//          "picture": [
//            "/img/lorempixel-1.jpg",
//            "/img/lorempixel-6.jpg"
//          ],
//          "description": "Veniam veniam id dolore anim Lorem. Enim officia sunt pariatur dolore officia Lorem Lorem elit pariatur deserunt non eu. Id incididunt velit ex labore aliquip duis adipisicing consectetur ut cillum anim. Officia ex esse consectetur labore officia enim duis dolore laborum eu dolore labore.\r\n",
//          "type": "Window Cleaning",
//          "contactName": "Mcdaniel Watkins",
//          "email": "mcdanielwatkins@kneedles.com",
//          "phone": "+1 (922) 528-2840",
//          "address": "278 Withers Street, Bainbridge, Alaska, 194"
//        }
//      },
//      {
//        "id": 1,
//        "serviceID": 2,
//        "title": "Isologix",
//        "completed": false,
//        "scheduled": true,
//        "scheduledDate": "2014-03-17",
//        "completeDate": "",
//        "dueRange": {
//          "start": "2007-05-18",
//          "end": "2008-12-05"
//        },
//        "employeeComment": "",
//        "adminComment": "",
//        "invoiced": false,
//        "serviceInfo": {
//          "picture": [
//            "/img/lorempixel-1.jpg",
//            "/img/lorempixel-6.jpg"
//          ],
//          "description": "Nisi proident excepteur enim amet cupidatat. Culpa esse mollit sit in dolor ullamco cupidatat nisi commodo amet aliqua nisi. Cupidatat do duis ut mollit labore mollit culpa cillum officia adipisicing. Voluptate exercitation anim ipsum duis. Aliqua labore adipisicing ut pariatur nostrud nisi nostrud duis dolor.\r\n",
//          "type": "Window Cleaning",
//          "contactName": "Knapp Rojas",
//          "email": "knapprojas@isologix.com",
//          "phone": "+1 (940) 418-2240",
//          "address": "492 Doone Court, Gallina, Rhode Island, 3353"
//        }
//      },
//      {
//        "id": 2,
//        "serviceID": 1,
//        "title": "Orbean",
//        "completed": false,
//        "scheduled": true,
//        "scheduledDate": "2014-03-27",
//        "completeDate": "",
//        "dueRange": {
//          "start": "2007-07-31",
//          "end": "1993-08-10"
//        },
//        "employeeComment": "",
//        "adminComment": "",
//        "invoiced": false,
//        "serviceInfo": {
//          "picture": [
//            "/img/lorempixel-1.jpg",
//            "/img/lorempixel-6.jpg"
//          ],
//          "description": "Id magna elit enim nisi eiusmod minim incididunt consectetur irure et magna proident quis. Minim duis sit aliquip ea veniam ea duis tempor. Fugiat sint eiusmod labore nostrud eu fugiat aliquip. Voluptate laborum ad do commodo nostrud do fugiat ut. Qui esse nulla ea sunt. Amet in duis aliquip adipisicing reprehenderit proident deserunt mollit dolor cillum nisi.\r\n",
//          "type": "Window Cleaning",
//          "contactName": "Quinn Lane",
//          "email": "quinnlane@orbean.com",
//          "phone": "+1 (989) 440-2308",
//          "address": "804 Cropsey Avenue, Norvelt, Iowa, 6431"
//        }
//      },
//      {
//        "id": 3,
//        "serviceID": 1,
//        "title": "Cytrak",
//        "completed": true,
//        "scheduled": true,
//        "scheduledDate": "2014-03-05",
//        "completeDate": "2014-03-05",
//        "dueRange": {
//          "start": "2005-09-30",
//          "end": "2007-10-25"
//        },
//        "employeeComment": "Tempor eu aute enim sint nulla laboris aute nulla mollit aliquip reprehenderit ullamco enim. Dolore ipsum cillum veniam veniam esse. Eiusmod proident voluptate ipsum cupidatat consectetur duis eiusmod ipsum adipisicing veniam in voluptate.\r\n",
//        "adminComment": "Duis incididunt occaecat ullamco incididunt ex pariatur ullamco excepteur pariatur anim minim exercitation. Voluptate dolor pariatur cillum dolor dolore anim aute id minim nostrud ipsum. Elit dolor adipisicing id consequat mollit ex. Ea labore mollit ad voluptate Lorem eu enim exercitation duis officia dolore exercitation et minim. Duis sit anim minim excepteur amet est quis nisi in ad. Non laboris laborum aliqua culpa velit voluptate mollit pariatur aliqua commodo id proident ullamco. Labore occaecat ex in irure ut consequat aliquip adipisicing.\r\n",
//        "invoiced": false,
//        "serviceInfo": {
//          "picture": [
//            "/img/lorempixel-1.jpg",
//            "/img/lorempixel-6.jpg"
//          ],
//          "description": "Qui reprehenderit incididunt do velit qui veniam eu magna cupidatat eu. Et anim quis anim fugiat ad est amet consectetur aute consectetur sunt mollit et tempor. Nulla occaecat irure Lorem tempor irure anim veniam quis mollit magna culpa fugiat. In dolor veniam sunt aliqua non amet velit est id ex. Nostrud et ipsum anim culpa ex veniam sint cupidatat mollit est et consectetur tempor nulla.\r\n",
//          "type": "Window Cleaning",
//          "contactName": "Wade Booker",
//          "email": "wadebooker@cytrak.com",
//          "phone": "+1 (953) 468-2068",
//          "address": "590 Granite Street, Nogal, Connecticut, 4874"
//        }
//      },
//      {
//        "id": 4,
//        "serviceID": 2,
//        "title": "Enomen",
//        "completed": false,
//        "scheduled": true,
//        "scheduledDate": "2014-03-24",
//        "completeDate": "",
//        "dueRange": {
//          "start": "1990-07-05",
//          "end": "1991-03-27"
//        },
//        "employeeComment": '',
//        "adminComment": '',
//        "invoiced": false,
//        "serviceInfo": {
//          "picture": [
//            "/img/lorempixel-1.jpg",
//            "/img/lorempixel-6.jpg"
//          ],
//          "description": "Excepteur cillum qui exercitation anim ex et et culpa aliquip Lorem aute. Velit proident nisi exercitation commodo id excepteur elit duis commodo enim eiusmod. Cupidatat cupidatat veniam ut incididunt sunt irure proident.\r\n",
//          "type": "Window Cleaning",
//          "contactName": "Walsh Fischer",
//          "email": "walshfischer@enomen.com",
//          "phone": "+1 (880) 594-2897",
//          "address": "454 Calder Place, Wauhillau, Oklahoma, 7640"
//        }
//      },
//      {
//        "id": 5,
//        "serviceID": 1,
//        "title": "Gluid",
//        "completed": true,
//        "scheduled": true,
//        "scheduledDate": "2014-03-14",
//        "completeDate": "2014-03-14",
//        "dueRange": {
//          "start": "1997-10-29",
//          "end": "1994-09-09"
//        },
//        "employeeComment": "Dolore esse ullamco proident est veniam qui cillum aliquip. Anim occaecat ea commodo fugiat nisi ut voluptate cupidatat labore nisi ex. Fugiat ut et commodo proident officia dolor minim.\r\n",
//        "adminComment": "Deserunt tempor quis aliquip laborum quis nisi pariatur esse enim occaecat pariatur. Magna elit nulla quis et do sunt elit ut mollit ad reprehenderit commodo labore magna. Ut labore culpa anim adipisicing sint magna in reprehenderit velit quis velit tempor qui. Eiusmod voluptate Lorem quis eiusmod eu est reprehenderit deserunt dolore. Elit ea occaecat mollit ut fugiat ad laboris laborum ex culpa duis commodo ut dolore. Minim fugiat minim dolor esse culpa sint ullamco veniam velit aliquip velit adipisicing. Commodo culpa aliqua mollit officia amet sunt Lorem nostrud incididunt culpa proident ex.\r\n",
//        "invoiced": false,
//        "serviceInfo": {
//          "picture": [
//            "/img/lorempixel-1.jpg",
//            "/img/lorempixel-6.jpg"
//          ],
//          "description": "Veniam in non ut sunt. Sit enim incididunt est consectetur ad voluptate est anim cupidatat irure eu. Tempor tempor incididunt quis nulla ullamco culpa laborum eiusmod veniam ad nulla et ea laborum. Veniam pariatur excepteur fugiat esse sunt ea proident do pariatur ullamco enim. Tempor anim reprehenderit sit ea do laboris culpa eu. Ea ut id sunt fugiat adipisicing Lorem commodo mollit aute minim. Excepteur laborum elit laboris aliqua deserunt reprehenderit exercitation dolor sint ullamco ut aute velit.\r\n",
//          "type": "Window Cleaning",
//          "contactName": "Ericka Harvey",
//          "email": "erickaharvey@gluid.com",
//          "phone": "+1 (889) 556-2690",
//          "address": "230 Elliott Walk, Leyner, New Hampshire, 5520"
//        }
//      },
//      {
//        "id": 6,
//        "serviceID": 2,
//        "title": "Zomboid",
//        "completed": false,
//        "scheduled": true,
//        "scheduledDate": "2014-03-27",
//        "completeDate": "",
//        "dueRange": {
//          "start": "2001-04-29",
//          "end": "1988-05-01"
//        },
//        "employeeComment": '',
//        "adminComment": '',
//        "invoiced": false,
//        "serviceInfo": {
//          "picture": [
//            "/img/lorempixel-1.jpg",
//            "/img/lorempixel-6.jpg"
//          ],
//          "description": "Amet tempor culpa nostrud cupidatat duis est aliqua. Ipsum mollit eu fugiat consequat pariatur non qui ullamco. Laboris ut ex laborum elit cillum exercitation labore cupidatat nisi. Deserunt enim aliqua aute quis Lorem laboris ex pariatur. Nisi consequat et excepteur ullamco commodo amet sit. Tempor adipisicing reprehenderit commodo occaecat ad deserunt.\r\n",
//          "type": "Window Cleaning",
//          "contactName": "Stein Reeves",
//          "email": "steinreeves@zomboid.com",
//          "phone": "+1 (804) 592-3940",
//          "address": "925 Box Street, Canby, Kansas, 3307"
//        }
//      },
//      {
//        "id": 7,
//        "serviceID": 1,
//        "title": "Zoinage",
//        "completed": false,
//        "scheduled": true,
//        "scheduledDate": "2014-03-17",
//        "completeDate": "",
//        "dueRange": {
//          "start": "2007-10-19",
//          "end": "2006-09-16"
//        },
//        "employeeComment": '',
//        "adminComment": '',
//        "invoiced": false,
//        "serviceInfo": {
//          "picture": [
//            "/img/lorempixel-1.jpg",
//            "/img/lorempixel-6.jpg"
//          ],
//          "description": "Cupidatat excepteur tempor voluptate consectetur minim. Ut reprehenderit voluptate cillum laborum labore consectetur exercitation id dolore. Occaecat consectetur dolor irure eu. Reprehenderit ut consequat proident amet voluptate fugiat non Lorem duis dolore officia. Consectetur duis eiusmod aliqua ad ea fugiat nulla id. Labore Lorem non labore cupidatat. Fugiat sint laborum eiusmod occaecat eu sit sint proident irure occaecat excepteur labore magna.\r\n",
//          "type": "Window Cleaning",
//          "contactName": "Everett Ward",
//          "email": "everettward@zoinage.com",
//          "phone": "+1 (949) 493-3097",
//          "address": "949 Coleman Street, Spelter, Montana, 239"
//        }
//      },
//      {
//        "id": 8,
//        "serviceID": 2,
//        "title": "Geekwagon",
//        "completed": false,
//        "scheduled": true,
//        "scheduledDate": "2014-03-17",
//        "completeDate": "",
//        "dueRange": {
//          "start": "1989-05-04",
//          "end": "1992-02-01"
//        },
//        "employeeComment": '',
//        "adminComment": 'fgh',
//        "invoiced": false,
//        "serviceInfo": {
//          "picture": [
//            "/img/lorempixel-1.jpg",
//            "/img/lorempixel-6.jpg"
//          ],
//          "description": "Aliqua velit eiusmod veniam deserunt ullamco exercitation. Anim Lorem magna quis elit labore ea veniam dolore cupidatat ullamco irure. Laboris et occaecat consequat ut aliqua fugiat aliquip laborum commodo aute aute ea elit. Occaecat reprehenderit ut exercitation ullamco amet ea tempor Lorem. Adipisicing proident irure sint id mollit duis et laborum qui deserunt sit ullamco. Magna sunt minim occaecat enim pariatur ea labore eu anim magna.\r\n",
//          "type": "Window Cleaning",
//          "contactName": "Weber Wilkerson",
//          "email": "weberwilkerson@geekwagon.com",
//          "phone": "+1 (949) 479-3524",
//          "address": "469 Pierrepont Place, Clayville, Arizona, 9022"
//        }
//      },
//      {
//        "id": 9,
//        "serviceID": 1,
//        "title": "Polaria",
//        "completed": false,
//        "scheduled": true,
//        "scheduledDate": "2014-03-24",
//        "completeDate": "",
//        "dueRange": {
//          "start": "1988-08-06",
//          "end": "1996-12-28"
//        },
//        "employeeComment": '',
//        "adminComment": '',
//        "invoiced": false,
//        "serviceInfo": {
//          "picture": [
//            "/img/lorempixel-1.jpg",
//            "/img/lorempixel-6.jpg"
//          ],
//          "description": "Sit officia magna ex pariatur laboris mollit excepteur proident tempor elit velit eu. Officia nostrud magna dolor id cupidatat ullamco dolor ea enim culpa reprehenderit. Consequat do voluptate culpa anim veniam Lorem. Veniam voluptate eu id commodo. Nisi eu deserunt esse proident fugiat adipisicing.\r\n",
//          "type": "Window Cleaning",
//          "contactName": "Kemp Tran",
//          "email": "kemptran@polaria.com",
//          "phone": "+1 (832) 442-2014",
//          "address": "714 Lake Avenue, Detroit, Michigan, 3385"
//        }
//      },
//
//      {
//        "id": 10,
//        "serviceID": 1,
//        "title": "Enquility",
//        "completed": false,
//        "scheduled": false,
//        "scheduledDate": "",
//        "completeDate": "",
//        "dueRange": {
//          "start": "2014-04-01",
//          "end": "2014-04-30"
//        },
//        "employeeComment": "",
//        "adminComment": "",
//        "invoiced": false,
//        "serviceInfo": {
//          "picture": [
//            "/img/lorempixel-1.jpg",
//            "/img/lorempixel-6.jpg"
//          ],
//          "description": "Tempor enim officia minim commodo. Elit irure anim nisi amet officia laboris id. Aliquip dolore aliqua et ipsum laborum cupidatat consequat elit adipisicing voluptate officia. Esse elit irure ullamco reprehenderit Lorem mollit amet sunt sunt qui non consequat in aute. Enim et id irure aute qui cupidatat. Officia nisi adipisicing ad proident.\r\n",
//          "type": "Window Cleaning",
//          "contactName": "Sally Marks",
//          "email": "sallymarks@enquility.com",
//          "phone": "+1 (800) 532-3160",
//          "address": "583 Bayview Place, Keller, Florida, 2011"
//        }
//      },
//      {
//        "id": 11,
//        "serviceID": 1,
//        "title": "Furnafix",
//        "completed": false,
//        "scheduled": false,
//        "scheduledDate": "",
//        "completeDate": "",
//        "dueRange": {
//          "start": "2014-04-01",
//          "end": "2014-04-30"
//        },
//        "employeeComment": "",
//        "adminComment": "",
//        "invoiced": false,
//        "serviceInfo": {
//          "picture": [
//            "/img/lorempixel-1.jpg",
//            "/img/lorempixel-6.jpg"
//          ],
//          "description": "Commodo mollit eu eiusmod duis consequat incididunt do tempor nisi ex nostrud. Ex Lorem ex ex ad nulla incididunt reprehenderit cillum nostrud tempor ut. Reprehenderit eu aliquip occaecat anim adipisicing. Commodo dolor veniam labore cupidatat magna consectetur aute. Mollit quis do Lorem consequat ex tempor cupidatat pariatur nostrud laborum dolor. Eiusmod amet esse enim incididunt est cupidatat non sit sint irure dolor culpa.\r\n",
//          "type": "Window Cleaning",
//          "contactName": "Elliott Garza",
//          "email": "elliottgarza@furnafix.com",
//          "phone": "+1 (800) 465-2241",
//          "address": "879 Buffalo Avenue, Sunriver, West Virginia, 1034"
//        }
//      },
//      {
//        "id": 12,
//        "serviceID": 1,
//        "title": "Ultrasure",
//        "completed": false,
//        "scheduled": false,
//        "scheduledDate": "",
//        "completeDate": "",
//        "dueRange": {
//          "start": "2014-04-01",
//          "end": "2014-04-30"
//        },
//        "employeeComment": "",
//        "adminComment": "",
//        "invoiced": false,
//        "serviceInfo": {
//          "picture": [
//            "/img/lorempixel-1.jpg",
//            "/img/lorempixel-6.jpg"
//          ],
//          "description": "Nulla adipisicing proident veniam duis duis exercitation est dolore Lorem. Do aute proident ex reprehenderit veniam pariatur culpa laborum ipsum in cupidatat proident. Nulla ut exercitation veniam dolore dolore nisi voluptate officia duis.\r\n",
//          "type": "Window Cleaning",
//          "contactName": "Jillian Huber",
//          "email": "jillianhuber@ultrasure.com",
//          "phone": "+1 (941) 469-2212",
//          "address": "444 Ash Street, Bainbridge, Iowa, 9035"
//        }
//      },
//      {
//        "id": 13,
//        "serviceID": 2,
//        "title": "Terrago",
//        "completed": false,
//        "scheduled": false,
//        "scheduledDate": "",
//        "completeDate": "",
//        "dueRange": {
//          "start": "2014-04-01",
//          "end": "2014-04-30"
//        },
//        "employeeComment": "",
//        "adminComment": "",
//        "invoiced": false,
//        "serviceInfo": {
//          "picture": [
//            "/img/lorempixel-1.jpg",
//            "/img/lorempixel-6.jpg"
//          ],
//          "description": "Officia nisi ipsum non qui Lorem minim et consequat elit magna labore incididunt. Commodo nulla et irure voluptate consectetur. Aute laborum ipsum ullamco voluptate duis reprehenderit consequat nulla officia pariatur adipisicing officia reprehenderit. Laborum ea nostrud officia anim voluptate aliquip ad ex.\r\n",
//          "type": "Window Cleaning",
//          "contactName": "Vargas Wise",
//          "email": "vargaswise@terrago.com",
//          "phone": "+1 (928) 435-3860",
//          "address": "600 Poplar Avenue, Florence, Texas, 9946"
//        }
//      },
//      {
//        "id": 14,
//        "serviceID": 1,
//        "title": "Martgo",
//        "completed": false,
//        "scheduled": false,
//        "scheduledDate": "",
//        "completeDate": "",
//        "dueRange": {
//          "start": "2014-04-01",
//          "end": "2014-04-30"
//        },
//        "employeeComment": "",
//        "adminComment": "",
//        "invoiced": false,
//        "serviceInfo": {
//          "picture": [
//            "/img/lorempixel-1.jpg",
//            "/img/lorempixel-6.jpg"
//          ],
//          "description": "Non voluptate reprehenderit commodo amet eiusmod proident anim minim incididunt consequat pariatur ullamco cupidatat. Eu qui aute commodo voluptate ut commodo labore officia incididunt non sint qui sint sit. Labore velit cillum ipsum incididunt adipisicing mollit voluptate. Id exercitation commodo sit consequat officia minim culpa sunt esse aute enim laboris. Occaecat veniam ut mollit laboris ex id duis. Nisi est quis amet ipsum duis quis.\r\n",
//          "type": "Window Cleaning",
//          "contactName": "Hines Roman",
//          "email": "hinesroman@martgo.com",
//          "phone": "+1 (806) 403-2037",
//          "address": "962 Durland Place, Homestead, Wisconsin, 1890"
//        }
//      },
//      {
//        "id": 15,
//        "serviceID": 1,
//        "title": "Snacktion",
//        "completed": false,
//        "scheduled": false,
//        "scheduledDate": "",
//        "completeDate": "",
//        "dueRange": {
//          "start": "2014-04-01",
//          "end": "2014-04-30"
//        },
//        "employeeComment": "",
//        "adminComment": "",
//        "invoiced": false,
//        "serviceInfo": {
//          "picture": [
//            "/img/lorempixel-1.jpg",
//            "/img/lorempixel-6.jpg"
//          ],
//          "description": "Labore cupidatat amet pariatur labore ea. Dolor quis ex aliquip elit velit. Dolor aliqua labore enim quis Lorem.\r\n",
//          "type": "Window Cleaning",
//          "contactName": "Bishop Durham",
//          "email": "bishopdurham@snacktion.com",
//          "phone": "+1 (995) 526-2526",
//          "address": "543 Dwight Street, Crucible, Minnesota, 4540"
//        }
//      },
//      {
//        "id": 16,
//        "serviceID": 2,
//        "title": "Diginetic",
//        "completed": false,
//        "scheduled": false,
//        "scheduledDate": "",
//        "completeDate": "",
//        "dueRange": {
//          "start": "2014-04-15",
//          "end": "2014-04-30"
//        },
//        "employeeComment": "",
//        "adminComment": "",
//        "invoiced": false,
//        "serviceInfo": {
//          "picture": [
//            "/img/lorempixel-1.jpg",
//            "/img/lorempixel-6.jpg"
//          ],
//          "description": "Anim qui anim aliqua id do irure do enim sint pariatur. Duis fugiat sunt fugiat est aliquip. Aute irure magna ut dolore qui mollit quis. Lorem elit tempor pariatur esse cupidatat irure qui eiusmod consequat irure.\r\n",
//          "type": "Window Cleaning",
//          "contactName": "Navarro Anderson",
//          "email": "navarroanderson@diginetic.com",
//          "phone": "+1 (866) 411-2195",
//          "address": "496 Richardson Street, Hiseville, Rhode Island, 7521"
//        }
//      },
//      {
//        "id": 17,
//        "serviceID": 2,
//        "title": "Uneeq",
//        "completed": false,
//        "scheduled": false,
//        "scheduledDate": "",
//        "completeDate": "",
//        "dueRange": {
//          "start": "2014-04-01",
//          "end": "2014-04-30"
//        },
//        "employeeComment": "",
//        "adminComment": "",
//        "invoiced": false,
//        "serviceInfo": {
//          "picture": [
//            "/img/lorempixel-1.jpg",
//            "/img/lorempixel-6.jpg"
//          ],
//          "description": "Aliquip enim amet laboris aliquip. Do anim cupidatat exercitation est aliqua enim cupidatat. Incididunt pariatur aute nulla sint Lorem dolor dolor pariatur reprehenderit dolor aliquip mollit.\r\n",
//          "type": "Window Cleaning",
//          "contactName": "Mayra Sherman",
//          "email": "mayrasherman@uneeq.com",
//          "phone": "+1 (843) 405-2558",
//          "address": "137 Lancaster Avenue, Berwind, Kentucky, 2452"
//        }
//      },
//      {
//        "id": 18,
//        "serviceID": 1,
//        "title": "Musix",
//        "completed": false,
//        "scheduled": false,
//        "scheduledDate": "",
//        "completeDate": "",
//        "dueRange": {
//          "start": "2014-04-01",
//          "end": "2014-04-15"
//        },
//        "employeeComment": "",
//        "adminComment": "",
//        "invoiced": false,
//        "serviceInfo": {
//          "picture": [
//            "/img/lorempixel-1.jpg",
//            "/img/lorempixel-6.jpg"
//          ],
//          "description": "Sunt Lorem nisi elit in do laboris est quis. Ut fugiat magna adipisicing non ea ipsum incididunt exercitation fugiat occaecat magna. Exercitation occaecat ut pariatur eiusmod sunt ea dolor labore magna. Voluptate reprehenderit dolor eiusmod consectetur fugiat veniam veniam ea deserunt voluptate sunt consequat ut nostrud.\r\n",
//          "type": "Window Cleaning",
//          "contactName": "Meyers Solis",
//          "email": "meyerssolis@musix.com",
//          "phone": "+1 (853) 574-3747",
//          "address": "692 Townsend Street, Bawcomville, Nevada, 5933"
//        }
//      },
//      {
//        "id": 19,
//        "serviceID": 2,
//        "title": "Proxsoft",
//        "completed": false,
//        "scheduled": false,
//        "scheduledDate": "",
//        "completeDate": "",
//        "dueRange": {
//          "start": "2014-04-01",
//          "end": "2014-05-30"
//        },
//        "employeeComment": "",
//        "adminComment": "",
//        "invoiced": false,
//        "serviceInfo": {
//          "picture": [
//            "/img/lorempixel-1.jpg",
//            "/img/lorempixel-6.jpg"
//          ],
//          "description": "Qui enim in est id ullamco deserunt exercitation ipsum nostrud velit quis. Lorem ad incididunt amet consequat magna. Consequat duis officia elit nisi anim labore aliqua laboris in enim dolore do. Sunt sunt irure velit laboris tempor nostrud sunt proident duis incididunt.\r\n",
//          "type": "Window Cleaning",
//          "contactName": "Brandie Baxter",
//          "email": "brandiebaxter@proxsoft.com",
//          "phone": "+1 (852) 452-2250",
//          "address": "442 Classon Avenue, Walland, Missouri, 5417"
//        }
//      }
//    ];
//
//  var user = {
//    firstName: 'John',
//    lastName: 'Doe',
//    email: 'john@doe.com',
//    phoneMobile: '604-123-4567',
//    phoneOffice: ''
//  };
//});

app.service('notifyService', function() {
  this.clearAll = function() {
    alerts.length = 0;
  }
  this.getAlerts = function() {
    return alerts;
  }
  this.removeAlert = function(index) {
    alerts.splice(index, 1);
  }
  this.addAlert = function(newAlert) {
    alerts.push(newAlert);
  }

  var alerts = [
    // { type: 'alert', msg: 'There are 2 jobs that are overdue!'},
    // { msg: 'Please remember to put in your schedule for next month.'}
  ];
});

app.factory('passwordMatch', function() {
  return function(pass, pwdRepeat, pwdMatchErr, formField) {
    if (pwdRepeat.indexOf(pass) > -1 && pass.length == pwdRepeat.length  ) {

      pwdMatchErr = false;
      formField.$setValidity('pwdRepeat', true);
    }
    else {

      pwdMatchErr = true;
      formField.$setValidity('pwdRepeat', false);

    }
  }
});