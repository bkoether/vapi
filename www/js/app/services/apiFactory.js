app.factory('apiService', ['Restangular', 'notifyService', '$location', function(Restangular, notifyService, $location){

  var restAngular =
    Restangular.withConfig(function(Configurer) {
      Configurer.setBaseUrl('/');
    });
  restAngular.setResponseInterceptor(function(data, operation, what) {
    var response = data['data'];
    return response;
  });

  var service = {
    user: restAngular.all('api/users'),
    customer: restAngular.all('api/customers')
  };

//  var service = {
//    getUser: function(id) {
//      return _userService.get(id);
//    },
//    updateUser: function(user, pass){
//      if (pass.length > 0) {
//        user.password = pass;
//      }
//      return user.put().then(
//        function(){
//          notifyService.addAlert({msg: 'Account updated.'});
//          $location.path('/accounts');
//
//        },
//        function(response) {
//          notifyService.addAlert({msg: 'Error.'});
//        }
//      );
//    },
//    addUser: function(user, pass){
//      user.active = true;
//      user.password = pass;
//      user.role = 'admin';
//      return _userService.post(user).then(
//        function(){
//          notifyService.addAlert({msg: 'Account created.'});
//          $location.path('/accounts');
//
//        },
//        function(response) {
//          notifyService.addAlert({msg: 'Error.'});
//        }
//      );
//    },
//    deleteUser: function(user){
//      return user.remove().then(
//        function(){
//          notifyService.addAlert({msg: 'Account deleted.'});
//          $location.path('/accounts');
//
//        },
//        function(response) {
//          notifyService.addAlert({msg: 'Error.'});
//        }
//      );
//    },
//    getAdminList: function() {
//      return _userService.getList({admins: 'true'});
//    }
//  };

  return service;

}]);
