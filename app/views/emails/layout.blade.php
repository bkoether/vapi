<!doctype html>
<html lang="en">
<head>
  <meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />
</head>
<body style="margin: 0;padding: 0; background-color: #ececec;">

<table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:#ececec;padding-bottom:20px;">
    <tbody>
        <tr style="border-collapse:collapse">
            <td align="center" style="font-family:'Helvetica Neue',Arial,Helvetica,sans-serif;border-collapse:collapse">

                <table width="90%" cellpadding="0" cellspacing="0" border="0" style="margin-top:0;margin-bottom:0;margin-right:10px;margin-left:10px;">
                    <tbody>
                        <tr style="border-collapse:collapse;">
                            <td colspan="3" height="20" style="font-family:'Helvetica Neue',Arial,Helvetica,sans-serif;border-collapse:collapse"></td>
                        </tr>
                        <tr style="border-collapse:collapse; background-color: #000000;">
                            <td width="20px">&nbsp;</td>
                            <td style="font-family:'Helvetica Neue',Arial,Helvetica,sans-serif;border-collapse:collapse">
                                <p style="margin: 12px 0;color:#ffffff;font-family:'Lucida Grande',Lucida,Verdana,sans-serif;font-size:12px;text-transform: uppercase;">
                                    Vitrex Building Maintenance Ltd.
                                </p>
                            </td>
                            <td width="20px">&nbsp;</td>
                        </tr>
                        <tr style="border-collapse:collapse; background-color: #ffffff;">
                            <td colspan="3" height="10" style="font-family:'Helvetica Neue',Arial,Helvetica,sans-serif;border-collapse:collapse"></td>
                        </tr>

                        <tr style="border-collapse:collapse; background-color: #ffffff;">
                            <td width="20px">&nbsp;</td>
                            <td style="font-family:'Helvetica Neue',Arial,Helvetica,sans-serif;border-collapse:collapse">
                                @yield('content')
                            </td>
                            <td width="20px">&nbsp;</td>
                        </tr>
                        <tr style="border-collapse:collapse; background-color: #ffffff;">
                            <td colspan="3" height="10" style="font-family:'Helvetica Neue',Arial,Helvetica,sans-serif;border-collapse:collapse"></td>
                        </tr>
                        <tr style="border-collapse:collapse; background-color: #000000;">
                            <td colspan="3" height="2" style="font-family:'Helvetica Neue',Arial,Helvetica,sans-serif;border-collapse:collapse"></td>
                        </tr>
                    </tbody>
                </table>

            </td>
        </tr>
    </tbody>
</table>





</body>
</html>