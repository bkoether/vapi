<!doctype html>
<html lang="en">
<head>
    <meta content="text/html; charset=UTF-8" http-equiv="Content-Type"/>
</head>
<body style="margin: 0;padding: 0; background-color: #fff;">

    <div style="background-color:#fff;padding-bottom:20px;">


        <div style="width:90%; margin: 0 auto;">

            <div style=" background-color:#fff; margin-top: 20px; padding: 20px;color:#000 ;font-family:'Lucida Grande',Lucida,Verdana,sans-serif;font-size:12px;text-transform: uppercase; border-bottom: 3px solid #000;">
                Vitrex Building Maintenance Ltd
            </div>


            <div style="font-family:'Helvetica Neue',Arial,Helvetica,sans-serif;border-collapse:collapse; background-color: #fff; padding: 20px; border-bottom: 2px solid#000;">
                @yield('content')
            </div>
        </div>
    </div>
</body>
</html>