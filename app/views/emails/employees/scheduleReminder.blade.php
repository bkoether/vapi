@extends('emails.layout')

@section('content')

<table width="100%" cellpadding="0" cellspacing="0" border="0">
  <tr>
    <td>
      <p style="margin: 12px 0;color:#333333;font-family:'Lucida Grande',Lucida,Verdana,sans-serif;font-size:22px;">
        {{ $title }}
      </p>
    </td>
  </tr>

  <tr>
    <td style="font-family:'Helvetica Neue',Arial,Helvetica,sans-serif;border-collapse:collapse; border-top: 1px solid #333333;">
      <p>Hello {{ $name }},<br/><br/>
        This is a reminder that the following <span style="font-weight: bold;color: #ac2925;">{{ $count }} {{ Str::plural('job', $count) }}</span>
        @if ($count > 1) have @else has @endif not been scheduled yet.<br/></p>
      <p>
        @foreach ($jobs as $job)
          {{ $job }}
          <br/>
        @endforeach
      </p>
      <p>
        <a href="{{ $url }}">Please complete your schedule here.</a>
      </p>
      </p>
    </td>
  </tr>
</table>

@stop