@extends('emails.layout')

@section('content')

<table width="100%" cellpadding="0" cellspacing="0" border="0">
  <tr>
    <td>
      <p style="margin: 12px 0;color:#333333;font-family:'Lucida Grande',Lucida,Verdana,sans-serif;font-size:22px;">
        {{ $subj }}
      </p>
      <p style="text-align: right; font-size: 16px;">Invoice #: {{ $id }}<br/>{{ $date }}</p>
    </td>
  </tr>

  <tr>
    <td>
      <p></p>
    </td>
  </tr>

  <tr>
    <td>
      <p>Please find attached the Cash Receipt for work completed at your business.</p>
      <p>&nbsp;</p>

      <p>Thank you for you business!</p>
      <p>
        <span style="font-weight: bold;">Vitrex Building Maintenance Ltd. </span><br/>
        34A - 2755 Lougheed Highway, Suite #129 <br/>
        Port Coquitlam, BC, V3B 5Y9
      </p>
      <p>&nbsp;</p>
    </td>
  </tr>
</table>

@stop