@extends('emails.layout')

@section('content')

<table width="100%" cellpadding="0" cellspacing="0" border="0">
  <tr>
    <td>
      <p style="margin: 12px 0;color:#333333;font-family:'Lucida Grande',Lucida,Verdana,sans-serif;font-size:22px;">
        {{ $title }}
      </p>
      <p style="text-align: right; font-size: 16px;">{{ $today }}<br/>Invoice #: {{ $inv_number }}</p>
    </td>
  </tr>

  <tr>
    <td>
      <p style="margin: 12px 0; background-color: #eeeeee;color:#333333;font-family:'Lucida Grande',Lucida,Verdana,sans-serif;padding: 5px;">
        <span style="font-weight: bold;">{{ $company }}</span><br/>
        @if ($name)
          {{ $name }}<br/>
        @endif
        {{ $address }}<br/>
        GST#: {{ $tax_number }}
      </p>
    </td>
  </tr>

  <tr>
    <td>
      <p>
        <span style="font-weight: bold;">Bill To:</span><br/>
        Vitrex Building Maintenance Ltd. <br/>
        34A - 2755 Lougheed Highway, Suite #129 <br/>
        Port Coquitlam, BC, V3B 5Y9
      </p>
    </td>
  </tr>

  <tr>
    <td style="font-family:'Helvetica Neue',Arial,Helvetica,sans-serif;border-collapse:collapse; border-top: 1px solid #333333;">
      <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
          <td>Date</td>
          <td>Job</td>
          <td>Rate</td>
          <td>Cash</td>
        </tr>
        @foreach ($jobs as $job)
        <tr style="border-top: 1px solid #333333;">
          <td style="font-size: 12px;">{{ $job['completed'] }}</td>
          <td style="font-size: 12px;">{{ $job['name'] }} ({{ $job['type'] }})</td>
          <td style="font-size: 12px;">$ {{ $job['rate'] }}</td>
          <td style="font-size: 12px;">$ {{ $job['cash'] }}</td>
        </tr>
        @endforeach
        @if($commission)
          <tr style="border-top: 1px solid #333333;">
            <td colspan="1"></td>
            <td colspan="1" style="font-size: 12px;">Commissions</td>
            <td colspan="2" style="font-size: 12px;">$ {{ number_format($commission, 2) }}</td>
          </tr>
        @endif
        <tr>
          <td colspan="1"style="border-top: 1px double #333333;padding-top: 11px;"></td>
          <td colspan="1"style="border-top: 1px double #333333;padding-top: 11px;">Sub Total</td>
          <td colspan="1"style="border-top: 1px double #333333;padding-top: 11px;">$ {{ $total }}</td>
          <td colspan="1"style="border-top: 1px double #333333;padding-top: 11px;">$ {{ number_format(($cash / 1.05), 2) }}</td>
        </tr>
        <tr>
          <td colspan="1"></td>
          <td colspan="1">GST 5%</td>
          <td colspan="2">$ {{ $tax }}</td>
        </tr>
        <tr>
          <td colspan="1"></td>
          <td colspan="1">Less Cash Received (incl GST)</td>
          <td colspan="2">($ {{ number_format($cash, 2) }})</td>
        </tr>
        <tr>
          <td colspan="1"></td>
          <td colspan="1"><span style="font-weight: bold;">Total</span></td>
          <td colspan="2"><span style="font-weight: bold;">$ {{ $grand_total }}</span></td>
        </tr>
      </table>
    </td>
  </tr>
</table>

@stop