@extends('emails.layout_pdf')

@section('content')
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td>
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td>
                            <p>
                                <span style="font-weight: bold;">Vitrex Building Maintenance Ltd.</span><br/>
                                34A - 2755 Lougheed Highway, Suite #129 <br/>
                                Port Coquitlam, BC, V3B 5Y9 <br/>
                                1-604-723-4672 <br/>
                                info@vitrexltd.ca
                            </p>

                        </td>
                        <td>
                            <p style="margin: 12px 0;color:#333333;font-family:'Lucida Grande',Lucida,Verdana,sans-serif;font-size:22px; text-align: right">
                                {{ $title }}
                            </p>
                            <p style="text-align: right; font-size: 16px;">
                                Receipt #: {{ $id }}<br/>{{ $date }}</p>
                        </td>
                    </tr>
                </table>


            </td>
        </tr>

        <tr>
            <td>
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td>
                            <p><span style="font-weight: bold;">To: </span><br/>
                                {{  $to_name or '' }} <br/>
                                {{  $to_company or '' }} <br/>
                                {{  $to_address or '' }}
                            </p>
                        </td>
                        <td>
                            @if($site_name)
                                <p><span style="font-weight: bold;">Work Site Address: </span><br/>
                                    {{  $site_name or '' }} <br/>
                                    {{  $site_address or '' }}
                                </p>
                            @endif
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="font-family:'Helvetica Neue',Arial,Helvetica,sans-serif;border-collapse:collapse; border-top: 1px solid #333333;">
            </td>
        </tr>
    </table>

    <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td>Quantity</td>
            <td>Service Type</td>
            <td>Unit Price</td>
            <td>Total</td>
        </tr>
        <tr style="border-top: 1px solid #333333;">
            <td style="font-size: 12px;">1</td>
            <td style="font-size: 12px;">{{ $jobType }}</td>
            <td style="font-size: 12px;">$ {{ $total }}</td>
            <td style="font-size: 12px;">$ {{ $total }}</td>
        </tr>
        <tr>
            <td colspan="2"
                style="border-top: 1px double #333333;padding-top: 11px;"></td>
            <td colspan="1"
                style="border-top: 1px double #333333;padding-top: 11px;">
                Subtotal
            </td>
            <td colspan="1"
                style="border-top: 1px double #333333;padding-top: 11px;">
                $ {{ $total }}</td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td colspan="1">GST 5%</td>
            <td colspan="1">$ {{ $tax }}</td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td colspan="1"><span style="font-weight: bold;">Total Due</span>
            </td>
            <td colspan="2"><span
                        style="font-weight: bold;">$ {{ $grand_total }}</span>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td>
                <p>&nbsp;</p>
                <p style="font-weight: bold; text-align: center">Thank you for
                    your business!</p>
                <p>&nbsp;</p>
                <p style="text-align: center">GST# 843445321</p>
            </td>
        </tr>

    </table>
@stop