@extends('emails.layout')

@section('content')

<table width="100%" cellpadding="0" cellspacing="0" border="0">
  <tr>
    <td>
      <p style="margin: 12px 0;color:#333333;font-family:'Lucida Grande',Lucida,Verdana,sans-serif;font-size:22px;">
        {{ $title }}
      </p>
    </td>
  </tr>

  <tr>
    <td style="font-family:'Helvetica Neue',Arial,Helvetica,sans-serif;border-collapse:collapse; border-top: 1px solid #333333;">

      <table width="100%" cellpadding="0" cellspacing="0">
        @foreach ($jobs as $job)
        <tr>
          <td style="border-top: 1px solid #333; padding: 10px;">
            <p>
              <a href="{{ $job['url'] }}">{{ $job['title'] }}</a> ({{ $job['type'] }}) -- {{ $job['status'] }} {{ $job['date'] }} -- Attachment: {{ $job['file'] }}<br/>
              @if (!empty($job['comment']))
              Comment: {{ $job['comment'] }}
              @endif
            </p>
          </td>
        </tr>
        @endforeach
      </table>

    </td>
  </tr>
</table>

@stop

