@extends('emails.layout')

@section('content')

<table width="100%" cellpadding="0" cellspacing="0" border="0">
  <tr>
    <td>
      <p style="margin: 12px 0;color:#333333;font-family:'Lucida Grande',Lucida,Verdana,sans-serif;font-size:22px;">
        {{ $subj }}
      </p>
      <p style="text-align: right; font-size: 16px;">Invoice #: {{ $id }}<br/>{{ $date }}</p>
    </td>
  </tr>

  <tr>
    <td>
      <p></p>
    </td>
  </tr>

  <tr>
    <td>
      <p>Attached is a copy of a Cash Receipt. <br/>
        Printed by: {{ $employee }}<br/>
        Printed on: {{ $printDate }}
      </p>
      <p>&nbsp;</p>

    </td>
  </tr>
</table>

@stop