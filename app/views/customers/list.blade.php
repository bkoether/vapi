@extends('layouts.admin')

@section('title')
  <h1>Customers</h1>
@stop

@section('bc')
@stop

@section('content')
  <div class="col-sm-12">
    <p><a class="btn-sm btn-primary" href="{{ route('admin.customers.create') }}"><i class="fa fa-plus"></i> Add Customer</a></p>
    <table class="table table-hover data-tables dt-big">
      <thead>
        <tr>
          <th>Name</th>
          <th>Chain</th>
          <th>Phone</th>
          <th>Email</th>
          <th>Address</th>
          <th>Status</th>
          <th>&nbsp;</th>
        </tr>
      </thead>
      <tbody>
        @foreach($customers as $cust)
        <tr>
          <td>{{ link_to_route('admin.customers.show', $cust->name, [$cust->id]) }}</td>
          <td>{{ $cust->chain }}</td>
          <td>{{ $cust->phone1 }}</td>
          <td><a href="mailto:{{ $cust->email }}">{{ $cust->email }}</a></td>
          <td>{{ nl2br($cust->billing_address) }}</td>
          <td><span class="label label-{{ $cust->status ? 'success' : 'danger' }}">{{ $cust->status ? 'active' : 'disabled' }}</span></td>
          <td><a class="btn btn-circle btn-default pop-up-link" data-id="{{ $cust->id }}" data-popup-type="customer" href="#"><i class="fa fa-info"></i></a></td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>

  @include('partials.pop-up')
@stop