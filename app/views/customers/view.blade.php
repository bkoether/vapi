@extends('layouts.admin')


@section('title')
    <h1>@if(!$customer->status) <i class="fa fa-lock text-danger"></i> @endif
        @if ($customer->store_number)
            # {{ $customer->store_number }}
        @endif
        {{ $customer->name }}
        <small>{{ $customer->chain }}</small>
    </h1>
@stop

@section('bc')
    <li>{{ link_to_route('admin.customers.index', 'Customers') }}</li>
@stop


@section('content')
    <div class="col-sm-6">

        <div class="box box-solid">
            <div class="box-header">
                <h3 class="box-title">Contact</h3>

                <div class="box-tools pull-right">
                    <button class="btn btn-default btn-sm"
                            data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <p class="lead">&nbsp;<i
                            class="fa fa-user"></i> {{ $customer->contact_name }}
                </p>

                <p class="lead">&nbsp;<i class="fa fa-phone"></i> <a
                            href="tel:+1{{ $customer->phone1 }}">{{ $customer->phone1 }}</a>
                </p>
                @if(!empty($customer->phone2))
                    <p class="lead">&nbsp;<i class="fa fa-phone"></i> <a
                                href="tel:+1{{ $customer->phone2 }}">{{ $customer->phone2 }}</a>
                    </p>
                @endif
                <p class="lead">&nbsp;<i class="fa fa-envelope"></i> <a
                            href="mailto:{{ $customer->email }}">{{ $customer->email }}</a>
                </p>
            </div>
            <div class="box-footer clearfix">
                <a class="pull-right"
                   href="{{ route('admin.customers.edit', [$customer->id]) }}"><i
                            class="fa fa-edit"></i> Edit Customer</a>
            </div>
        </div>

        @if(!empty($customer->contact_2_name))
            <div class="box box-solid">
                <div class="box-header">
                    <h3 class="box-title">Secondary Contact @if($customer->contact_2_title) ({{ $customer->contact_2_title }}) @endif</h3>

                    <div class="box-tools pull-right">
                        <button class="btn btn-default btn-sm"
                                data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <p class="lead">
                        &nbsp;<i class="fa fa-user"></i> {{ $customer->contact_2_name }}
                    </p>

                    <p class="lead">
                        &nbsp;<i class="fa fa-phone"></i> <a href="tel:+1{{ $customer->contact_2_phone }}">{{ $customer->contact_2_phone }}</a>
                    </p>
                    <p class="lead">
                        &nbsp;<i class="fa fa-envelope"></i> <a href="mailto:{{ $customer->contact_2_email }}">{{ $customer->contact_2_email }}</a>
                    </p>
                </div>
            </div>
        @endif

        <div class="box box-solid">
            <div class="box-header">
                <h3 class="box-title">Billing Address</h3>

                <div class="box-tools pull-right">
                    <button class="btn btn-default btn-sm"
                            data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body clearfix">
                <p>{{ nl2br($customer->billing_address) }}</p>
            </div>
        </div>

        @if(!empty($customer->description))
            <div class="box box-solid">
                <div class="box-header">
                    <h3 class="box-title">Description</h3>

                    <div class="box-tools pull-right">
                        <button class="btn btn-default btn-sm"
                                data-widget="collapse"><i
                                    class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body clearfix">
                    <p>{{ combine_description($customer, 'customer') }}</p>
                </div>
            </div>
        @endif

    </div>

    <div class="col-sm-6">

        <div class="box box-solid">
            <div class="box-header">
                <h3 class="box-title">Pending Estimates</h3>

                <div class="box-tools pull-right">
                    <button class="btn btn-default btn-sm"
                            data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body no-padding">
                <table class="table table-condensed">
                    @if (!$customer->estimates->count())
                        <tr>
                            <td>No pending estimates.</td>
                        </tr>
                    @else
                        <thead>
                            <tr>
                                <th>Estimate #</th>
                                <th>Service</th>
                                <th>Frequency</th>
                                <th>Expires</th>
                            </tr>
                        </thead>
                    @endif
                    @foreach($customer->estimates as $estimate)
                        <tr>
                            <td>
                                <a href="{{ route('admin.estimates.show', $estimate->id) }}">
                                    &nbsp;#{{ $estimate->estimate_number }}</a>
                            </td>
                            <td>{{ $estimate->data['type'] }}</td>
                            <td>{{ $estimate->data['frequency'] }}</td>
                            <td>{{ $estimate->expiration_date->toDateString() }}</td>
                        </tr>
                    @endforeach
                </table>
            </div>

            <div class="box-footer clearfix">
                <a class="pull-right"
                   href="{{ route('admin.estimates.create', ['customerId' => $customer->id]) }}"><i
                            class="fa fa-plus"></i> Add Estimate</a>
            </div>

        </div>

        <div class="box box-solid">
            <div class="box-header">
                <h3 class="box-title">Services</h3>

                <div class="box-tools pull-right">
                    <button class="btn btn-default btn-sm"
                            data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body no-padding">
                <table class="table table-condensed">
                    @if (!$customer->services->count())
                        <tr>
                            <td>No services assigned.</td>
                        </tr>
                    @endif
                    @foreach($services as $service)
                        <tr>
                            <td>
                                <a href="{{ route('admin.services.show', $service->id) }}">
                                    &nbsp;{{ $service->site_name }}
                                    ({{ $service->type }})</a></td>
                            <td>{{ $service->getFrequencyString() }}</td>
                            <td>
                                <span class="label label-{{ $service->status ? 'success' : 'danger' }}">{{ $service->status ? 'active' : 'blocked' }}</span>
                            </td>
                            <td>
                                <a class="btn btn-circle btn-default pop-up-link"
                                   data-id="{{ $service->id }}"
                                   data-popup-type="service" href="#"><i
                                            class="fa fa-info"></i></a></td>
                        </tr>
                    @endforeach
                </table>
            </div>

            <div class="box-footer clearfix">
                Default Rate: <strong>
                    ${{ number_format($customer->default_rate, 2) }}</strong>
                <a class="pull-right"
                   href="{{ route('admin.services.create', ['customerId' => $customer->id]) }}"><i
                            class="fa fa-plus"></i> Add Service</a>
            </div>

        </div>

        <div class="box box-solid">
            <div class="box-header">
                <h3 class="box-title">Upcoming Jobs</h3>

                <div class="box-tools pull-right">
                    <button class="btn btn-default btn-sm"
                            data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body no-padding">
                <table class="table table-condensed">
                    @if (!$customer->jobs->count())
                        <tr>
                            <td>No upcoming jobs.</td>
                        </tr>
                    @endif
                    @foreach($customer->jobs as $job)
                        <tr>
                            <td>
                                <a href="{{ route('admin.jobs.show', $job->id) }}">
                                    &nbsp;{{ $job->service->site_name or $job->customer->name }}
                                    ({{ $job->service->type or 'Custom' }})</a>
                            </td>
                            <td>
                                @if($job->scheduled)
                                    {{ $job->scheduled_at->toDateString() }}
                                @else
                                    <span class="label label-warning">Not scheduled</span>
                                @endif
                            </td>
                            <td>
                                <a class="btn btn-circle btn-default pop-up-link"
                                   data-id="{{ $job->id }}"
                                   data-popup-type="job" href="#"><i
                                            class="fa fa-info"></i></a></td>
                        </tr>
                    @endforeach
                </table>

            </div>

            <div class="box-footer">
                <a class="pull-right"
                   href="{{ route('admin.jobs.create', ['customerId' => $customer->id])}}"><i
                            class="fa fa-plus"></i> Add Custom Job</a>
                <a href="{{ action('ReportsController@index', ['customer' => $customer->id])}}"><i
                            class="fa fa-file-text-o"></i> Past Jobs</a>
            </div>
        </div>

    </div>
    @include('partials.pop-up')
@stop