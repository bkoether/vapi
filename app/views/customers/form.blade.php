@extends('layouts.admin')


@section('title')
    @if($new)
        <h1>New Customer</h1>
    @else
        <h1>{{ $customer->name }}
            <small>Edit Account</small>
        </h1>
    @endif
@stop



@section('bc')
    <li>{{ link_to_route('admin.customers.index', 'Customers') }}</li>
    @if ($new)
        <li class="active">New Customer</li>
    @else
        <li class="active">{{ link_to_route('admin.customers.show', $customer->name, $customer->id) }}</li>
    @endif
@stop


@section('content')
    @if($new)
        {{ Form::model($customer, ['route' => 'admin.customers.store']) }}
    @else
        {{ Form::model($customer, ['route' => ['admin.customers.update', $customer->id], 'method' => 'put']) }}
    @endif
    {{ Form::hidden('_destination', URL::previous()) }}

    <div class="form-group col-sm-12">
        {{ Form::checkbox('status') }}
        {{ Form::label('status', ' Account Active') }}
    </div>

    <div class="col-sm-6">
        <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
            {{ Form::label('name', 'Customer') }}
            {{ Form::text('name',  NULL, ['class' => 'form-control']) }}
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">Primary Contact</div>
            <div class="panel-body">
                <div class="form-group {{ $errors->has('contact_name') ? 'has-error' : ''}}">
                    {{ Form::label('contact_name', 'Contact Name') }}
                    {{ Form::text('contact_name',  NULL, ['class' => 'form-control']) }}
                </div>

                <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                    {{ Form::label('email', 'Email') }}
                    {{ Form::email('email',  NULL, ['class' => 'form-control']) }}
                </div>

                <div class="form-group {{ $errors->has('phone1') ? 'has-error' : ''}}">
                    {{ Form::label('phone1', 'Phone 1') }}
                    <div class="input-group">
                        <span class="input-group-addon">+1</span>
                        {{ Form::text('phone1',  NULL, ['class' => 'form-control']) }}
                    </div>
                </div>

                <div class="form-group {{ $errors->has('phone2') ? 'has-error' : ''}}">
                    {{ Form::label('phone2', 'Phone 2') }}
                    <div class="input-group">
                        <span class="input-group-addon">+1</span>

                        {{ Form::text('phone2',  NULL, ['class' => 'form-control']) }}
                    </div>
                </div>

            </div>
        </div>

        <div class="form-group {{ $errors->has('billing_address') ? 'has-error' : ''}}">
            {{ Form::label('billing_address', 'Billing Address') }}
            {{ Form::textarea('billing_address',  NULL, ['class' => 'form-control']) }}
        </div>

    </div>

    <div class="col-sm-6">
        <div class="form-group">
            {{ Form::label('chain', 'Chain') }}
            {{ Form::select('chain', select_lists('reports_chain'),  NULL, ['class' => 'form-control']) }}
        </div>
        <div class="form-group {{ $errors->has('default_rate') ? 'has-error' : ''}}">
            {{ Form::label('default_rate', 'Default Rate') }}
            <div class="input-group">
                <span class="input-group-addon">$</span>
                {{ Form::text('default_rate',  NULL, ['class' => 'form-control']) }}
            </div>
        </div>

        <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
            {{ Form::label('description', 'Description') }}
            {{ Form::textarea('description',  NULL, ['class' => 'form-control']) }}
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">Secondary Contact</div>
            <div class="panel-body">
                <div class="form-group {{ $errors->has('contact_2_title') ? 'has-error' : ''}}">
                    {{ Form::label('contact_2_title', 'Position/Title') }}
                    {{ Form::text('contact_2_title',  NULL, ['class' => 'form-control']) }}
                </div>

                <div class="form-group {{ $errors->has('contact_2_name') ? 'has-error' : ''}}">
                    {{ Form::label('contact_2_name', 'Contact Name') }}
                    {{ Form::text('contact_2_name',  NULL, ['class' => 'form-control']) }}
                </div>

                <div class="form-group {{ $errors->has('contact_2_email') ? 'has-error' : ''}}">
                    {{ Form::label('contact_2_email', 'Email') }}
                    {{ Form::email('contact_2_email',  NULL, ['class' => 'form-control']) }}
                </div>

                <div class="form-group {{ $errors->has('contact_2_phone') ? 'has-error' : ''}}">
                    {{ Form::label('contact_2_phone', 'Phone') }}
                    <div class="input-group">
                        <span class="input-group-addon">+1</span>
                        {{ Form::text('contact_2_phone',  NULL, ['class' => 'form-control']) }}
                    </div>
                </div>
            </div>
        </div>


    </div>











    @if($new)
        <div class="col-sm-12">
            {{ Form::submit('Add Customer', ['class' => 'btn btn-primary']) }}
        </div>
    @else
        <div class="col-sm-12">
            {{ Form::submit('Save Changes', ['class' => 'btn btn-primary']) }}
            <a class="btn btn-small btn-danger pull-right" href="#"
               data-toggle="modal" data-target="#delete-user-modal">Delete
                Customer</a>
        </div>
    @endif

    {{ Form::close() }}

    @if(!$new)
        <div id="delete-user-modal" class="modal fade" tabindex="-1"
             role="dialog" aria-labelledby="mySmallModalLabel"
             aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"
                                aria-hidden="true">×
                        </button>
                        <h4 class="modal-title">Delete Customer</h4>
                    </div>
                    <div class="modal-body">
                        <p>Are you sure you want to delete this customer:
                            <strong>{{ $customer->name }}</strong>?<br/>
                            This will delete all services and jobs that are
                            associated with this customer.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default"
                                data-dismiss="modal">No
                        </button>
                        &nbsp;
                        {{ Form::open(array('route' => ['admin.customers.destroy', $customer->id], 'class' => 'pull-right', 'method' => 'delete'))  }}
                        {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    @endif

@stop
