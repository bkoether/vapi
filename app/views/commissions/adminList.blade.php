@extends('layouts.admin')


@section('title')
    <h1>Commissions</h1>
@stop

@section('bc')
@stop

@section('content')

    <div class="col-sm-6">
        <div class="box box-solid">
            <div class="box-header">
                <h3 class="box-title">Current Commission Rates</h3>
            </div>
            <div class="box-body">
                <table class="table table-hover data-tables dt-simple">
                    <thead>
                        <tr>
                            <th>Customer</th>
                            <th>Service</th>
                            <th>Employee</th>
                            <th>% Rate</th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach($services as $service)
                            <tr>
                                <td>{{ link_to_route('admin.customers.show', $service->name, [$service->cid]) }}</td>
                                <td>{{ link_to_route('admin.services.show', $service->site_name . ' [' . $service->type . ']', [$service->sid]) }}</td>
                                <td>{{ link_to_route('admin.employees.show', $service->company, [$service->eid]) }}</td>
                                <td>{{ $service->commission_percentage }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="box box-solid">
            <div class="box-header">
                <h3 class="box-title">Paid Commissions Total</h3>
            </div>
            <div class="box-body">
                <table class="table table-hover data-tables dt-min">
                    <thead>
                        <tr>
                            <th>Employee</th>
                            <th>Commissions Total</th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach($commissions as $commission)
                            <tr>
                                <td>{{ link_to_route('admin.employees.edit', $commission->company, [$commission->id]) }}</td>
                                <td>$ {{ number_format($commission->sum, 2) }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-sm-12">

        <h3>Monthly
            Commissions &mdash; {{ Carbon::parse($month)->format('F Y') }}</h3>
        <div class="btn-group">
            <a href="commissions/?date={{ Carbon::parse($month)->subMonth()->format('Y-m') }}"
               type="button" class="btn btn-default"><i
                        class="fa fa-arrow-left"></i> {{ Carbon::parse($month)->subMonth()->format('M Y') }}
            </a>
            <a href="commissions" type="button" class="btn btn-default"><i
                        class="fa fa-calendar"></i></a>
            <a href="commissions/?date={{ Carbon::parse($month)->addMonth()->format('Y-m') }}"
               type="button"
               class="btn btn-default">{{ Carbon::parse($month)->addMonth()->format('M Y') }}
                <i class="fa fa-arrow-right"></i></a>
        </div>

        @if(count($monthCommissions))
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Service</th>
                        <th>Employee</th>
                        <th>Rate</th>
                        <th>Commission</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th colspan="4"><span class="pull-right">Total:</span></th>
                        <th>$ {{ number_format($monthCommissionsTotal, 2) }}</th>
                    </tr>
                </tfoot>
                <tbody>
                    @foreach($monthCommissions as $com)
                        <tr>
                            <td>{{ $com->created_at->toDateString() }}</td>
                            <td>{{ $com->job->service->site_name }} [{{  $com->job->service->type }}]</td>
                            <td>{{ $com->employee->company }}</td>
                            <td>% {{ $com->percentage }}</td>
                            <td>$ {{ number_format($com->value, 2) }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

        @else
            <h4 class="well well-sm">No commissions for this month.</h4>
        @endif

    </div>

@stop