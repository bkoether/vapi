@extends('layouts.employee')


@section('title')
    <h1>Commissions</h1>
@stop

@section('bc')
@stop

@section('content')
    <div class="col-sm-7">
        <h3>Monthly
            Commissions &mdash; {{ Carbon::parse($month)->format('F Y') }}</h3>
        <div class="btn-group">
            <a href="commissions/?date={{ Carbon::parse($month)->subMonth()->format('Y-m') }}"
               type="button" class="btn btn-default"><i
                        class="fa fa-arrow-left"></i> {{ Carbon::parse($month)->subMonth()->format('M Y') }}
            </a>
            <a href="commissions" type="button" class="btn btn-default"><i
                        class="fa fa-calendar"></i></a>
            <a href="commissions/?date={{ Carbon::parse($month)->addMonth()->format('Y-m') }}"
               type="button"
               class="btn btn-default">{{ Carbon::parse($month)->addMonth()->format('M Y') }}
                <i class="fa fa-arrow-right"></i></a>
        </div>

        @if(count($monthCommissions))
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Service</th>
                        <th>Commission</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th colspan="2"><span class="pull-right">Total:</span>
                        </th>
                        <th>
                            $ {{ number_format($monthCommissionsTotal, 2) }}</th>
                    </tr>
                </tfoot>
                <tbody>
                    @foreach($monthCommissions as $com)
                        <tr>
                            <td>{{ $com->created_at->toDateString() }}</td>
                            <td>{{ $com->job->service->site_name }}
                                [{{  $com->job->service->type }}]
                            </td>
                            <td>$ {{ number_format($com->value, 2) }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

        @else
            <h4 class="well well-sm">No commissions for this month.</h4>
        @endif

    </div>


    <div class="col-sm-5">
        <div class="box box-solid">
            <div class="box-header">
                <h3 class="box-title">My Commissions</h3>
            </div>
            <div class="box-body">
                @if(count($services))

                    <p>You are receiving commissions on the following
                        services:</p>
                    <table class="table table-hover data-tables dt-min">
                        <thead>
                            <tr>
                                <th>Customer</th>
                                <th>Service</th>
                                <th>% Rate</th>

                            </tr>
                        </thead>
                        <tbody>
                            @foreach($services as $service)
                                <tr>
                                    <td>{{ $service->customer->name }}</td>
                                    <td>{{ $service->site_name }}
                                        [ {{  $service->type }}]
                                    </td>
                                    <td>{{ $service->commission_percentage }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>


                @else
                    <p>You currently do not receive commissions on any
                        services.</p>
                @endif
            </div>
        </div>
    </div>
@stop