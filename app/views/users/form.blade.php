@extends('layouts.admin')


@section('title')
  @if($new)
    <h1>New Admin Account</h1>
  @elseif($own)
    <h1>My Account <small>{{ $user->fullName() }}</small></h1>
  @else
    <h1>{{ $user->fullName() }} <small>Edit Account</small></h1>
  @endif
@stop



@section('bc')
  <li>{{ link_to_route('admin.users.index', 'Admin Accounts') }}</li>
  <li class="active">{{ $new ? 'New' : 'Edit' }} Account</li>
@stop

@section('errors')
  @if($errors->all())
  <div class="alert alert-danger alert-dismissable">
    <i class="fa fa-warning"></i>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      @foreach($errors->all() as $err)
      <p>{{ $err }}</p>
      @endforeach
  </div>
  @endif
@stop



@section('content')
  @if($new)
    {{ Form::open(['route' => 'admin.users.store']) }}
  @else
    {{ Form::model($user, ['route' => ['admin.users.update', $user->id], 'method' => 'put']) }}
  @endif

  <div class="form-group col-sm-12 {{ $own ? ' hidden' : '' }}">
    {{ Form::checkbox('status') }}
    {{ Form::label('status', ' Account Active') }}
  </div>

  <div class="form-group col-sm-6 {{ $errors->has('first') ? 'has-error' : ''}}">
      {{ Form::label('first', 'First Name') }}
      {{ Form::text('first',  null, ['class' => 'form-control']) }}
    </div>

  <div class="form-group col-sm-6  {{ $errors->has('last') ? 'has-error' : ''}}">
    {{ Form::label('last', 'Last Name') }}
    {{ Form::text('last', null, ['class' => 'form-control']) }}
  </div>

  <div class="form-group col-sm-12  {{ $errors->has('email') ? 'has-error' : ''}}">
    {{ Form::label('email', 'Email') }}
    {{ Form::email('email', null, ['class' => 'form-control']) }}
  </div>

  <div class="col-sm-12">&nbsp;</div>

  <div class="form-group col-sm-6  {{ $errors->has('password') ? 'has-error' : ''}}">
    {{ Form::label('password', 'Password') }}
    {{ Form::password('password', ['class' => 'form-control']) }}
  </div>
  <div class="form-group col-sm-6">
    {{ Form::label('password_confirmation', 'Repeat Password') }}
    {{ Form::password('password_confirmation', ['class' => 'form-control']) }}
  </div>


  @if($new)
    <div class="col-sm-12">
      {{ Form::submit('Add Account', ['class' => 'btn btn-primary']) }}
    </div>
  @else
    <div class="col-sm-12">
      {{ Form::submit('Save Changes', ['class' => 'btn btn-primary']) }}
      @if(!$own)
        <a class="btn btn-small btn-danger pull-right" href="#" data-toggle="modal" data-target="#delete-user-modal">Delete Account</a>
      @endif
    </div>
  @endif

  {{ Form::hidden('role', 'admin') }}
  {{ Form::close() }}

  @if(!$new)
    <div id="delete-user-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" >
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">Delete Account</h4>
          </div>
          <div class="modal-body">
            <p>Are you sure you want to delete this account: <strong>{{ $user->fullName() }}</strong>?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
            &nbsp;
            {{ Form::open(array('route' => ['admin.users.destroy', $user->id], 'class' => 'pull-right'))  }}
            {{ Form::hidden('_method', 'DELETE') }}
            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
            {{ Form::close() }}
          </div>
        </div>
      </div>
    </div>
  @endif

@stop
