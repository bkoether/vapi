@extends('layouts.admin')

@section('title')
  <h1>Admin Accounts</h1>
@stop

@section('bc')
  <li class="active">Admin Accounts</li>
@stop


@section('content')
  <div class="col-sm-12">
    <p><a class="btn-sm btn-primary" href="{{ route('admin.users.create') }}"><i class="fa fa-plus"></i> Add Administrator</a></p>
    <table class="table table-hover">
      <thead>
        <tr>
          <th>{{ order_by_link('admin.users.index', 'First Name', 'first') }}</th>
          <th>{{ order_by_link('admin.users.index', 'Last Name', 'last') }}</th>
          <th>{{ order_by_link('admin.users.index', 'Email', 'email') }}</th>
          <th>{{ order_by_link('admin.users.index', 'Status', 'status') }}</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        @foreach($admins as $admin)
        <tr>
          <td>{{ $admin->last }}</td>
          <td>{{ $admin->first }}</td>
          <td>{{ $admin->email }}</td>
          <td><span class="label label-{{ $admin->status ? 'success' : 'danger' }}">{{ $admin->status ? 'active' : 'blocked' }}</span></td>
          <td>
            <a href="{{ route('admin.users.edit', [$admin->id]) }}"><i class="fa fa-edit"></i> Edit</a>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    {{ $admins->appends( Request::only(['sortBy', 'sort']) )->links() }}
  </div>
@stop