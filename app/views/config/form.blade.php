@extends('layouts.admin')

@section('title')
  <h1>Vitrex Web App <small>Settings</small></h1>
@stop

@section('content')


  <div class="col-sm-12">

    {{ Form::open(['action' => 'ConfigController@update', 'method' => 'post']) }}

      <div class="form-group {{ $errors->has('service_types') ? 'has-error' : ''}}">
        {{ Form::label('service_types', 'Service Types', ['class' => 'control-label']) }}
        {{ Form::textarea('service_types', $config['service_types'], ['rows' => '15', 'class' => 'form-control']) }}
        <p class="help-block">Enter one service per line. Separate the shortcode and full name with a "|" symbol.</p>
      </div>

      <div class="form-group {{ $errors->has('chains') ? 'has-error' : ''}}">
        {{ Form::label('chains', 'Chains', ['class' => 'control-label']) }}
        {{ Form::textarea('chains', $config['chains'], ['rows' => 10, 'class' => 'form-control']) }}
        <p class="help-block">Enter one chain per line.</p>
      </div>

      <div class="form-group {{ $errors->has('reports_email') ? 'has-error' : ''}}">
        {{ Form::label('reports_email', 'Reports Email') }}
        {{ Form::text('reports_email',  $config['emails']['reports'], ['class' => 'form-control']) }}
      </div>

      <div class="form-group {{ $errors->has('invoice_emails') ? 'has-error' : ''}}">
        {{ Form::label('invoice_emails', 'Invoice Email') }}
        {{ Form::text('invoice_emails',  $config['emails']['invoices'], ['class' => 'form-control']) }}
      </div>

      <div class="form-group {{ $errors->has('starbucks_email') ? 'has-error' : ''}}">
        {{ Form::label('starbucks_email', 'Default Starbucks Email') }}
        {{ Form::text('starbucks_email',  $config['emails']['starbucks'], ['class' => 'form-control']) }}
      </div>


      {{ Form::submit('Save', ['class' => 'btn btn-primary']) }}
    {{ Form::close() }}
  </div>
@stop