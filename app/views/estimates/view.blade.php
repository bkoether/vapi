@extends('layouts.admin')

@section('title')
    <h1>
        Estimate #{{ $estimate->estimate_number }}

    </h1>
@stop

@section('bc')
    <li><a href="{{ route('admin.estimates.index') }}">Estimates</a></li>
    <li class="active">Estimate Details</li>
@stop


@section('content')
    <div class="col-sm-12 col-md-7">
        <div class="box box-solid">
            <div class="box-header">
                <h3 class="box-title">Details</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-default btn-sm"
                            data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <div class="list-group">

                    @if ($estimate->service['store_number'])
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">Store
                                Number</h4>
                            <p class="list-group-item-text">{{ $estimate->service['store_number'] }}</p>
                        </div>
                    @endif

                    <div class="list-group-item">
                        <h4 class="list-group-item-heading">Customer</h4>
                        <p class="list-group-item-text">{{ link_to_route('admin.customers.show', $estimate->customer->name, [$estimate->customer->id]) }}
                            <span class="pull-right">Rate: ${{ $estimate->service['customer_rate'] }}</span>
                        </p>
                    </div>

                    <div class="list-group-item clearfix">
                        <h4 class="list-group-item-heading">Frequency</h4>
                        <p class="list-group-item-text">
                            {{ $service->frequencyOption[$estimate->service['frequency']] }}
                            <br/>
                            Next
                            schedule: {{ $estimate->service['next_schedule'] }}
                            <span class="pull-right">Schedule ahead: {{ $estimate->service['schedule_ahead'] }} {{Str::plural('month', $estimate->service['schedule_ahead'])}}</span>
                        <p>&nbsp;</p>
                    </div>

                    <div class="list-group-item">
                        <h4 class="list-group-item-heading">Invoice</h4>
                        <p class="list-group-item-text">{{ $service->invoiceOptions[$estimate->service['invoice_type']] }}
                            @if(isset($estimate->service['workorder_required'])) &mdash;
                            Signed work order required @endif
                            @if(isset($estimate->service['signature_required'])) &mdash;
                            Signature on receipt required @endif
                        </p>
                    </div>


                    <div class="list-group-item">
                        <h4 class="list-group-item-heading">Description</h4>
                        <div class="list-group-item-text">{{ $estimate->service['description'] }}</div>
                    </div>

                    <div class="list-group-item">
                        <h4 class="list-group-item-heading">Address</h4>
                        <div class="list-group-item-text">{{ nl2br($estimate->service['address']) }}</div>
                    </div>

                </div>


            </div>
            <div class="box-footer clearfix">
                @if($estimate->accepted)
                    <a class="btn btn-small btn-danger pull-right" href="#"
                       data-toggle="modal" data-target="#delete-user-modal">Delete Estimate</a>
                @else
                    <a class="pull-right"
                       href="{{ route('admin.estimates.edit', [$estimate->id]) }}"><i
                                class="fa fa-edit"></i> Edit Estimate</a>
                @endif
            </div>
        </div>
    </div>

    <div class="col-sm-12 col-md-5">
        <div class="box box-solid">
            <div class="box-header">
                <h3 class="box-title">Estimate Details</h3>

                <div class="box-tools pull-right">
                    <button class="btn btn-default btn-sm"
                            data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <p>Estimate #: {{ $estimate->estimate_number }}</p>
                <p>Estimate
                    Date: {{ $estimate->estimate_date->toDateString() }}</p>
                <p>Expiry
                    Date: {{ $estimate->expiration_date->toDateString() }}</p>
                @if($estimate->accepted)
                    <h3 class="text-success">Accepted
                        On: {{ $estimate->accepted_date->toDateString() }}</h3>
                    <p><a class="btn btn-success" href="{{ route('admin.services.show', $estimate->service_id) }}">View Service</a></p>
                @endif

                @if($estimate->header_note)
                    <hr>
                    <h4>Header Text</h4>
                    {{ nl2br($estimate->header_note) }}
                @endif

                @if($estimate->footer_note)
                    <hr>
                    <h4>Footer Text</h4>
                    {{ nl2br($estimate->footer_note) }}
                @endif

                {{ Form::open(['action' => ['EstimateController@acceptEstimate', $estimate->id], 'method' => 'post']) }}

                @if(!$estimate->accepted)
                    <hr>
                    <p>
                        {{ Form::submit('Mark Estimate As Accepted', ['class' => 'btn btn-primary']) }}
                    </p>
                @endif
                {{ Form::close() }}
            </div>
        </div>
    </div>

    <div class="col-sm-12 col-md-5">
        <div class="box box-solid">
            <div class="box-header">
                <h3 class="box-title">Email Estimate</h3>

                <div class="box-tools pull-right">
                    <button class="btn btn-default btn-sm"
                            data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">



                {{ Form::open(['action' => ['EstimateController@sendEstimate', $estimate->id], 'method' => 'post']) }}

                <div class="form-group col-sm-12">
                    {{ Form::label('email_to', 'Email To:') }}
                    {{ Form::text('email_to',  $estimate->customer->email, ['class' => 'form-control']) }}
                </div>
                <div class="form-group col-sm-12">
                    {{ Form::label('email_subject', 'Subject:') }}
                    {{ Form::text('email_subject',  null, ['class' => 'form-control']) }}
                </div>
                <div class="form-group col-sm-12">
                    {{ Form::label('email_text', 'Email Text:') }}
                    {{ Form::textarea('email_text',  null, ['class' => 'form-control', 'rows' => 4]) }}
                </div>

                <div class="form-group col-sm-12">
                    {{ Form::submit('Send Estimate', ['class' => 'btn btn-primary']) }}
                </div>
                {{ Form::close() }}
                <div class="clearfix">
                    @if($estimate->last_sent_date)
                        <p class="pull-right text-success">Last sent: {{ $estimate->last_sent_date->toDateString() }}</p>
                    @endif
                </div>

            </div>
        </div>
    </div>


    @include('partials.deleteForm', ['title' => 'Delete Estimate', 'text' => 'Are you sure you want to delete this estimate?', 'route' => 'admin.estimates.destroy', 'id' => $estimate->id])



@stop


