@extends('layouts.admin')

@section('title')
    <h1>Estimates</h1>
@stop

@section('bc')
@stop

@section('content')
    <div class="col-sm-12">
        <p><a class="btn-sm btn-primary" href="{{ route('admin.estimates.create') }}"><i class="fa fa-plus"></i> Add Estimate</a></p>
        <table class="table table-hover data-tables dt-big">
            <thead>
                <tr>
                    <th>Estimate #</th>
                    <th>Customer</th>
                    <th>Date</th>
                    <th>Exp. Date</th>
                    <th>Accepted</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                @foreach($estimates as $estimate)
                    <tr>
                        <td>{{ link_to_route('admin.estimates.show', $estimate->estimate_number, [$estimate->id]) }}</td>
                        <td>{{ link_to_route('admin.customers.show', $estimate->customer->name, [$estimate->customer_id]) }}</td>
                        <td>{{ $estimate->estimate_date->toDateString() }}</td>
                        <td>{{ $estimate->expiration_date->toDateString() }}</td>
                        <td>@if($estimate->accepted){{ $estimate->accepted_date->toDateString() }} @endif</td>
                        <td>
                            <a class="btn btn-default" href="{{ route('admin.estimates.show', $estimate->id) }}"><i class="fa fa-file"></i> View</a>
                            @if(!$estimate->accepted)
                                <a class="btn btn-default" href="{{ route('admin.estimates.edit', $estimate->id) }}"><i class="fa fa-edit"></i> Edit</a>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@stop