@extends('layouts.admin')


@section('title')
    @if($new)
        <h1>New Estimate</h1>
    @else
        <h1>{{ $estimate->estimate_number }}
            <small>
                for {{ $estimate->customer->name or 'NO CUSTOMER SET' }}</small>
        </h1>
    @endif
@stop



@section('bc')
    <li>{{ link_to_route('admin.estimates.index', 'Estimates') }}</li>
    @if ($new)
        <li class="active">New Estimate</li>
    @elseif (isset($estimate->customer_id))
        <li class="active">{{ link_to_route('admin.estimates.show', $estimate->customer->name, [$estimate->id]) }}</li>
    @endif
@stop


@section('content')

    @if($new)
        {{ Form::model($estimate, ['route' => 'admin.estimates.store', 'files' => true]) }}
    @else
        {{ Form::model($estimate, ['route' => ['admin.estimates.update', $estimate->id], 'method' => 'put']) }}
    @endif

    {{ Form::hidden('_destination', URL::previous()) }}




    <div class="form-group col-sm-3 {{ $errors->has('customer_id') ? 'has-error' : ''}}">
        {{ Form::label('customer_id', 'Customer') }}
        {{ Form::select('customer_id', select_lists('customers'),  null, ['class' => 'form-control']) }}
    </div>

    <div class="form-group col-sm-3 {{ $errors->has('estimate_number') ? 'has-error' : ''}}">
        {{ Form::label('estimate_number', 'Estimate #') }}
        {{ Form::text('estimate_number',  null, ['class' => 'form-control']) }}
    </div>

    <div class="form-group col-sm-3 {{ $errors->has('estimate_date') ? 'has-error' : ''}}">
        {{ Form::label('estimate_date ', 'Estimate Date') }}
        <div class="input-group">
                <span class="input-group-addon">
                    <i class="fa fa-calendar"></i></span>
            {{ Form::text('estimate_date', (($estimate->estimate_date && $estimate->estimate_date->timestamp > 0) ? $estimate->estimate_date->toDateString() : Carbon::now()->toDateString()), ['class' => 'form-control dpick']) }}
        </div>
    </div>
    <div class="form-group col-sm-3">
        {{ Form::label('expiration_date ', 'Expiration Date') }}
        <div class="input-group">
                <span class="input-group-addon">
                    <i class="fa fa-calendar"></i></span>
            {{ Form::text('expiration_date', (($estimate->expiration_date && $estimate->expiration_date->timestamp > 0) ? $estimate->expiration_date->toDateString() : Carbon::now()->addDays(60)->toDateString()), ['class' => 'form-control dpick']) }}
        </div>
    </div>
    <div class="form-group col-sm-6">
        {{ Form::label('header_note', 'Header Text') }}
        {{ Form::textarea('header_note',  null, ['class' => 'form-control']) }}
    </div>
    <div class="form-group col-sm-6">
        {{ Form::label('footer_note', 'Footer Text') }}
        {{ Form::textarea('footer_note',  null, ['class' => 'form-control']) }}
    </div>


    <div class="col-sm-7">
        <div class="form-group {{ $errors->has('service[site_name]') ? 'has-error' : ''}}">
        {{ Form::label('service[site_name]', 'Site Name') }}
            {{ Form::text('service[site_name]',  ($estimate->data['site_name'] ?: null), ['class' => 'form-control']) }}
        </div>

        <div class="row">
            <div class="form-group col-xs-6 {{ $errors->has('service[type]') ? 'has-error' : ''}}">
                {{ Form::label('service[type]', 'Type') }}
                {{ Form::select('service[type]', Config::get('vitrex.service_types'),  ($estimate->data['type'] ?: null), ['class' => 'form-control']) }}
            </div>
            <div class="form-group col-xs-6 {{ $errors->has('service[store_number]') ? 'has-error' : ''}}">
                {{ Form::label('service[store_number]', 'Store Number') }}
                {{ Form::text('service[store_number]',  ($estimate->data['store_number'] ?: null), ['class' => 'form-control']) }}
            </div>

            <div class="form-group col-xs-6 {{ $errors->has('service[frequency]') ? 'has-error' : ''}}">
                {{ Form::label('service[frequency]', 'Frequency') }}
                {{Form::select('service[frequency]', $service->frequencyOptions, ($estimate->data['frequency'] ?: null), ['class' => 'form-control']) }}
            </div>
            <div class="form-group col-xs-6">
                {{ Form::label('service[schedule_ahead]', 'Schedule ahead (months)') }}
                {{ Form::selectRange('service[schedule_ahead]', 1, 12,  ($estimate->data['schedule_ahead'] ?: null), ['class' => 'form-control']) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('service[next_schedule] ', 'Next schedule date') }}
            <div class="input-group">
                <span class="input-group-addon"><i
                            class="fa fa-calendar"></i></span>
                {{ Form::text('service[next_schedule]', ($estimate->data['next_schedule'] ?: ''), ['class' => 'form-control dpick']) }}
            </div>
        </div>

    </div>

    <div class="col-sm-5">

                <div class="form-group {{ $errors->has('service[customer_rate]') ? 'has-error' : ''}}">
                    {{ Form::label('service[customer_rate]', 'Customer Rate') }}
                    <div class="input-group">
                        <span class="input-group-addon">$</span>
                        {{ Form::text('service[customer_rate]',  ($estimate->data['customer_rate'] ?: ''), ['class' => 'form-control']) }}
                    </div>
                </div>

                <div class="form-group {{ $errors->has('service[invoice_type]') ? 'has-error' : ''}}">
                    {{ Form::label('service[invoice_type]', 'Invoice Type') }}
                    {{ Form::select('service[invoice_type]', $service->invoiceOptions,  ($estimate->data['invoice_type'] ?: ''), ['class' => 'form-control']) }}
                </div>

                <div class="form-group">
                    {{ Form::checkbox('service[workorder_required]', 1, (isset($estimate->data['workorder_required']) ? $estimate->data['workorder_required'] : 0)) }}
                    {{ Form::label('service[workorder_required]', ' Signed work order required') }}
                </div>
                <div class="form-group">
                    {{ Form::checkbox('service[signature_required]', 1, (isset($estimate->data['signature_required']) ? $estimate->data['signature_required'] : 0)) }}
                    {{ Form::label('service[signature_required]', ' Signature on receipt required') }}
                </div>


    </div>

    <div class="col-sm-7">
        <div class="form-group">
            {{ Form::label('service[description]', 'Description') }}
            {{ Form::textarea('service[description]', ($estimate->data['description'] ?: false), ['class' => 'form-control', 'rows' => 3]) }}
        </div>
    </div>

    <div class="form-group col-sm-5">
        <div class="form-group">
            {{ Form::label('service[address]', 'Address') }}
            {{ Form::textarea('service[address]', ($estimate->data['address'] ?: false), ['class' => 'form-control', 'rows' => 3]) }}
            <button class="btn btn-xs copy-address">Use Customer Address</button>
        </div>
    </div>




    {{--<div class="col-sm-12">--}}
        {{--<div class="tab-content">--}}
            {{--<div id="services">--}}
                {{--@for($i = 1; $i < 6; $i++)--}}
                    {{--{{ Form::hidden('service['.$i.'][id]', $i) }}--}}
                    {{--<div class="panel panel-default">--}}
                        {{--<div class="panel-heading">--}}
                            {{--<h3 class="panel-title">--}}
                                {{--<label for='service[{{ $i }}][enabled]'>--}}
{{--                                    {{ Form::checkbox('service['.$i.'][enabled]', NULL, (isset($estimate->data[$i]['enabled']) ? $estimate->data[$i]['enabled'] : null)) }}--}}
                                    {{--Service #{{ $i }}--}}
                                {{--</label>--}}
                            {{--</h3>--}}
                        {{--</div>--}}
                        {{--<div class="panel-body">--}}
                            {{--<div class="col-sm-4">--}}
                                {{--<div class="form-group {{ $errors->has('service[]'.$i.'.site_name') ? 'has-error' : ''}}">--}}
                                    {{--{{ Form::label('service['.$i.'][site_name]', 'Site Name') }}--}}
                                    {{--{{ Form::text('service['.$i.'][site_name]',  ($estimate->data[$i]['site_name'] ?: null), ['class' => 'form-control']) }}--}}

                                {{--</div>--}}
                                {{--<div class="form-group {{ $errors->has('service[]'.$i.'.customer_rate') ? 'has-error' : ''}}">--}}
                                    {{--{{ Form::label('service['.$i.'][customer_rate]', 'Customer Rate') }}--}}
                                    {{--<div class="input-group">--}}
                                        {{--<span class="input-group-addon">$</span>--}}
                                        {{--{{ Form::text('service['.$i.'][customer_rate]', ($estimate->data[$i]['customer_rate'] ?: null), ['class' => 'form-control']) }}--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                            {{--</div>--}}
                            {{--<div class="col-sm-4">--}}
                                {{--<div class="form-group]">--}}
                                    {{--{{ Form::label('service['.$i.'][type]', 'Type') }}--}}
                                    {{--{{ Form::select('service['.$i.'][type]', Config::get('vitrex.service_types'),  ($estimate->data[$i]['type'] ?: null), ['class' => 'form-control']) }}--}}
                                {{--</div>--}}
                                {{--<div class="form-group">--}}
                                    {{--{{ Form::label('service['.$i.'][frequency]', 'Frequency') }}--}}
                                    {{--{{Form::select('service['.$i.'][frequency]', $service->frequencyOptions, ($estimate->data[$i]['frequency'] ?: null), ['class' => 'form-control']) }}--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="col-sm-4">--}}
                                {{--<div class="form-group">--}}
                                    {{--{{ Form::label('service['.$i.'][description]', 'Description') }}--}}
                                    {{--{{ Form::textarea('service['.$i.'][description]',  ($estimate->data[$i]['description'] ?: null), ['class' => 'form-control', 'rows' => 5]) }}--}}
                                {{--</div>--}}
                            {{--</div>--}}


                        {{--</div>--}}
                    {{--</div>--}}

                    {{--<div class="row">--}}

                    {{--</div>--}}
                {{--@endfor--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}




    @if($new)
        <div class="col-sm-12">
            {{ Form::submit('Save Estimate', ['class' => 'btn btn-primary']) }}
        </div>
    @else
        <div class="col-sm-12">
            {{ Form::submit('Save Changes', ['class' => 'btn btn-primary']) }}
            <a class="btn btn-small btn-danger pull-right" href="#"
               data-toggle="modal" data-target="#delete-user-modal">Delete
                Estimate</a>
        </div>
    @endif

    {{ Form::close() }}

    @if(!$new)
        @include('partials.deleteForm', ['title' => 'Delete Estimate', 'text' => 'Are you sure you want to delete this estimate?', 'route' => 'admin.estimates.destroy', 'id' => $estimate->id])
    @endif

@stop
