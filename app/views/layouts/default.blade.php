<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Vitrex Web App</title>
  <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
  <link rel="shortcut icon" href="{{ asset('ico/favicon.ico') }}">
  <link rel="icon" sizes="16x16 32x32 64x64" href="/ico/favicon.ico">
  <link rel="icon" type="image/png" sizes="196x196" href="/ico/favicon-196.png">
  <link rel="icon" type="image/png" sizes="160x160" href="/ico/favicon-160.png">
  <link rel="icon" type="image/png" sizes="96x96" href="/ico/favicon-96.png">
  <link rel="icon" type="image/png" sizes="64x64" href="/ico/favicon-64.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/ico/favicon-32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/ico/favicon-16.png">
  <link rel="apple-touch-icon" sizes="152x152" href="/ico/favicon-152.png">
  <link rel="apple-touch-icon" sizes="144x144" href="/ico/favicon-144.png">
  <link rel="apple-touch-icon" sizes="120x120" href="/ico/favicon-120.png">
  <link rel="apple-touch-icon" sizes="114x114" href="/ico/favicon-114.png">
  <link rel="apple-touch-icon" sizes="76x76" href="/ico/favicon-76.png">
  <link rel="apple-touch-icon" sizes="72x72" href="/ico/favicon-72.png">
  <link rel="apple-touch-icon" href="/ico/favicon-57.png">
  <meta name="msapplication-TileColor" content="#FFFFFF">
  <meta name="msapplication-TileImage" content="/ico/favicon-144.png">
  <meta name="msapplication-config" content="/ico/browserconfig.xml">
  @yield('css')
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  <![endif]-->
</head>
<body class="skin-blue">

  <header class="header">
    <a href="/admin" class="logo">Vitrex Web App</a>

    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>
      @yield('navbar-right')
    </nav>
  </header>

  <div class="wrapper row-offcanvas row-offcanvas-left">
    @yield('navigation')

    <!-- Right side column. Contains the navbar and content of the page -->
    <aside class="right-side">
      <section class="content-header">
        @yield('title')
        <ol class="breadcrumb">
          <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
          @yield('bc')
        </ol>
      </section>

      <section class="content">
        @include('partials.errors')

        <div class="row">
          @yield('content')
        </div>
      </section>
    </aside><!-- /.right-side -->
  </div><!-- ./wrapper -->

  @yield('scripts')
</body>
</html>