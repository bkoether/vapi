<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Vitrex Web App</title>
  <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
  <!-- bootstrap 3.0.2 -->
  <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
  <link href="/css/AdminLTE.css" rel="stylesheet" type="text/css" />
  <link href="/css/vitrix.css" rel="stylesheet" type="text/css" />

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  <![endif]-->
</head>
<body class="skin-blue">
  <nav class="navbar navbar-default" role="navigation">
    <a class="navbar-brand" href="/">Vitrex Web App</a>
  </nav>
<!-- header logo: style can be found in header.less -->
<div class="wrapper">
  <div class="container">
    <div class="row">


      <div class="col-md-6 col-md-push-3">
        <div class="error-box">
          @if(Session::has('error'))
          <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-warning"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            {{ Session::get('error') }}
          </div>
          @endif
        </div>
        @yield('content')
      </div>


    </div>
  </div>
</div><!-- ./wrapper -->


<!-- jQuery 2.0.2 -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="/js/bootstrap.min.js" type="text/javascript"></script>
</body>
</html>