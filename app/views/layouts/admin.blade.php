@extends('layouts.default')

@section('css')
<!-- bootstrap 3.0.2 -->
<link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<!-- font Awesome -->
<link href="/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!-- Ionicons -->
<link href="/css/ionicons.min.css" rel="stylesheet" type="text/css" />
<!-- Theme style -->
<link href="/css/AdminLTE.css" rel="stylesheet" type="text/css" />
<link href="//cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css" />
{{--<link href="/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />--}}
<link href="/css/daterangepicker.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.9.1/fullcalendar.min.css"/>
{{--<link rel="stylesheet" href="/css/vendor/fullcalendar.css"/>--}}
<link href="/css/vitrix.css" rel="stylesheet" type="text/css" />
@stop

@section('navbar-right')
<div class="navbar-right">
  <ul class="nav navbar-nav">
    <li class="dropdown">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <i class="glyphicon glyphicon-user"></i> <span>{{ Auth::user()->fullName() }} <i class="caret"></i></span>
      </a>
      <ul class="dropdown-menu">
        <li>{{ link_to_route('admin.users.edit', ' Edit Account', [Auth::user()->id], ['class' => 'fa fa-cog']) }}</li>
        <li>{{ link_to('logout', ' Log Out', ['class' => 'fa fa-power-off']) }}</li>
      </ul>
    </li>
  </ul>
</div>
@stop

@section('navigation')
  @include('partials.admin-nav')
@stop

@section('scripts')
<!-- jQuery 2.0.2 -->
<script src="/js/jquery.js" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/js/AdminLTE/app.js" type="text/javascript"></script>
<script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="//cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js" type="text/javascript"></script>
<script src="/js/moment.min.js" type="text/javascript"></script>
<script src="/js/daterangepicker.js" type="text/javascript"></script>
<script src="/js/jquery-ui.custom.min.js"></script>
<script src="/js/jquery.ui.touch.js"></script>
<script src="/js/imagelightbox.min.js" type="text/javascript"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.9.1/fullcalendar.min.js"></script>
<script src="/js/vendor/handlebars.min.js"></script>
<script src="/js/vendor/list.min.js"></script>
<script src="/js/signature_pad.min.js"></script>
<!-- AdminLTE App -->

<script src="//maps.google.com/maps/api/js?sensor=true"></script>
<script src="/js/gmaps.js"></script>
<script src="/js/vitrix.js"></script>
@stop


