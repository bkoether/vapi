@extends('layouts.employee')


@section('title')
    <h1>Sign Worksheet</h1>
@stop



@section('bc')
@stop


@section('content')


    @if($new)
        {{ Form::open(['action' => 'SignatureController@store', 'method' => 'post']) }}
    @else
        {{ Form::open(['action' => 'SignatureController@update', 'method' => 'post']) }}
    @endif
    {{ Form::hidden('_destination', URL::previous()) }}
    <div class="col-sm-12 col-md-4">
        <div class="form-group">
            {{ Form::label('customer', 'Customer', ['class' => 'control-label']) }}
            {{ Form::text('customer', $job->customer->name, ['class' => 'form-control', 'disabled' => true]) }}
        </div>
    </div>
    <div class="col-sm-12 col-md-4">
        <div class="form-group">
            {{ Form::label('service', 'Service', ['class' => 'control-label']) }}
            @if($job->service)
                {{ Form::text('service', $job->service->site_name . ' [' . $job->service->type . ']', ['class' => 'form-control', 'disabled' => true]) }}
            @else
                {{ Form::text('service', 'Custom Job', ['class' => 'form-control', 'disabled' => true]) }}
            @endif
        </div>
    </div>
    <div class="col-sm-12 col-md-4">
        <div class="form-group">
            {{ Form::label('date', 'Date', ['class' => 'control-label']) }}
            {{ Form::text('date', Carbon::now()->format('M jS, Y - g:ia'), ['class' => 'form-control', 'disabled' => true]) }}
        </div>
    </div>
    <div class="col-sm-12">

        <div class="form-group">
            {{ Form::label('signature_name', 'Signature Name', ['class' => 'control-label']) }}
            {{ Form::text('signature_name', '', ['class' => 'form-control']) }}
        </div>
        <div class="form-group">
            {{ Form::label('signature', 'Signature', ['class' => 'control-label']) }}
            <div id="canvas-pad">
                <div class="inner">
                    <canvas></canvas>
                </div>
            </div>
            <div class="clear-pad btn btn-default btn-sm">Clear</div>&nbsp;&nbsp;<div class="accept-pad btn btn-success btn-sm">Accept</div>

            <div class="hidden">{{ Form::textarea('signature', '', ['class' => 'form-control signature-string']) }}</div>
        </div>

        {{ Form::hidden('job_id', $job->id) }}
        {{ Form::submit('Submit Signature', ['class' => 'submit-signature btn btn-primary', 'disabled' => true]) }}
    </div>
    {{ Form::close() }}


@stop
