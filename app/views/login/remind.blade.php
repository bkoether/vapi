@extends('layouts.base')


@section('content')

@if(Session::has('status'))
  <div class="alert alert-success alert-dismissable">
    <i class="fa fa-check"></i>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    {{ Session::get('status') }}
  </div>
@endif
<h3>Request Password Reminder</h3>
<div class="well well-lg">
  {{ Form::open(array('action' =>  'RemindersController@postRemind')) }}
  <div class="form-group">
    {{ Form::label('email', 'Your Email:') }}
    {{ Form::text('email', null, ['class' => 'form-control']) }}
  </div>
  {{ Form::submit('Send reminder', array('class' => 'btn btn-primary')) }}
  {{ link_to('/login', 'Back to login', array('class' => 'pull-right')) }}
  {{ Form::close() }}
</div>
@stop