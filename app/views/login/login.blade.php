@extends('layouts.base')


@section('content')
<h3>Login</h3>
<div class="well well-lg">

  {{ Form::open(array('route' =>  'session.login')) }}

  <div class="form-group">
    {{ Form::label('email', 'Email:') }}
    {{ Form::text('email', null, ['class' => 'form-control']) }}
  </div>

  <div class="form-group">
    {{ Form::label('password', 'Password:') }}
    {{ Form::password('password', ['class' => 'form-control']) }}
  </div>
  {{ Form::submit('Login', array('class' => 'btn btn-primary')) }}
  {{ link_to('password/remind', 'Forgot your password?', array('class' => 'pull-right')) }}

  {{ Form::close() }}
</div>
@stop