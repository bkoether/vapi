@extends('layouts.base')


@section('content')


<h3>Reset Password</h3>
<div class="well well-lg">
  {{ Form::open(array('action' =>  'RemindersController@postReset')) }}
  {{ Form::hidden('token', $token) }}

  <div class="form-group">
    {{ Form::label('email', 'Your Email:') }}
    {{ Form::text('email', null, ['class' => 'form-control']) }}
  </div>

  <div class="form-group">
    {{ Form::label('password', 'New Password:') }}
    {{ Form::password('password', ['class' => 'form-control']) }}
  </div>

  <div class="form-group">
    {{ Form::label('password_confirmation', 'Confirm Password:') }}
    {{ Form::password('password_confirmation', ['class' => 'form-control']) }}
  </div>

  {{ Form::submit('Reset Password', array('class' => 'btn btn-primary')) }}

  {{ Form::close() }}
</div>
@stop