@extends('layouts.admin')

@section('title')
<h1>@if($report->submitted) <i class="fa fa-check text-success"></i> @endif Starbucks Daily Report <small>{{ $report->call_date->format('l, F jS,  Y'); }}</small></h1>
@stop

@section('bc')
<li>{{ link_to_route('admin.starbucks.index', 'Starbucks Reports') }}</li>
@stop



@section('content')

<div class="col-xs-12">

  {{ Form::open(['route' => ['admin.starbucks.update', $report->id], 'method' => 'put']) }}
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>Store</th>
        <th>Service</th>
        <th>Completed</th>
        <th>Contact</th>
        <th>Position</th>
        <th>Comment</th>
        <th>Time</th>
      </tr>
    </thead>

    @foreach ($report->report_data as $id => $job)
    <tr>
      {{ Form::hidden('report_data['.$id.'][store_number]', $job['store_number']) }}
      {{ Form::hidden('report_data['.$id.'][location_name]', $job['location_name']) }}
      {{ Form::hidden('report_data['.$id.'][service_type]', $job['service_type']) }}
      {{ Form::hidden('report_data['.$id.'][signed_worksheet]', $job['signed_worksheet']) }}
      {{ Form::hidden('report_data['.$id.'][completed]', (isset($job['completed']) ? $job['completed'] : 'N/A')) }}

      <td class="no-break">#{{ $job['store_number'] or ' ' }} {{ $job['location_name'] }}</td>
      <td>{{ $job['service_type']}}</td>
      <td>{{ $job['completed'] or "N/A" }}</td>
      @if($job['signed_worksheet'])
        <td colspan="4">Signed Worksheet</td>
      @else
        <td>{{ Form::text('report_data['.$id.'][contact_name]', $job['contact_name'], ['class' => 'form-control', 'size' => 500]) }}</td>
        <td>{{ Form::text('report_data['.$id.'][contact_position]', $job['contact_position'], ['class' => 'form-control' , 'size' => 500]) }}</td>
        <td>{{ Form::textarea('report_data['.$id.'][comment]', $job['comment'], ['class' => 'form-control', 'rows' => 2, 'cols' => 700]) }}</td>
        <td>
          <div class="input-group">
            {{ Form::text('report_data['.$id.'][call_time]', $job['call_time'], ['class' => 'form-control']) }}
            <span class="input-group-addon set-time"><i class="fa fa-clock-o"></i></span>
          </div>
        </td>
      @endif

    </tr>

    @endforeach
  </table>
  {{ Form::submit('Save', ['class' => 'btn btn-primary']) }}
  <a class="btn btn-small btn-success pull-right" href="#" data-toggle="modal" data-target="#send-report-modal">Send Report</a>
  {{ Form::close() }}
</div>

<div id="send-report-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Send Report</h4>
      </div>
      {{ Form::open(array('route' => ['admin.starbucks.submit', $report->id], 'method' => 'post'))  }}

      <div class="modal-body">
        <div class="form-group">
          {{ Form::label('email', 'Email report to') }}
          {{ Form::text('email', Config::get('vitrex.emails.starbucks'), ['class' => 'form-control']) }}
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        &nbsp;
        {{ Form::submit('Send', array('class' => 'btn btn-success')) }}
      </div>
      {{ Form::close() }}

    </div>
  </div>
</div>
@stop