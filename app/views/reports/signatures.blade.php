@extends('emails.layout_pdf')

@section('content')


    <table width="100%" cellpadding="0" cellspacing="0" border="1">
        @foreach($data as $row)
        <tr>
            <td>
                <table style="page-break-inside: avoid;" width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td width="60%" style="width: 60%; vertical-align: top; padding: 10px;">

                                {{$row['client']}}<br/>
                                {{$row['site']}} - {{$row['service']}}<br/><br/>
                                Signed by: {{ $row['signed_by'] }}<br/>
                                Signed on: {{ $row['signed_on'] }}
                        </td>
                        <td>
                            <img src="/{{$row['file']}}" style="width: auto; max-height: 110px; padding: 10px;">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        @endforeach

    </table>
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td>
                <p>&nbsp;</p>
            </td>
        </tr>

    </table>
@stop