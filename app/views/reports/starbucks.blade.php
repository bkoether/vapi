@extends('layouts.admin')

@section('title')
<h1>Starbucks Daily Report</h1>
@stop

@section('bc')
<li class="active">Starbucks Reports</li>
@stop



@section('content')
<div class="col-sm-6">
  <h3>Jobs To Report</h3>
  {{ Form::open(['path' => 'starbucks.store', 'method' => 'post']) }}

  <table class="table table-bordered">
    <thead>
      <tr>
        <th></th>
        <th>Location</th>
        <th>Service</th>
        <th>Completed</th>
        <th>Sub-Contractor</th>
      </tr>
    </thead>
    <tbody>
      @if(!$completed_jobs->count())
        <tr>
          <td colspan="5">All jobs reported on.</td>
        </tr>
      @endif

      @foreach ($completed_jobs as $job)
      <tr>
        <td>{{ Form::checkbox('jobs[]', $job->id, true) }}</td>
        <td>{{ $job->service->site_name or $job->customer->name }}</td>
        <td>{{ $job->service->type or 'Custom' }}</td>
        <td>{{ $job->completed_at->toDateString() }}</td>
        <td>{{ $job->employee->company or '' }}</td>
      </tr>
      @endforeach
    </tbody>


  </table>
  @if($completed_jobs->count())
  {{ Form::submit('Create Report', ['class' => 'form-control']) }}
  @endif

  {{ Form::close() }}
</div>

<div class="col-sm-6">
  <h3>Export Report</h3>
  {{ Form::open(['action' => 'StarbucksController@export', 'method' => 'get']) }}
    <div class="row">
    <div class="col-sm-10">
          
          <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            {{ Form::text('date', null, ['class' => 'form-control rangepick']) }}
          </div>
        </div>
        <div class="col-sm-2">
          {{ Form::submit('Export', ['class' => 'btn btn-default']) }}
        </div>
    </div>

  {{ Form::close() }}
  <p>&nbsp;</p>
  <h3>Past Reports</h3>
  <table class="table table-hover data-tables dt-reverse">
    <thead>
      <tr>
        <th>Date</th>
        <th>Submitted</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      @if(!$reports)
      <tr>
        <td colspan="3">No Reports entered</td>
      </tr>
      @endif
      @foreach ($reports as $report)
      <tr>
        <td>{{ $report->call_date->toDateString() }}</td>
        <td>@if($report->submitted)<span class="label label-success">YES</span>@else<span class="label label-warning">NO</span>@endif</td>
        <td><a href="{{ route('admin.starbucks.show', $report->id)}}"><i class="fa fa-file-text-o"></i> View</a></td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>

@stop