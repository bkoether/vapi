@extends('layouts.admin')

@section('title')
<h1>Reports</h1>
@stop

@section('bc')
  <li class="active">Reports</li>
@stop



@section('content')
<div class="col-sm-12">

  <div class="box box-solid">
    <div class="box-header">
      <h3 class="box-title">Filter</h3>
      <div class="box-tools pull-right">
        <button class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body">
      {{ Form::open(['path' => 'reports', 'method' => 'get']) }}
      <div class="row">
        <div class="form-group col-sm-3">
          {{ Form::label('date', 'Date') }}
          <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            {{ Form::text('date', $date, ['class' => 'form-control rangepick']) }}
          </div>
        </div>
        <div class="form-group col-sm-3">
          {{ Form::label('chain', 'Chain') }}
          {{ Form::select('chain', select_lists('reports_chain', 'All') , $chain , ['class' => 'form-control']) }}
        </div>

        <div class="form-group col-sm-3">
          {{ Form::label('customer', 'Customer') }}
          {{ Form::select('customer', select_lists('reports_customer', 'All') , $customer , ['class' => 'form-control']) }}
        </div>
        <div class="form-group col-sm-3">
          {{ Form::label('employee', 'Sub-Contractor') }}
          {{ Form::select('employee', select_lists('reports_employees', 'All') , $employee , ['class' => 'form-control']) }}
        </div>

        <div class="form-group col-sm-3">
          <label>
            {{ Form::checkbox('uninvoiced', '1', $uninvoiced,  ['id' => 'uninvoiced']) }}
            Only show un-invoiced jobs.
          </label>
        </div>
        <div class="form-group col-sm-3">
          <label>
            {{ Form::checkbox('unfiled', '1', $unfiled,  ['id' => 'unfiled']) }}
            Only show un-filed jobs.
          </label>
        </div>
        <div class="col-sm-6">
          <div class="pull-right">
            {{ Form::submit('Filter', ['class' => 'btn btn-default']) }}
          </div>

        </div>

      </div>
      {{ Form::close() }}
    </div>
  </div>


<table class="table table-hover data-tables dt-reports">
  <thead>
    <tr>
      <th><input type="checkbox" id="jobs-select-all"/></th>
      <th>Customer</th>
      <th>Site</th>
      <th>Type</th>
      <th>Sub-Contractor</th>
      <th>Completed</th>
      <th>IT</th>
      <th>Gross</th>
      <th>Net</th>
      <th>SC Invoice</th>
      <th></th>
    </tr>
  </thead>
  <tfoot>
    <tr>
      <th colspan="6"></th>
      <th><span class="pull-right">Total:</span></th>
      <th class="total-gross"></th>
      <th class="total-net"></th>
      <th class="total-sci"></th>
      <th></th>
    </tr>
    <tr>
      <th colspan="6">
        <a href="#" class="btn btn-default" id="mark-as-invoiced"><i class="fa fa-dollar"></i> Mark selected as invoiced</a>&nbsp;
        <a href="#" class="btn btn-default" id="mark-as-filed"><i class="fa fa-archive"></i> Mark selected as filed</a>&nbsp;
        <a href="#" class="btn btn-default" id="collect-signatures"><i class="fa fa-pencil"></i> Collect Signatures</a>
        <div class="waiting hidden"><i class="fa fa-refresh fa-spin fa-fw"></i> Saving changes...</div>
      </th>
      <th><span class="pull-right">Filtered:</span></th>
      <th class="filter-gross"></th>
      <th class="filter-net"></th>
      <th class="filter-sci"></th>
      <th></th>
    </tr>
  </tfoot>
  <tbody>
    @foreach($jobs as $job)
    <tr>
      <td>
        {{ Form::checkbox('jobs[]', $job->id, null) }}&nbsp;
        @if($job->invoiced)
          <span class="text-success"
                data-toggle="tooltip"
                data-placement="top"
                title="Invoiced on: {{$job->invoiced_at->toDateString()}}"
          ><i class="fa fa-dollar"></i></span>&nbsp;
        @endif
        @if($job->filed_at && $job->filed_at->timestamp > 0)
          <span class="text-success"
                data-toggle="tooltip"
                data-placement="top"
                title="Filed on: {{$job->filed_at->toDateString()}}"
          ><i class="fa fa-archive"></i></span>
        @endif
        @if($job->document)
          <span class="text-success"
                data-toggle="tooltip"
                data-placement="top"
                title="Signed on: {{$job->document->created_at->toDateString()}}"
          ><i class="fa fa-pencil"></i></span>&nbsp;
        @endif

      </td>
      <td><a href="{{ route('admin.jobs.show', $job->id) }}">{{ $job->customer->name}}</a></td>
      <td>{{ $job->service->site_name or $job->customer->name }}</td>
      <td>{{ $job->service->type or 'Custom' }}</td>
      <td>@if ($job->employee){{  $job->employee->fullName() }} @else {{ '' }}@endif</td>
      <td>{{ $job->completed_at->toDateString() }}</td>
      <td>
        @if($job->service)
          <span class="label label-default @if($job->service->invoice_type == 'C'){{'label-success'}}
          @elseif($job->service->invoice_type == 'S'){{'label-warning'}}
          @elseif($job->service->invoice_type == 'A'){{'label-info'}}@endif"
                data-toggle="tooltip"
                data-placement="top"
                title="{{$job->service->getInvoiceOption()}}"
          >{{ $job->service->invoice_type }}</span>
        @else
          <span class="label label-default">Custom</span>
        @endif
      </td>
      <td>${{ $job->customer_rate }}</td>
      <td>${{ $job->netValue() }}</td>
      <td>${{ $job->invoiceAmount() }}</td>
      <td><a class="btn btn-circle btn-default pop-up-link" data-id="{{ $job->id }}" data-popup-type="job" href="#"><i class="fa fa-info"></i></a></td>
    </tr>
    @endforeach
  </tbody>
</table>


</div>

@include('partials.pop-up')
@stop