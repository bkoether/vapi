@extends('layouts.employee')

@section('title')
<h1>Invoices <small>{{ Carbon::parse($month)->format('F Y') }}</small></h1>
@stop

@section('bc')
<li class="active">Reports</li>
@stop

@section('content')
<div class="col-sm-6">
  <div class="btn-group">
    <a href="reports/?date={{ Carbon::parse($month)->subMonth()->format('Y-m') }}" type="button" class="btn btn-default"><i class="fa fa-arrow-left"></i> {{ Carbon::parse($month)->subMonth()->format('M Y') }}</a>
    <a href="/reports" type="button" class="btn btn-default"><i class="fa fa-calendar"></i></a>
    <a href="reports/?date={{ Carbon::parse($month)->addMonth()->format('Y-m') }}" type="button" class="btn btn-default">{{ Carbon::parse($month)->addMonth()->format('M Y') }} <i class="fa fa-arrow-right"></i></a>
  </div>
<!--  <a href="#" class="btn btn-primary disabled pull-right"><i class="fa fa-download"></i> Export</a>-->
</div>
<div class="col-sm-6">
  <div class="text-right form-inline" id="jump-form">
    Jump to:
    {{ Form::selectMonth('month', Carbon::now()->format('n'), ['class' => "form-control"]) }}
    {{ Form::selectYear('year', Carbon::now()->subYears(3)->format('Y'), Carbon::now()->format('Y'), Carbon::now()->format('Y'), ['class' => "form-control"]) }}
    {{ Form::button('Go', ['class' => "btn btn-default"]) }}
  </div>
</div>
<p>&nbsp;</p>
<div class="col-sm-12">

  <div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
      <li class="active"><a href="#tab_1" data-toggle="tab">{{ Carbon::parse($part1['from'])->format('M j') }} &mdash; {{ Carbon::parse($part1['to'])->format('j') }}</a></li>
      @if($split)
        <li class=""><a href="#tab_2" data-toggle="tab">{{ Carbon::parse($part2['from'])->format('M j') }} &mdash; {{ Carbon::parse($part2['to'])->format('j') }}</a></li>
      @endif
    </ul>
    <div class="tab-content">
      <div class="tab-pane active" id="tab_1">
        <table class="table table-hover">
              <thead>
                <tr>
                  <th>Site</th>
                  <th>Type</th>
                  <th>Completed</th>
                  <th>My Rate</th>
                  <th>Cash</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                @if (!count($part1['jobs']))
                <tr>
                  <td colspan="6">No data available.</td>
                </tr>
                @endif
                @foreach ($part1['jobs'] as $job)
                <tr>
                  <td>{{ $job->service->site_name or $job->customer->name }}</td>
                  <td>{{ $job->service->type or 'Custom' }}</td>
                  <td>{{ $job->completed_at->toDateString() }}</td>
                  <td>$ {{ $job->employee_rate }}</td>
                  <td>{{ $job->cashAmount()?: "" }}</td>
                  <td>
                    <a class="btn btn-circle btn-default pop-up-link" data-id="{{ $job->id }}" data-popup-type="job" href="#"><i class="fa fa-info"></i></a>
                  </td>
                </tr>
                @endforeach
              </tbody>
              <tfoot>
                @if($part1['total'])
                  @if($part1['commission'])
                    <tr>
                      <td colspan="1"></td>
                      <td colspan="2" class="text-right">Commissions</td>
                      <td colspan="3">$ {{ number_format($part1['commission'], 2) }}</td>

                    </tr>
                  @endif
                <tr>
                  <td colspan="1"></td>
                  <td colspan="2" class="text-right"><strong>Sub Total</strong></td>
                  <td colspan="1"><strong>$ {{ number_format($part1['total'], 2) }}</strong></td>
                  <td colspan="2"><strong>$ {{ number_format(($part1['cash'] / 1.05), 2) }}</strong></td>
                </tr>
                <tr>
                  <td colspan="1"></td>
                  <td colspan="2" class="text-right">GST 5%</td>
                  <td colspan="3">$ {{ $part1['tax'] }}</td>
                </tr>
                <tr>
                  <td colspan="1"></td>
                  <td colspan="2" class="text-right">Less Cash Received (incl. GST)</td>
                  <td colspan="3">($ {{ number_format($part1['cash'], 2) }})</td>
                </tr>
                <tr>
                  <td colspan="1"></td>
                  <td colspan="2" class="text-right"><strong>Total</strong></td>
                  <td colspan="3"><strong>$ {{ $part1['grand_total'] }}</strong></td>
                </tr>
                @endif
              </tfoot>
            </table>
        @if ($part1['invoice'])
            <p>Invoice <strong>{{ $part1['invoice']->number }}</strong> submitted on {{ $part1['invoice']->created_at->toDayDateTimeString() }}</p>
        @elseif($part1['total'])
          {{ Form::open(['action' => 'UserEmployeeController@submitInvoice', 'method' => 'post']) }}
            {{ Form::label('invoice_number', 'Invoice Number:', ['class' => 'control-label']) }}
            {{ Form::text('invoice_number', null) }}
            {{ Form::hidden('month', $month) }}
            {{ Form::hidden('month_part', 1) }}
            {{ Form::hidden('employee_id', $employee->id) }}
            {{ Form::submit('Submit Invoice', ['class' => 'btn btn-primary']) }}
          {{ Form::close() }}
        @endif
      </div><!-- /.tab-pane -->
      @if ($split)
        <div class="tab-pane" id="tab_2">
          <table class="table table-hover">
            <thead>
              <tr>
                <th>Site</th>
                <th>Type</th>
                <th>Completed</th>
                <th>My Rate</th>
                <th>Cash</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              @if (!count($part2['jobs']))
              <tr>
                <td colspan="6">No data available.</td>
              </tr>
              @endif
              @foreach ($part2['jobs'] as $job)
              <tr>
                <td>{{ $job->service->site_name or $job->customer->name }}</td>
                <td>{{ $job->service->type or 'Custom' }}</td>
                <td>{{ $job->completed_at->toDateString() }}</td>
                <td>$ {{ $job->employee_rate }}</td>
                <td>{{ $job->cashAmount()?: "" }}</td>
                <td>
                  <a class="btn btn-circle btn-default pop-up-link" data-id="{{ $job->id }}" data-popup-type="job" href="#"><i class="fa fa-info"></i></a>
                </td>
              </tr>
              @endforeach
            </tbody>
            <tfoot>
              @if($part2['total'])
                @if($part2['commission'])
                  <tr>
                    <td colspan="1"></td>
                    <td colspan="2" class="text-right">Commissions</td>
                    <td colspan="3">$ {{ number_format($part2['commission'], 2) }}</td>

                  </tr>
                @endif
              <tr>
                <td colspan="1"></td>
                <td colspan="2" class="text-right"><strong>Sub Total</strong></td>
                <td colspan="1"><strong>$ {{ number_format($part2['total'], 2) }}</strong></td>
                <td colspan="2"><strong>$ {{ number_format(($part2['cash']/1.05), 2) }}</strong></td>
              </tr>
              <tr>
                <td colspan="1"></td>
                <td colspan="2" class="text-right">GST 5%</td>
                <td colspan="3">$ {{ $part2['tax'] }}</td>
              </tr>
              <tr>
                <td colspan="1"></td>
                <td colspan="2" class="text-right">Less Cash Received (incl. GST)</td>
                <td colspan="3">($ {{ number_format($part2['cash'], 2) }})</td>
              </tr>
              <tr>
                <td colspan="1"></td>
                <td colspan="2"  class="text-right"><strong>Total</strong></td>
                <td colspan="3"><strong>$ {{ $part2['grand_total'] }}</strong></td>
              </tr>
              @endif
            </tfoot>
          </table>
          @if ($part2['invoice'])
            <p>Invoice <strong>{{ $part2['invoice']->number }}</strong> submitted on {{ $part2['invoice']->created_at->toDayDateTimeString() }}</p>
          @elseif($part2['total'])
            {{ Form::open(['action' => 'UserEmployeeController@submitInvoice', 'method' => 'post']) }}
              {{ Form::label('invoice_number', 'Invoice Number:', ['class' => 'control-label']) }}
              {{ Form::text('invoice_number', null) }}
              {{ Form::hidden('month', $month) }}
              {{ Form::hidden('month_part', 2) }}
              {{ Form::hidden('employee_id', $employee->id) }}
              {{ Form::submit('Submit Invoice', ['class' => 'btn btn-primary']) }}
            {{ Form::close() }}
          @endif
        </div><!-- /.tab-pane -->
      @endif

    </div><!-- /.tab-content -->
  </div>

</div>

@include('partials.pop-up')
@stop