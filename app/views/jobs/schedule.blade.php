@extends('layouts.employee')

@section('title')
    <h1>Schedule</h1>
@stop

@section('bc')
@stop



@section('content')
    <div class="col-md-12 col-lg-9">

        <div class="box box-primary">
            <div class="box-body no-padding">
                <!-- THE CALENDAR -->
                <div id="calendar"></div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /. box -->



    </div>

    <div class="col-sm-12 col-md-12 col-lg-3">
        <p>
            <a class="btn btn-block btn-primary submit-jobs new-jobs hidden">Save changes</a>
        </p>
        <div class="callout callout-info hidden" id="loader-msg">
            <h4><i class="fa fa-refresh fa-spin fa-fw"></i> Saving changes...</h4>
        </div>


        @if (count($to_schedule))
            <div class="box box-solid">
                <div class="box-header">
                    <h3 class="box-title">Jobs to schedule</h3>

                    <div class="box-tools pull-right">
                        <button class="btn btn-default btn-sm"
                                data-widget="collapse"><i
                                    class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body" id="job-box">
                    <p class="btn-group">
                        <span class="btn btn-default sort"
                              data-sort="name">Name</span>
                        {{--<span class="btn btn-default sort" data-sort="route">Route</span>--}}
                    </p>

                    <p class="btn-group pull-right">
                        <span class="btn btn-default scroll down"><i
                                    class="fa fa-chevron-down"></i></span>
                        <span class="btn btn-default scroll up"><i
                                    class="fa fa-chevron-up"></i></span>
                    </p>
                    <ul class="list-group list">

                        @foreach ($to_schedule as $job)
                            <li class="list-group-item"
                                data-due-start="{{ $job->due_start->copy()->subDay()->toDateString() }}"
                                data-due-end="{{ $job->due_end->copy()->addDay()->toDateString() }}"
                                data-job-id="{{ $job->id }}"
                                data-priority="{{ $job->priority_task or 0 }}"
                                data-job-duration="{{ $job->duration or 0 }}"
                                data-job-title="{{ $job->service->site_name or $job->customer->name }} ({{ $job->service->type or 'Custom' }})"
                                data-job-description="{{ $job->service->type or 'Custom' }}<br>Due: {{ $job->due_start->format('M j') }} - {{ $job->due_end->format('M j, Y') }}">
                                <a class="pull-right btn btn-circle btn-default pop-up-link"
                                   data-id="{{ $job->id }}"
                                   data-popup-type="job" href="#"><i
                                            class="fa fa-info"></i></a>
                                {{--@if($job->name)--}}
                                    {{--<span class="label label-primary route">{{ $job->name }}</span>--}}
                                    {{--<br/>--}}
                                {{--@else--}}
                                    {{--<span class="route hidden">zz</span>--}}
                                {{--@endif--}}
                                @if($job->priority_task) <i
                                        class="fa fa-warning text-danger"></i>@endif
                                <span class="name">{{ $job->service->site_name or $job->customer->name }}</span>
                                ({{ $job->service->type or 'Custom' }})<br/>
                                <small>
                                    {{ ($job->service ? $job->service->getFrequencyString() : '') }}
                                    <br/>
                                    {{ ($job->last_completed ? 'Last: ' . $job->last_completed->format('M j, Y') . ' - ' : '') }}
                                    Due: {{ $job->due_end->format('M j, Y') }}
                                </small>
                            </li>
                        @endforeach
                    </ul>

                </div>
            </div>
        @endif
    </div>


    @include('partials.pop-up')
@stop
