@extends('layouts.admin')

@section('title')
<h1>Jobs <small>Upcoming</small></h1>
@stop

@section('bc')
<li class="active">Jobs</li>
@stop


@section('content')

<div class="col-sm-12">

  <div class="box box-solid">
    <div class="box-header">
      <h3 class="box-title">Filter</h3>
      <div class="box-tools pull-right">
        <button class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body">
      {{ Form::open(['path' => 'jobs', 'method' => 'get']) }}
      <div class="row">
        <div class="form-group col-sm-6">
          {{ Form::label('date', 'Date') }}
          <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            {{ Form::text('date', $date, ['class' => 'form-control rangepick']) }}
          </div>
        </div>
        <div class="form-group col-sm-6">
          {{ Form::label('date_filter', 'Filter Date') }}
          {{ Form::select('date_filter', select_lists('date_filter') , $date_filter , ['class' => 'form-control']) }}
        </div>

        <div class="form-group col-sm-4">
          {{ Form::label('chain', 'Chain') }}
          {{ Form::select('chain', select_lists('reports_chain', 'All') , $chain , ['class' => 'form-control']) }}
        </div>

        <div class="form-group col-sm-4">
          {{ Form::label('customer', 'Customer') }}
          {{ Form::select('customer', select_lists('reports_customer', 'All') , $customer , ['class' => 'form-control']) }}
        </div>
        <div class="form-group col-sm-4">
          {{ Form::label('employee', 'Sub-Contractor') }}
          {{ Form::select('employee', select_lists('reports_employees', 'All') , $employee , ['class' => 'form-control']) }}
        </div>
        <div class="col-sm-12">
          <div class="pull-right">
            {{ Form::submit('Filter', ['class' => 'btn btn-default']) }}
          </div>

        </div>

      </div>
      {{ Form::close() }}
    </div>
  </div>




{{ Form::open(['action' => 'JobsController@trash', 'method' => 'post']) }}


  <table class="table table-hover data-tables dt-big2">
    <thead>
      <tr>
        <th><input type="checkbox" id="jobs-select-all"/></th>
        <th>Site</th>
        <th>Type</th>
        <th>Sub-Contractor</th>
        <th>Due Latest</th>
        <th>Scheduled</th>
        <th>Completed</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      @foreach($jobs as $job)
      <tr>
        <td>{{ Form::checkbox('jobs[]', $job->id, null) }}</td>
        <td>
            <a href="{{ route('admin.jobs.show', $job->id) }}">{{ $job->service->site_name or $job->customer->name }}</a>
            @if($job->priority_task)
                <i class="fa fa-warning text-danger"></i>
            @endif
        </td>
        <td>{{ $job->service->type or 'Custom' }}</td>
        <td>@if ($job->employee){{  $job->employee->fullName() }} @else {{ '<span class="label label-warning">Not assigned</span>' }}@endif</td>
        <td>{{ $job->due_end->toDateString() }}</td>
        <td>{{ $job->status('scheduled') }}</td>
        <td>{{ $job->status() }}</td>
        <td><a class="btn btn-circle btn-default pop-up-link" data-id="{{ $job->id }}" data-popup-type="job" href="#"><i class="fa fa-info"></i></a></td>
      </tr>
      @endforeach
    </tbody>
  </table>

  {{ Form::submit('Remove Jobs', ['class' => 'btn btn-default']) }}
</div>

{{ Form::close() }}

@include('partials.pop-up')
@stop