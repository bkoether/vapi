@extends('layouts.admin')

@section('title')
    <h1>
        {{ $job->service->site_name or 'Custom Job' }}

    </h1>
@stop

@section('bc')
    <li><a href="{{ route('admin.jobs.index') }}">Jobs</a></li>
    <li class="active">Job Details</li>
@stop


@section('content')
    {{--<pre>{{ print_r($job->toArray(),1) }}</pre>{{die;}}--}}
    <div class="col-sm-8">
        <div class="box box-solid">
            <div class="box-header">
                <h3 class="box-title">Details</h3>

                <div class="box-tools pull-right">
                    <button class="btn btn-default btn-sm"
                            data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">

                <div class="list-group">
                    <div class="list-group-item active">
                        <h4 class="list-group-item-heading">Dates</h4>

                        <div class="list-group-item-text">
                            @if($job->priority_task)
                                <p><span class="label label-danger"><i class="fa fa-warning"></i> HIGH PRIORITY JOB</span></p>
                            @endif
                            @if($job->completed && !$job->cancelled)
                                <p>
                                    <span class="label label-success">Completed</span> {{ date('l, M j Y', strtotime($job->completed_at)) }}
                                </p>
                            @elseif($job->cancelled)
                                <p>
                                    <span class="label label-warning">Cancelled</span>
                                    on {{ date('l, M j Y', strtotime($job->completed_at)) }}
                                </p>
                            @else
                                <p>
                                    @if($job->scheduled) Scheduled
                                    for {{ $job->scheduled_at->format('l, M j Y') }} @else
                                        <span class="label label-warning">Not scheduled</span> @endif
                                    <span class="pull-right">
                                        @if($job->last_completed)Last done: {{ $job->last_completed->format('M j Y') }} / @endif
                                        Due latest: {{ $job->due_end->format('M j Y') }}
                                    </span>
                                </p>
                            @endif
                                <br/>Time required: {{ $job->duration }} {{ str_plural('day', $job->duration) }}
                        </div>
                    </div>

                    @if ($job->service_id)
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">Service</h4>

                            <p class="list-group-item-text">{{ link_to_route('admin.services.show', $job->service->getTypeName(), [$job->service->id]) }}
                        </div>
                    @endif


                    <div class="list-group-item">
                        <h4 class="list-group-item-heading">Customer</h4>

                        <p class="list-group-item-text">{{ link_to_route('admin.customers.show', $job->customer->name, [$job->customer->id]) }}
                            <span class="pull-right">Rate: ${{ $job->customer_rate }}</span>
                        </p>
                    </div>


                    <div class="list-group-item">
                        <h4 class="list-group-item-heading">Sub-Contractor</h4>

                        <p class="list-group-item-text">
                            @if(empty($job->employee))
                                <span class="label label-warning">Not assigned</span>
                            @else
                                {{ link_to_route('admin.employees.show', $job->employee->company, [$job->employee->id]) }}
                                <span class="pull-right">Rate: ${{ $job->employee_rate }}</span>
                            @endif
                        </p>
                    </div>

                    <div class="list-group-item">
                        <h4 class="list-group-item-heading">Payment</h4>

                        <div class="list-group-item-text">
                            @if ($job->service_id)
                                {{ $job->service->getInvoiceOption() }} @if($job->service->signed_workorder)
                                    (signed work order required) @endif
                            @endif
                            &nbsp;
                            <div class="pull-right">
                                @if($job->invoiced)
                                    <span class="label label-success">Invoiced</span> {{$job->invoiced_at->toDateString()}}
                                @else
                                    <span class="label label-default">Not invoiced</span>
                                @endif
                                @if($job->filed_at && $job->filed_at->timestamp > 0)
                                    <br/><span class="label label-success">Filed</span> {{$job->filed_at->toDateString()}}
                                @else
                                    <br/><span class="label label-default">Not filed</span>
                                @endif
                            </div>
                            <div class="clearfix">&nbsp;</div>
                        </div>
                    </div>

                    @if(!empty($job->description_override))
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">Description</h4>

                            <div class="list-group-item-text">{{ combine_description($job, 'job') }}</div>
                        </div>
                    @endif

                    @if ($job->document)
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">Signature</h4>
                            <div class="list-group-item-text">
                                <p><img src="{{ $job->document->file->url() }}" alt="" style="width: 50%; height: auto"></p>
                                <p>{{ $job->document->signature_name }} on {{ $job->document->created_at->toDayDateTimeString() }}</p>
                            </div>
                        </div>
                    @endif

                </div>


            </div>
            <div class="box-footer clearfix">
                <a class="pull-right"
                   href="{{ route('admin.jobs.edit', [$job->id]) }}"><i
                            class="fa fa-edit"></i> Edit Job Details</a>
            </div>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="box box-solid">
            <div class="box-header">
                <h3 class="box-title">Comments</h3>

                <div class="box-tools pull-right">
                    <button class="btn btn-default btn-sm"
                            data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                @if($job->employee_comment)
                    <h4>Sub-Contractor</h4>
                    <blockquote>
                        <p>{{ nl2br($job->employee_comment) }}</p>
                    </blockquote>
                @endif

                @if($job->admin_comment)
                    <h4>Admin</h4>
                    <blockquote>
                        <p>{{ nl2br($job->admin_comment) }}</p>
                    </blockquote>
                @endif
            </div>
        </div>

        <div class="box box-solid box-primary collapsed-box">
            <div class="box-header">
                <h3 class="box-title">Quick Edit</h3>

                <div class="box-tools pull-right">
                    <button class="btn btn-default btn-sm"
                            data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body" style="display: none;">
                {{ Form::open(['route' => ['admin.jobs.update', $job->id], 'method' => 'PUT']) }}
                @if(!$job->invoiced)
                    <div class="form-group">
                        {{ Form::checkbox('markInvoiced') }}
                        {{ Form::label('markInvoiced', 'Mark as invoiced') }}
                    </div>
                @endif

                <div class="form-group">
                    {{ Form::label('adminComment', 'Comment') }}
                    {{ Form::textarea('adminComment',  $job->admin_comment, ['class' => 'form-control']) }}
                </div>

                {{ Form::submit('Save', ['class' => 'btn btn-default']) }}

                {{ Form::close() }}


            </div>
        </div>

    </div>


@stop


