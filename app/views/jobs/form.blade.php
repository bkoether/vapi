@extends('layouts.admin')


@section('title')
@if($new)
  <h1>New <i>{{ $service->type or 'Custom'}}</i> Job <small>for {{ $customer->name }}</small></h1>
@else
  <h1>{{ $job->service->type or 'Custom Job' }} <small>{{ $job->customer->name }}</small></h1>
@endif
@stop



@section('bc')
<li>{{ link_to_route('admin.jobs.index', 'Jobs') }}</li>
<li class="active">{{ $new ? 'New' : 'Edit' }} Job</li>
@stop


@section('content')
@if ($new)
  {{ Form::model($job, ['route' => 'admin.jobs.store']) }}
  {{ Form::hidden('qstring', Request::getQueryString()) }}
@else
  {{ Form::model($job, ['route' => ['admin.jobs.update', $job->id], 'method' => 'PUT']) }}
@endif
{{ Form::hidden('_destination', URL::previous()) }}

<div class="form-group col-sm-12">
    <h4>
        <label class="text-uppercase">
            {{ Form::checkbox('priority_task', '1', null,  ['id' => 'priority_task']) }}
            High Priority Job
        </label>
    </h4>
</div>

<div class="col-sm-5">
  <div class="form-group">
    {{ Form::label('cust_name', 'Customer') }}
    {{ Form::hidden('customer_id', null) }}
    {{ Form::text('cust_name',  $customer->name, ['class' => 'form-control', 'disabled' => 'true']) }}
  </div>

  <div class="form-group">
    {{ Form::label('s_type', 'Service') }}
    {{ Form::hidden('service_id', null) }}
    {{ Form::text('s_type',  (isset($service) ? $service->type : 'Custom') , ['class' => 'form-control', 'disabled' => 'true']) }}
  </div>

  <div class="form-group {{ $errors->has('employee_id') ? 'has-error' : ''}}">
    {{ Form::label('employee_id', 'Sub-Contractor') }}
    {{ Form::select('employee_id', select_lists('employees') , null , ['class' => 'form-control']) }}
  </div>
</div>

<div class="col-sm-7">
  <div class="row">
    <div class="form-group col-xs-6 col-sm-3 {{ $errors->has('customer_rate') ? 'has-error' : ''}}">
      {{ Form::label('customer_rate', 'Customer Rate') }}
      <div class="input-group">
        <span class="input-group-addon">$</span>
        {{ Form::text('customer_rate',  null, ['class' => 'form-control']) }}
      </div>
    </div>

    <div class="form-group col-xs-6 col-sm-3 {{ $errors->has('employee_rate') ? 'has-error' : ''}}">
      {{ Form::label('employee_rate', 'Sub-Contractor Rate') }}
      <div class="input-group">
        <span class="input-group-addon">$</span>
        {{ Form::text('employee_rate',  null, ['class' => 'form-control']) }}
      </div>
    </div>

    <div class="form-group col-xs-6 col-sm-3 {{ $errors->has('invoiced_at') ? 'has-error' : ''}}">
      {{ Form::label('invoiced_at', 'Invoiced At') }}
      <div class="input-group">
        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
        {{ Form::text('invoiced_at',  ($job->invoiced ? $job->invoiced_at->toDateString() : ''), ['class' => 'form-control dpick']) }}
      </div>
    </div>

    <div class="form-group col-xs-6 col-sm-3 {{ $errors->has('filed_at') ? 'has-error' : ''}}">
      {{ Form::label('filed_at', 'Filed At') }}
      <div class="input-group">
        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
        {{ Form::text('filed_at',  ($job->filed_at && $job->filed_at->timestamp > 0 ? $job->filed_at->toDateString() : ''), ['class' => 'form-control dpick']) }}
      </div>
    </div>

  </div>
</div>

<div class="col-sm-7">
  <div class="row">
    <div class="form-group col-xs-4 {{ $errors->has('duration') ? 'has-error' : ''}}">
      {{ Form::label('duration', 'Duration (days)') }}
      {{Form::select('duration', array_combine(range(1, 10), range(1, 10)), NULL, ['class' => 'form-control']) }}
    </div>

    <div class="form-group col-xs-4 {{ $errors->has('due_start') ? 'has-error' : ''}}">
      {{ Form::label('due_start', 'Due From') }}
      <div class="input-group">
        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
        {{ Form::text('due_start',  ($job->due_start ? $job->due_start->toDateString() : null), ['class' => 'form-control dpick', 'disabled' => (isset($job->service->type) ? 'true' : 'false')]) }}
      </div>
    </div>

    <div class="form-group col-xs-4 {{ $errors->has('due_end') ? 'has-error' : ''}}">
      {{ Form::label('due_end', 'Due Latest') }}
      <div class="input-group">
        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
        {{ Form::text('due_end',  ($job->due_end ? $job->due_end->toDateString() : null), ['class' => 'form-control dpick']) }}
      </div>
    </div>

    <div class="form-group col-xs-6 {{ $errors->has('scheduled_at') ? 'has-error' : ''}}">
      {{ Form::label('scheduled_at', 'Scheduled For') }}
      <div class="input-group">
        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
        {{ Form::text('scheduled_at',  ($job->scheduled ? $job->scheduled_at->toDateString() : ''), ['class' => 'form-control dpick']) }}
      </div>
    </div>

    <div class="form-group col-xs-6 {{ $errors->has('completed_at') ? 'has-error' : ''}}">
      {{ Form::label('completed_at', 'Completed On') }}
      <div class="input-group">
        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
        {{ Form::text('completed_at',  ($job->completed ? $job->completed_at->toDateString() : ''), ['class' => 'form-control dpick']) }}
      </div>
    </div>

  </div>
</div>

<div class="col-sm-6">
  <div class="form-group {{ $errors->has('employee_comment') ? 'has-error' : ''}}">
    {{ Form::label('employee_comment', 'Sub-Contractor Comment') }}
    {{ Form::textarea('employee_comment',  null, ['class' => 'form-control', 'rows' => 4]) }}
  </div>
  <div class="form-group {{ $errors->has('admin_comment') ? 'has-error' : ''}}">
    {{ Form::label('admin_comment', 'Admin Comment') }}
    {{ Form::textarea('admin_comment',  null, ['class' => 'form-control', 'rows' => 4]) }}
  </div>
</div>

<div class="form-group col-sm-6 {{ $errors->has('description_override') ? 'has-error' : ''}}">
  {{ Form::label('description_override', 'Additional Description') }}
  {{ Form::textarea('description_override',  null, ['class' => 'form-control']) }}
</div>

<div class="col-sm-12">

@if ($new)
  {{ Form::submit('Save', ['class' => 'btn btn-primary']) }}
@else
  {{ Form::submit('Save Changes', ['class' => 'btn btn-primary']) }}
  <a class="btn btn-small btn-danger pull-right" href="#" data-toggle="modal" data-target="#delete-user-modal">Delete Job</a>
  @endif
</div>

{{ Form::close() }}

@if(!$new)
  @include('partials.deleteForm', ['title' => 'Delete Job', 'text' => 'Are you sure you want to delete this job?', 'route' => 'admin.jobs.destroy', 'id' => $job->id])
@endif

@stop