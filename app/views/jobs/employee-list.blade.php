@extends('layouts.employee')

@section('title')
<h1>Jobs & Services </h1>
@stop

@section('bc')
@stop


@section('content')

<div class="col-sm-6">
  <div class="box box-solid">
    <div class="box-header">
      <h3 class="box-title">Upcoming Jobs</h3>
      <div class="box-tools pull-right">
        <button class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body no-padding">

      <table class="table table-condensed">
        <tbody>
          @foreach($jobs as $job)
          <tr>
            <td>
                <a href="{{ action('UserEmployeeController@jobView', $job->id)}}">{{ $job->service->site_name or $job->customer->name}}</a>
                @if($job->priority_task)
                    <i class="fa fa-warning text-danger"></i>
                @endif
            </td>
            <td>{{ $job->service->type or 'Custom' }}</td>
            <td>
              @if ($job->scheduled)
                {{ $job->scheduled_at->toDateString() }}
              @elseif ( $job->scheduled && $job->scheduled_at->lt(Carbon::now()->startOfDay()) )
                {{ $job->scheduled_at->toDateString() }} <span class="label label-danger">Overdue</span>
              @else
                <span class="label label-warning">Not scheduled yet</span>
              @endif
            </td>
            <td><a class="btn btn-circle btn-default pop-up-link" data-id="{{ $job->id }}" data-popup-type="job" href="#"><i class="fa fa-info"></i></a></td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>

    <div class="box-footer clearfix">
      <a href="{{ action('UserEmployeeController@reports')}}"><i class="fa fa-file-text-o"></i> Completed Jobs</a>
    </div>
  </div>
</div>


<div class="col-sm-6">
  <div class="box box-solid">
    <div class="box-header">
      <h3 class="box-title">Services</h3>
      <div class="box-tools pull-right">
        <button class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body no-padding">

      <table class="table table-condensed">
        <tbody>
          @foreach($services as $service)
          <tr>
            <td><a href="{{ action('UserEmployeeController@serviceView', $service->id)}}">{{ $service->site_name}}</a></td>
            <td>{{ $service->type }}</td>
            <td>{{ $service->getFrequencyString() }}</td>
            <td>${{ $service->employee_rate }}</td>
            <td><a class="btn btn-circle btn-default pop-up-link" data-id="{{ $service->id }}" data-popup-type="service" href="#"><i class="fa fa-info"></i></a></td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>

  </div>
</div>

@include('partials.pop-up')
@stop