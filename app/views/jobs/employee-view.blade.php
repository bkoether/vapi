@extends('layouts.employee')

@section('title')
  <h1>
    {{ $job->service->site_name or $job->customer->name }}
    <small>{{ ($job->service ? $job->service->getTypeName() : 'Custom') }}</small>
  </h1>
@stop

@section('bc')
  <li><a href="{{ action('UserEmployeeController@jobs') }}">Jobs</a></li>
@stop


@section('content')

<div class="col-sm-6">
  <div class="list-group">

    <div class="list-group-item">
      <h4 class="list-group-item-heading">Dates</h4>
      <div class="list-group-item-text">
          @if($job->priority_task)
              <p><span class="label label-danger"><i class="fa fa-warning"></i> HIGH PRIORITY JOB</span></p>
          @endif
        <p>
          @if ($job->completed && !$job->cancelled)
            Completed on {{ $job->completed_at->format('l, M j Y') }}
          @elseif($job->cancelled)
            Cancelled on {{ $job->completed_at->format('l, M j Y') }}
          @else
            @if($job->scheduled)
              Scheduled for {{ $job->scheduled_at->format('l, M j Y') }}
              @if ($job->scheduled_at->lt(Carbon::now()->startOfDay()) && !$job->completed)
                <span class="label label-danger">Overdue</span>
              @endif
            @else
              <span class="label label-warning">Not scheduled</span>
            @endif
            <br/>
              @if($job->last_completed)Last done: {{ $job->last_completed->format('M j Y') }}<br/>@endif
              <span>Due latest: {{ $job->due_end->format('M j Y') }}</span>
          @endif
            <br/>Time required: {{ $job->duration }} {{ str_plural('day', $job->duration) }}
        </p>
      </div>
    </div>

    <div class="list-group-item">
      <h4 class="list-group-item-heading">Payment/Invoice</h4>
      <div class="list-group-item-text">
        <div class="pull-right">
          My rate: ${{ $job->employee_rate }}<br/>
          @if ($job->service && $job->service->invoice_type != 'O')
          Client rate: ${{ $job->customer_rate }}
          @endif
        </div>

        <span class="label label-default">{{ ($job->service ? $job->service->getInvoiceOption() : '') }}</span><br/>
        @if($job->service && $job->service->workorder_required)
          <strong>Signed work order required</strong><br/>
        @endif
        @if($job->service && $job->service->signature_required)
          <strong>Signature on receipt required</strong>
        @endif
        <p></p>
      </div>
    </div>


    <div class="list-group-item">
      <h4 class="list-group-item-heading">Description</h4>
      <div class="list-group-item-text">
        {{ combine_description($job, 'job') }}
        {{--@if($job->service)--}}
{{--          {{ $job->service->description }}--}}
        {{--@endif--}}
{{--        {{ $job->description_override }}--}}
      </div>
    </div>

    @if ($job->employee_comment)
      <div class="list-group-item">
        <h4 class="list-group-item-heading">My Comment</h4>
        <div class="list-group-item-text">
          {{ $job->employee_comment }}
        </div>
      </div>
    @endif

    {{--@if ($job->admin_comment)--}}
      {{--<div class="list-group-item">--}}
        {{--<h4 class="list-group-item-heading">Admin Comment</h4>--}}
        {{--<div class="list-group-item-text">--}}
          {{--{{ $job->admin_comment }}--}}
        {{--</div>--}}
      {{--</div>--}}
    {{--@endif--}}


    <div class="list-group-item">
      <h4 class="list-group-item-heading">Signature</h4>
      <div class="list-group-item-text">
        @if ($job->document)
          <p><img src="{{ $job->document->file->url() }}" alt="" style="width: 100%; height: auto"></p>
          <p>{{ $job->document->signature_name }} on {{ $job->document->created_at->toDayDateTimeString() }}</p>
        @else
          <a href="{{ route('jobs.signature.create', [$job->id]) }}">Collect Signature</a>
        @endif
      </div>
    </div>



  </div>
  @if ($job->service)
  <div class="row">
    @foreach ($job->service->locationImages as $img)
    <div class="col-xs-6 col-sm-4"><a href="{{ asset($img->photo->url()) }}" class="thumbnail lb"><img src="{{ asset($img->photo->url('thumbnail')) }}" /></a></div>
    @endforeach
  </div>
  @endif

</div>

<div class="col-sm-6">
  <div class="box box-solid">
    <div class="box-header">
      <h3 class="box-title">Contact</h3>
      <div class="box-tools pull-right">
        <button class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body">
      <p class="lead">&nbsp;<i class="fa fa-user"></i> {{ $job->customer->contact_name }}</p>
      <p class="lead">&nbsp;<i class="fa fa-phone"></i> <a href="tel:+1{{ $job->customer->phone1 }}">{{ $job->customer->phone1 }}</a></p>
      @if(!empty($job->customer->phone2))
      <p class="lead">&nbsp;<i class="fa fa-phone"></i> <a href="tel:+1{{ $job->customer->phone2 }}">{{ $job->customer->phone2 }}</a></p>
      @endif
      <p class="lead">&nbsp;<i class="fa fa-envelope"></i> <a href="mailto:{{ $job->customer->email }}">{{ $job->customer->email }}</a></p>
    </div>
  </div>

  <div class="box box-solid">
    <div class="box-header">
      <h3 class="box-title"><i class="fa fa-map-marker"></i> Address</h3>
      <div class="box-tools pull-right">
        <button class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body clearfix">
      <div id="gmap" class="col-sm-6" data-address="{{ $job->service->address or '' }}"></div>
    </div>
    <div class="box-footer">{{ nl2br(($job->service ? $job->service->address: '')) }}</div>
  </div>
</div>

@stop


