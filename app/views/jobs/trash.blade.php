@extends('layouts.admin')

@section('title')
<h1>Remove Jobs</h1>
@stop

@section('bc')
<li><a href="{{ route('admin.jobs.index') }}">Jobs</a></li>
<li class="active">Remove Jobs</li>
@stop

@section('content')
{{ Form::open(['action' => 'JobsController@trashConfirm', 'method' => 'post']) }}
  <div class="col-sm-8">

    <div class="callout callout-danger">
      <h4>The following jobs are to be deleted/cancelled:</h4>
      <ul>
        @foreach($jobs as $job)
          <li>{{ $job->service->site_name or $job->customer->name }} ({{ $job->customer->name}})</li>
          {{ Form::hidden('jobs[]', $job->id) }}
        @endforeach
      </ul>
    </div>

  </div>

  <div class="col-sm-4">
    <input name="delete" class="btn btn-block btn-danger" type="submit" value="Delete Jobs">
    <input name="cancel" class="btn btn-block btn-warning" type="submit" value="Cancel Jobs">
  </div>
{{ Form::close() }}
@stop