@extends('layouts.admin')

@section('title')
<h1>Services</h1>
@stop

@section('bc')
@stop

@section('content')
<div class="col-sm-12">
  <p><a class="btn-sm btn-primary" href="{{ route('admin.services.create') }}"><i class="fa fa-plus"></i> Add Service</a></p>
  <table class="table table-hover data-tables dt-big">
    <thead>
      <tr>
        <th>Site</th>
        <th>Type</th>
        <th>Sub-Contractor</th>
        <th>Description</th>
        <th>Frequency</th>
        <th>Status</th>
        <th>&nbsp;</th>
      </tr>
    </thead>
    <tbody>
      @foreach($services as $service)
      <tr>
        <td><a href="{{ route('admin.services.show', $service->id) }}">{{ $service->site_name }}</a></td>
        <td>{{ $service->getTypeCode() }}</td>
        <td>@if ($service->employee){{  $service->employee->fullName() }} @else {{ '<span class="label label-warning">Not assigned</span>' }}@endif</td>
        <td>{{ Str::words($service->description, 4) }}</td>
        <td>{{ $service->getFrequencyString() }}</td>
        <td><span class="label label-{{ $service->status ? 'success' : 'danger' }}">{{ $service->status ? 'active' : 'disabled' }}</span></td>
        <td><a class="btn btn-circle btn-default pop-up-link" data-id="{{ $service->id }}" data-popup-type="service" href="#"><i class="fa fa-info"></i></a></td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>
@include('partials.pop-up')
@stop