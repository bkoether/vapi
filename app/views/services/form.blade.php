@extends('layouts.admin')


@section('title')
    @if($new)
        <h1>New Service</h1>
    @else
        <h1>{{ $service->getTypeName() }}
            <small>{{ $service->customer->name or 'NO CUSTOMER SET' }}</small>
        </h1>
    @endif
@stop



@section('bc')
    <li>{{ link_to_route('admin.services.index', 'Services') }}</li>
    @if ($new)
        <li class="active">New Service</li>
    @elseif (isset($service->customer))
        <li class="active">{{ link_to_route('admin.services.show', $service->customer->name, $service->id) }}</li>
    @endif
@stop


@section('content')

    @if($new)
        {{ Form::model($service, ['route' => 'admin.services.store', 'files' => TRUE]) }}
    @else
        {{ Form::model($service, ['route' => ['admin.services.update', $service->id], 'method' => 'put', 'files' => TRUE]) }}
    @endif

    {{ Form::hidden('_destination', URL::previous()) }}

    <div class="form-group col-sm-12">
        {{ Form::checkbox('status') }}
        {{ Form::label('status', ' Service Active') }}
    </div>

    <div class="col-sm-7">
        <div class="form-group {{ $errors->has('customer_id') ? 'has-error' : ''}}">
            {{ Form::label('customer_id', 'Customer') }}
            {{ Form::select('customer_id', select_lists('customers'),  NULL, ['class' => 'form-control']) }}
            <p class="help-block">Default Rate: $<span id="customerRate"></span>
            </p>
        </div>

        <div class="form-group {{ $errors->has('site_name') ? 'has-error' : ''}}">
            {{ Form::label('site_name', 'Site Name') }}
            {{ Form::text('site_name',  NULL, ['class' => 'form-control']) }}
        </div>

        <div class="row">
            <div class="form-group col-xs-6 {{ $errors->has('type') ? 'has-error' : ''}}">
                {{ Form::label('type', 'Type') }}
                {{ Form::select('type', $serviceTypes,  NULL, ['class' => 'form-control']) }}
            </div>
            <div class="form-group col-xs-6 {{ $errors->has('store_number') ? 'has-error' : ''}}">
                {{ Form::label('store_number', 'Store Number') }}
                {{ Form::text('store_number',  NULL, ['class' => 'form-control']) }}
            </div>

            <div class="form-group col-xs-6 {{ $errors->has('duration') ? 'has-error' : ''}}">
                {{ Form::label('duration', 'Duration (days)') }}
                {{Form::select('duration', array_combine(range(1, 10), range(1, 10)), NULL, ['class' => 'form-control']) }}
            </div>
            <div class="form-group col-xs-6 {{ $errors->has('frequency') ? 'has-error' : ''}}">
                {{ Form::label('frequency', 'Frequency') }}
                {{Form::select('frequency', $service->frequencyOptions, NULL, ['class' => 'form-control']) }}
            </div>
            <div class="form-group col-xs-6">
                {{ Form::label('schedule_ahead', 'Schedule ahead (months)') }}
                {{ Form::selectRange('schedule_ahead', 1, 12,  NULL, ['class' => 'form-control']) }}
            </div>
            <div class="form-group col-xs-6">
                {{ Form::label('next_schedule ', 'Next schedule date') }}
                <div class="input-group">
                <span class="input-group-addon"><i
                            class="fa fa-calendar"></i></span>
                    {{ Form::text('next_schedule', (($service->next_schedule && $service->next_schedule->timestamp > 0) ? $service->next_schedule->toDateString() : ''), ['class' => 'form-control dpick']) }}
                </div>
            </div>
        </div>




        <div class="form-group {{ $errors->has('employee_id') ? 'has-error' : ''}}">
            {{ Form::label('employee_id', 'Sub-Contractor') }}
            {{ Form::select('employee_id', select_lists('employees'),  NULL, ['class' => 'form-control']) }}
            <p class="help-block">Default Rate: $<span id="employeeRate"></span>
            </p>
        </div>

    </div>

    <div class="col-sm-5">
        <div class="box box-solid">
            <div class="box-body">

                <div class="form-group {{ $errors->has('customer_rate') ? 'has-error' : ''}}">
                    {{ Form::label('customer_rate', 'Customer Rate') }}
                    <div class="input-group">
                        <span class="input-group-addon">$</span>
                        {{ Form::text('customer_rate',  NULL, ['class' => 'form-control']) }}
                    </div>
                </div>

                <div class="form-group {{ $errors->has('employee_rate') ? 'has-error' : ''}}">
                    {{ Form::label('employee_rate', 'Sub-Contractor Rate') }}
                    <div class="input-group">
                        <span class="input-group-addon">$</span>
                        {{ Form::text('employee_rate',  NULL, ['class' => 'form-control']) }}
                    </div>
                </div>

                <div class="form-group {{ $errors->has('invoice_type') ? 'has-error' : ''}}">
                    {{ Form::label('invoice_type', 'Invoice Type') }}
                    {{ Form::select('invoice_type', $service->invoiceOptions,  NULL, ['class' => 'form-control']) }}
                    <p class="help-block">Note: When selecting <em>Automatic Invoice</em> make sure the customer entry has a billing address and email set!</p>
                </div>

                <div class="form-group">
                    {{ Form::checkbox('workorder_required') }}
                    {{ Form::label('workorder_required', ' Signed work order required') }}
                </div>
                <div class="form-group">
                    {{ Form::checkbox('signature_required') }}
                    {{ Form::label('signature_required', ' Signature on receipt required') }}
                </div>

                <h3>Commissions</h3>
                <div class="form-group {{ $errors->has('commission_employee_id') ? 'has-error' : ''}}">
                    {{ Form::label('commission_employee_id', 'Sub-Contractor') }}
                    {{ Form::select('commission_employee_id', select_lists('employees'),  NULL, ['class' => 'form-control']) }}
                </div>

                <div class="form-group {{ $errors->has('commission_percentage') ? 'has-error' : ''}}">
                    {{ Form::label('commission_percentage', 'Commission Percentage') }}
                    <div class="input-group">
                        <span class="input-group-addon">%</span>
                        {{ Form::text('commission_percentage',  NULL, ['class' => 'form-control']) }}
                    </div>
                </div>

            </div>
        </div>


    </div>

    <div class="col-sm-7">
        <div class="form-group">
            {{ Form::label('address', 'Address') }}
            {{ Form::textarea('address',  NULL, ['class' => 'form-control', 'rows' => 3]) }}
            <button class="btn btn-xs copy-address">Use Customer Address</button>
        </div>
        <div class="form-group">
            {{ Form::label('description', 'Description') }}
            {{ Form::textarea('description',  NULL, ['class' => 'form-control']) }}
        </div>
    </div>

    <div class="form-group col-sm-5">
        <div class="form-group">
            {{ Form::label('images', 'Images') }}
            @if(!$new && $service->locationImages->count())
                <div class="well well-sm">
                    @foreach ($service->locationImages as $img)
                        <div class="media">
                            <a class="pull-left lb"
                               href="{{ asset($img->photo->url()) }}">
                                <img class="media-object"
                                     src="{{ asset($img->photo->url('preview')) }}">
                            </a>

                            <div class="media-body">
                                <h4 class="media-heading">{{ $img->photo_file_name }}</h4>
                                <label>{{ Form::checkbox('delete_image[]', $img->id, NULL,  ['id' => 'delete_image']) }}
                                    Remove Image</label>
                            </div>
                        </div>
                    @endforeach
                </div>
            @endif

            {{ Form::file('photos[]', ['class' => 'form-control']) }}<br/>
            {{ Form::file('photos[]', ['class' => 'form-control']) }}<br/>
            {{ Form::file('photos[]', ['class' => 'form-control']) }}<br/>
            {{ Form::file('photos[]', ['class' => 'form-control']) }}<br/>

        </div>
    </div>


    @if($new)
        <div class="col-sm-12">
            {{ Form::submit('Add Service', ['class' => 'btn btn-primary']) }}
        </div>
    @else
        <div class="col-sm-12">
            {{ Form::submit('Save Changes', ['class' => 'btn btn-primary']) }}
            <a class="btn btn-small btn-danger pull-right" href="#"
               data-toggle="modal" data-target="#delete-user-modal">Delete
                Service</a>
        </div>
    @endif

    {{ Form::close() }}

    @if(!$new)
        <div id="delete-user-modal" class="modal fade" tabindex="-1"
             role="dialog" aria-labelledby="mySmallModalLabel"
             aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"
                                aria-hidden="true">×
                        </button>
                        <h4 class="modal-title">Delete Service</h4>
                    </div>
                    <div class="modal-body">
                        <p>Are you sure you want to delete this service?<br/>All
                            associated jobs will be deleted as well.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default"
                                data-dismiss="modal">No
                        </button>
                        &nbsp;
                        {{ Form::open(array('route' => ['admin.services.destroy', $service->id], 'class' => 'pull-right', 'method' => 'delete'))  }}
                        {{ Form::hidden('_destination', URL::previous()) }}
                        {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    @endif

@stop
