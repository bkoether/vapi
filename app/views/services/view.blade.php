@extends('layouts.admin')


@section('title')
<h1>@if(!$service->status) <i class="fa fa-lock text-danger"></i> @endif {{ $service->site_name }} <small>{{ $service->getTypeName() }}</small></h1>
@stop

@section('bc')
<li class="active">{{ link_to_route('admin.services.index', 'Services') }}</li>
@stop


@section('content')
<div class="col-sm-8">
  <div class="box box-solid">
    <div class="box-header">
      <h3 class="box-title">Details</h3>
      <div class="box-tools pull-right">
        <button class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body">
      <div class="list-group">

        @if ($service->store_number)
        <div class="list-group-item">
          <h4 class="list-group-item-heading">Store Number</h4>
          <p class="list-group-item-text">{{ $service->store_number }}</p>
        </div>
        @endif

        <div class="list-group-item">
          <h4 class="list-group-item-heading">Customer</h4>
          @if (isset($service->customer))
          <p class="list-group-item-text">{{ link_to_route('admin.customers.show', $service->customer->name, [$service->customer->id]) }}
            <span class="pull-right">Rate: ${{ $service->customer_rate }}</span>
          </p>
          @else
            <p class="list-group-item-text">NO CUSTOMER SET</p>
          @endif
        </div>

        <div class="list-group-item">
          <h4 class="list-group-item-heading">Sub-Contractor</h4>
          <p class="list-group-item-text">
            @if(empty($service->employee))
            <span class="label label-warning">No employee assigned.</span>
          @else
          {{ link_to_route('admin.employees.show', $service->employee->company, [$service->employee->id]) }}
            <span class="pull-right">Rate: ${{ $service->employee_rate }}</span>
          @endif
          </p>
        </div>

        <div class="list-group-item clearfix">
          <h4 class="list-group-item-heading">Frequency</h4>
          <p class="list-group-item-text">
            {{ $service->getFrequencyString() }}<br/>
            Next schedule: {{ $service->next_schedule->format('F Y') }}
            <span class="pull-right">Schedule ahead: {{ $service->schedule_ahead }} {{Str::plural('month', $service->schedule_ahead)}}</span><br/>
            Time required: {{ $service->duration }} {{ str_plural('day', $service->duration) }}
            <span class="pull-right">
              <a href="{{ action('ServicesController@generateJobs', ['id' => $service->id]) }}">Generate Jobs (starting {{ Carbon::now()->addMonth()->format('Y-m')}})</a><br/>
            </span>

          </p>
        </div>

        <div class="list-group-item">
          <h4 class="list-group-item-heading">Invoice</h4>
          <p class="list-group-item-text">{{ $service->getInvoiceOption() }}
            @if($service->workorder_required) &mdash; Signed work order required @endif
            @if($service->signature_required) &mdash; Signature on receipt required @endif
          </p>
        </div>


        <div class="list-group-item">
          <h4 class="list-group-item-heading">Description</h4>
          <div class="list-group-item-text">{{ combine_description($service, 'service') }}</div>
        </div>

        @if ($service->locationImages->count())
        <div class="list-group-item">
          <div class="list-group-item-text">
            <div class="row">
              @foreach ($service->locationImages as $img)
              <div class="col-xs-6 col-sm-4"><a href="{{ asset($img->photo->url()) }}" class="thumbnail lb"><img src="{{ asset($img->photo->url('thumbnail')) }}" /></a></div>
              @endforeach
            </div>

          </div>
        </div>
        @endif
      </div>



    </div>
    <div class="box-footer clearfix">
      <a class="pull-right" href="{{ route('admin.services.edit', [$service->id]) }}"><i class="fa fa-edit"></i> Edit Service</a>
      <a class="pull-left" href="{{ url('admin/services/create?clone=' . $service->id) }}"><i class="fa fa-copy"></i> Clone Service</a>
    </div>
  </div>

  <div class="box box-solid">
    <div class="box-header">
      <h3 class="box-title"><i class="fa fa-map-marker"></i> Address</h3>
      <div class="box-tools pull-right">
        <button class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body clearfix">
      <div id="gmap" class="col-sm-6" data-address="{{ $service->address }}"></div>
    </div>
    <div class="box-footer">{{ nl2br($service->address) }}</div>
  </div>
</div>

<div class="col-sm-4">
  <div class="box box-solid">
    <div class="box-header">
      <h3 class="box-title">Upcoming Jobs</h3>
      <div class="box-tools pull-right">
        <button class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body no-padding">
      <table class="table table-condensed">
        <thead><tr><th>On</th><th>Due latest</th><th></th></tr></thead>
        @if(!$service->jobs->count())
        <tr>
          <td colspan="3">No upcoming jobs for this service.</td>
        </tr>
        @endif
        @foreach($service->jobs as $job)
        <tr>
          <td>
            @if($job->scheduled)
            {{ $job->scheduled_at->toDateString() }}
            @else
            <span class="label label-warning">Not scheduled</span>
            @endif
          </td>
          <td>{{ $job->due_end->toDateString() }}</td>
          <td><a class="btn btn-circle btn-default pop-up-link" data-id="{{ $job->id }}" data-popup-type="job" href="#"><i class="fa fa-info"></i></a></td>
          {{--<td><a href="{{ route('admin.jobs.show', [$job->id]) }}"><i class="fa fa-file-text-o"></i> Details</a></td>--}}
        </tr>
        @endforeach
      </table>

    </div>
    <div class="box-footer clearfix">
      <a class="pull-right" href="{{ route('admin.jobs.create', ['serviceId' => $service->id])}}"><i class="fa fa-plus"></i> Add Custom Job</a>
      <a href="{{ action('ReportsController@index', ['customer' => (isset($service->customer) ? $service->customer->id : 0)]) }}"><i class="fa fa-file-text-o"></i> Past Jobs</a>
    </div>
  </div>
</div>
@include('partials.pop-up')
@stop