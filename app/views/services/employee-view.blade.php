@extends('layouts.employee')

@section('title')
<h1>{{ $service->site_name }} <small>{{ $service->getTypeName() }}</small></h1>
@stop

@section('bc')
<li><a href="{{ action('UserEmployeeController@jobs') }}">Services</a></li>
@stop


@section('content')

<div class="col-sm-6">
  <div class="list-group">

    <div class="list-group-item">
      <h4 class="list-group-item-heading">Payment/Invoice</h4>
      <div class="list-group-item-text">
        <div class="pull-right">
          My rate: ${{ $service->employee_rate }}<br/>
          @if ($service->invoice_type != 'O')
          Client rate: ${{ $service->customer_rate }}
          @endif
        </div>
        <span class="label label-default">{{ $service->getInvoiceOption() }}</span><br/>

        @if($service->workorder_required)
        <strong>Signed work order required</strong><br/>
        @endif
        @if($service->signature_required)
        <strong>Signature on receipt required</strong>
        @endif
      </div>
    </div>
    <div class="list-group-item">
      <h4 class="list-group-item-heading">Frequency</h4>
      <p class="list-group-item-text">{{ $service->getFrequencyString() }}<br/>
        Time required: {{ $service->duration }} {{ str_plural('day', $service->duration) }}
      </p>
    </div>


    <div class="list-group-item">
      <h4 class="list-group-item-heading">Description</h4>
      <div class="list-group-item-text">
        {{ combine_description($service, 'service') }}
      </div>
    </div>

  </div>
  <div class="row">
    @foreach ($service->locationImages as $img)
      <div class="col-xs-6 col-sm-4"><a href="{{ asset($img->photo->url()) }}" class="thumbnail lb"><img src="{{ asset($img->photo->url('thumbnail')) }}" /></a></div>
    @endforeach
  </div>
</div>

<div class="col-sm-6">
  <div class="box box-solid">
    <div class="box-header">
      <h3 class="box-title">Contact</h3>
      <div class="box-tools pull-right">
        <button class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body">
      <p class="lead">&nbsp;<i class="fa fa-user"></i> {{ $service->customer->contact_name }}</p>
      <p class="lead">&nbsp;<i class="fa fa-phone"></i> <a href="tel:+1{{ $service->customer->phone1 }}">{{ $service->customer->phone1 }}</a></p>
      @if(!empty($service->customer->phone2))
      <p class="lead">&nbsp;<i class="fa fa-phone"></i> <a href="tel:+1{{ $service->customer->phone2 }}">{{ $service->customer->phone2 }}</a></p>
      @endif
      <p class="lead">&nbsp;<i class="fa fa-envelope"></i> <a href="mailto:{{ $service->customer->email }}">{{ $service->customer->email }}</a></p>
    </div>
  </div>

  <div class="box box-solid">
    <div class="box-header">
      <h3 class="box-title"><i class="fa fa-map-marker"></i> Address</h3>
      <div class="box-tools pull-right">
        <button class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body clearfix">
      <div id="gmap" class="col-sm-6" data-address="{{ $service->address }}"></div>
    </div>
    <div class="box-footer">{{ nl2br($service->address) }}</div>
  </div>
</div>

@stop


