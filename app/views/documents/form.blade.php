@extends('layouts.employee')


@section('title')
  <h1>Upload Documents</h1>
@stop



@section('bc')
@stop


@section('content')


{{ Form::open(['action' => 'UserEmployeeController@documentUpload', 'method' => 'post', 'files' => true]) }}

  <div class="col-sm-12">
    <div class="callout callout-warning">
      <h4>Note</h4>
      <p>Uploading multiple large files at once can take up to 5 minutes. Do not refresh the page until the upload process is completed.</p>
    </div>

    @foreach ($jobs as $job)
        <div class="form-group col-sm-6">
          {{ Form::label('documents['.$job->id.']', $job->service->site_name . ' [' . $job->service->type . '] (Completed: '. $job->completed_at->toDateString() .')') }}
          {{ Form::file('documents['.$job->id.']', ['class' => 'form-control']) }}
        </div>



    @endforeach
    <div class="form-group col-sm-12">
      {{ Form::submit('Upload Files', ['class' => 'btn btn-primary']) }}
    </div>
  </div>
{{ Form::close() }}


@stop
