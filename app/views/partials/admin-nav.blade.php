<!-- Left side column. contains the logo and sidebar -->
<aside class="left-side sidebar-offcanvas">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <ul class="sidebar-menu">
      <li class="{{ set_active('admin') }}">
        <a href="/admin">
          <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a>
      </li>

      <li class="{{ set_active('admin/employees*') }} {{ set_active('admin/schedule*') }}">
        <a href="/admin/employees"><i class="fa fa-group"></i> <span>Sub-Contractors</span></a>
      </li>

      <li class="{{ set_active('admin/customers*') }}">
        <a href="/admin/customers">
          <i class="fa fa-briefcase"></i> <span>Customers</span>
        </a>
      </li>

      <li class="treeview {{ set_active('admin/services*') }} {{ set_active('admin/jobs*') }} {{ set_active('admin/estimates*') }}">
        <a href="#">
          <i class="fa fa-wrench"></i>
          <span>Work</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li class="{{ set_active('admin/services*') }}"><a href="/admin/services"><i class="fa fa-angle-double-right"></i> Services</a></li>
          <li class="{{ set_active('admin/jobs*') }}"><a href="/admin/jobs"><i class="fa fa-angle-double-right"></i> Jobs</a></li>
          <li class="{{ set_active('admin/estimates*') }}"><a href="/admin/estimates"><i class="fa fa-angle-double-right"></i> Estimates</a></li>
        </ul>
      </li>

      <li class="treeview {{ set_active('admin/reports*') }} {{ set_active('admin/starbucks*') }} {{ set_active('admin/commissions*') }}">
        <a href="#">
          <i class="fa fa-bar-chart-o"></i> <span>Reports</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li class="{{ set_active('admin/reports*') }}"><a href="/admin/reports"><i class="fa fa-angle-double-right"></i> <span>General</span></a></li>
          <li class="{{ set_active('admin/starbucks*') }}"><a href="/admin/starbucks"><i class="fa fa-angle-double-right"></i> Starbucks</a></li>
          <li class="{{ set_active('admin/commissions*') }}"><a href="/admin/commissions"><i class="fa fa-angle-double-right"></i> Commissions</a></li>
        </ul>
      </li>

      <li class="{{ set_active('admin/config*') }}">
        <a href="/admin/config">
          <i class="fa fa-pencil-square-o"></i> <span>Settings</span>
        </a>
      </li>

      <li class="{{ set_active('admin/users*') }}">
        <a href="/admin/users">
          <i class="fa fa-gears"></i> <span>Admin Accounts</span>
        </a>
      </li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
