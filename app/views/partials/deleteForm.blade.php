<div id="delete-user-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">{{ $title }}</h4>
      </div>
      <div class="modal-body">
        <p>{{ $text }}</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        &nbsp;
        {{ Form::open(array('route' => [$route, $id], 'class' => 'pull-right', 'method' => 'delete'))  }}
        {{ Form::hidden('_destination', URL::previous()) }}
        {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
        {{ Form::close() }}
      </div>
    </div>
  </div>
</div>