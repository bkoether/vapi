<!-- Left side column. contains the logo and sidebar -->
<aside class="left-side sidebar-offcanvas">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <ul class="sidebar-menu">
      <li class="{{ set_active('/') }}">
        <a href="/">
          <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a>
      </li>

      <li class="treeview {{ set_active('schedule*') }} {{ set_active('time-off*') }}">
        <a href="#">
          <i class="fa fa-calendar"></i> <span>Schedule</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li class="{{ set_active('schedule*') }}"><a href="/schedule"><i class="fa fa-angle-double-right"></i> Work Schedule</a></li>
          <li class="{{ set_active('time-off*') }}"><a href="/time-off"><i class="fa fa-angle-double-right"></i> Time Off</a></li>
        </ul>
      </li>

      <li class="{{ set_active('jobs*') }}">
        <a href="/jobs">
          <i class="fa fa-briefcase"></i> <span>Jobs & Services</span>
        </a>
      </li>

      {{--<li class="{{ set_active('service-routes*') }}">--}}
        {{--<a href="/service-routes">--}}
          {{--<i class="fa fa-road"></i> <span>Routes</span>--}}
        {{--</a>--}}
      {{--</li>--}}

      <li class="treeview {{ set_active('reports*') }} {{ set_active('commissions*') }}">
        <a href="#">
          <i class="fa fa-bar-chart-o"></i> <span>Reports</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>

        <ul class="treeview-menu">
          <li class="{{ set_active('reports*') }}">
            <a href="/reports"><i class="fa fa-angle-double-right"></i> <span>Invoices</span></a>
          </li>

          <li class="{{ set_active('commissions*') }}">
          <a href="/commissions">
            <i class="fa fa-angle-double-right"></i> <span>Commissions</span>
          </a>
          </li>
        </ul>
      </li>

      {{--<li class="{{ set_active('directory') }}">--}}
        {{--<a href="/directory">--}}
          {{--<i class="fa fa-users"></i> <span>Directory</span>--}}
        {{--</a>--}}
      {{--</li>--}}

      <li class="{{ set_active('account*') }}">
        <a href="/account">
          <i class="fa fa-cogs"></i> <span>My Account</span>
        </a>
      </li>


    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
