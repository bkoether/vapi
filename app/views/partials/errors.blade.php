@if(Session::has('msg'))
  <div class="alert alert-success alert-dismissable">
    <i class="fa fa-check"></i>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    {{ Session::get('msg') }}
  </div>
@endif

@if($errors->all())
  <div class="alert alert-danger alert-dismissable">
    <i class="fa fa-warning"></i>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    @foreach($errors->all() as $err)
      <p>{{ $err }}</p>
    @endforeach
  </div>
@endif