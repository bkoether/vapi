<div id="info-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" >
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    </div>
  </div>
</div>

<script id="info-modal-template" type="text/x-handlebars-template">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">
      <small>{{client}}</small><br/>
      {{site}} {{#if priority}}<span class="label label-danger">HIGH PRIORITY</span>{{/if}}</h4>
  </div>
  <div class="modal-body">
    <div class="row">
      <div class="col-sm-6">
        <p>
          {{#if phone}}<a href="tel:{{phone}}">{{phone}}</a><br/>{{/if}}
          {{#if email}}<a href="mailto:{{email}}">{{email}}</a><br/>{{/if}}
        </p>
        <p>
          {{{address}}}
        </p>
        <hr/>
        <div class="description-container">
          {{{description}}}
        </div>
        {{#if descForm}}
        <div class="desc-update-wrapper" data-request-id="{{requestId}}">
          <label for="desc-update">Add Note:</label><br/>
          <textarea class="form-control" name="desc-update" id="desc-update" rows="2"></textarea>
          <p class="btn-group-sm">
            <br/>
            <button data-id="{{clientId}}" class="add-client desc-up-btn btn btn-sm btn-default">Add to client</button>
            {{#if serviceId}}
              <button data-id="{{serviceId}}" class="add-service desc-up-btn btn btn-sm btn-default">Add to service</button>
            {{/if}}
          </p>
        </div>
        {{/if}}
      </div>
      <div class="col-sm-6">
        {{#if showRates}}
          <p>Rate: ${{e_rate}}{{#if c_rate }} | Customer: ${{c_rate}}{{/if}}</p>
        {{/if}}
        {{#if completed}}
        <h5>Last Completed</h5>
        <ul>
          {{#each completed}}
          <li>{{{this}}}</li>
          {{/each}}
        </ul>
        {{/if}}

        {{#if upcoming}}
        <h5>Upcoming Schedule</h5>
        <ul>
          {{#each upcoming}}
          <li>{{{this}}}</li>
          {{/each}}
        </ul>
        {{/if}}
      </div>
    </div>

  </div>
  <div class="modal-footer">
    {{#if signatureLink}}
    <a class="btn btn-default" href="{{signatureLink}}"><i class="fa fa-pencil"></i> Collect Signature</a>
    {{/if}}

    {{#if showPrint}}
      <a target="_blank" class="btn btn-default" href="/print/{{jobId}}"><i class="fa fa-print"></i> Print Cash Receipt</a>
    {{/if}}
    {{#if calRemove}}
      <a href="#" class="btn btn-warning cal-remove" data-event-id="{{calRemove}}">Remove From Calendar</a>
      &nbsp;
    {{/if}}

    {{#if viewLink}}
      <a href="{{viewLink}}" class="btn btn-default">{{viewText}}</a>
      &nbsp;
    {{/if}}
    {{#if editLink}}
      <a href="{{editLink}}" type="button" class="btn btn-default">{{editText}}</a>
      &nbsp;
      {{#if serviceId}}
        <a class="btn btn-default" href="/admin/services/create?clone={{serviceId}}"><i class="fa fa-copy"></i> Clone Service</a>
        &nbsp;
      {{/if}}
    {{/if}}
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  </div>

</script>