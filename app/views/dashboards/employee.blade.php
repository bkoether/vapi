@extends('layouts.employee')

@section('title')
  <h1>Vitrex Web App <small>Sub-Contractor Dashboard</small></h1>
@stop

@section('content')
<div class="col-sm-8">
  <div class="box">
    <div class="box-header">
      <h3 class="box-title">Today's Jobs</h3>
    </div>
    <div class="box-body">
      {{ Form::open(['action' => 'UserEmployeeController@submitReport', 'method' => 'post', 'class' => 'job-dashboard']) }}
      <div class="list-group jobs">
        @if (!$today->count())
          <div class="list-group-item">No jobs today.</div>
        @endif
        @foreach ($today as $tJob)


          <div class="list-group-item">

            {{ Form::hidden('job[' . $tJob->id . '][status]', 0, ['class' => 'status-field']) }}
            <div class="row">
              @if ($tJob->scheduled_at->isPast() && !$tJob->scheduled_at->isToday())
                <div class="col-xs-12 job-indicator">
                  <span class="label label-danger"><i class="fa fa-clock-o"></i> OVERDUE</span>
                </div>
              @endif

              <div class="col-xs-12">
                <h4>
                    <a class="btn btn-circle btn-default pop-up-link pull-right" data-id="{{ $tJob->id }}" data-popup-type="job" href="#"><i class="fa fa-info"></i></a>
                    @if($tJob->service && $tJob->service->invoice_type == 'C')
                        <a class="btn btn-sm btn-default pull-right" target="_blank" href="/print/{{ $tJob->id }}">Print Cash Invoice <i class="fa fa-print"></i></a>
                        <a class="btn btn-sm btn-default pull-right cash-invoice {{ $tJob->cash_invoice_sent ? 'sent' : '' }}" href="#">
                            <span class="on text-success">Cash Invoice Sent <i class="fa fa-check"></i></span>
                            <span class="off">Send Cash Invoice <i class="fa fa-envelope-o"></i></span>
                            </a>
                        <div class="cash-invoice-form well-sm well hidden">
                            Email to: <input type="text" class="input-sm" value="{{ $tJob->customer->email }}"/>
                            <a class="btn btn-primary btn-sm" href="#" data-job-id="{{ $tJob->id }}">Send</a><p class="text-danger"></p>
                        </div>

                    @endif

                  <a href="{{ action('UserEmployeeController@jobView', $tJob->id) }}">
                    @if ($tJob->service)
                      <small>{{ Str::upper($tJob->service->getTypeName()) }}</small><br/>
                      {{ $tJob->service->site_name }}
                    @else
                      <small>Custom Job</small><br/>
                      {{ $tJob->customer->name }}
                    @endif
                  </a>
                    @if($tJob->priority_task)
                        <i class="fa fa-warning text-danger"></i>
                    @endif
                </h4>
              </div>

              <div class="col-xs-12">
                <div class="btn-group btn-block">
                  <a href="#" class="trigger btn btn-success complete"><i class="fa fa-square-o"></i> COMPLETED</a>
                  <a class="trigger postpone btn btn-warning" href="#"><i class="fa fa-square-o"></i> POSTPONED</a>
                  <a class="trigger cancel btn btn-danger" href="#"><i class="fa fa-square-o"></i> CANCELED</a>
                </div>
              </div>

              <div class="col-xs-12">

                <div class="form-group job-report-date">
                  <div class="new-date-field hidden">
                    {{ Form::label('date', 'Completed On') }}
                    <div class="input-group">
                      <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                      {{ Form::text('job[' . $tJob->id . '][date]',  null, ['class' => 'form-control dpick']) }}
                    </div>
                  </div>
                </div>
                <div class="form-group job-report-comment hidden">
                  {{ Form::label('comment', 'Comments:') }}
                  {{ Form::textarea('job[' . $tJob->id . '][comment]',  null, ['class' => 'form-control', 'rows' => 1]) }}
                </div>
                {{--<div class="form-group">--}}
                  {{--<label>--}}
                    {{--{{ Form::checkbox('job[' . $tJob->id . '][upload]', '1', null) }}--}}
                    {{--Upload file after submission.--}}
                  {{--</label>--}}
                {{--</div>--}}

              </div>
            </div>


          </div>
        @endforeach
      </div>
      @if ($today->count())
        {{ Form::submit('Submit Report', ['class' => 'form-control btn btn-primary hidden submit-button']) }}
      @endif
      {{ Form::close() }}

    </div>
  </div>
</div>

<div class="col-sm-4">
  <div class="box">
    <div class="box-header">
      <h3 class="box-title">Upcoming</h3>
    </div>
    <div class="box-body no-padding">
      <table class="table table-condensed">
        @if (!$upcoming->count())
        <tr>
          <td>&nbsp;No upcoming jobs.</td>
        </tr>
        @endif

        @foreach($upcoming as $job)
        <tr>
          <td>
              {{ $job->service->site_name or $job->customer->name }} @if($job->priority_task)<i class="fa fa-warning text-danger"></i>@endif
          </td>
          <td>{{ $job->service->type or 'Custom' }}</td>
          <td>
            @if($job->scheduled)
            {{ $job->scheduled_at->toDateString() }}
            @else
            <span class="label label-warning">Not scheduled</span>
            @endif
          </td>
          <td><a class="btn btn-circle btn-default pop-up-link" data-id="{{ $job->id }}" data-popup-type="job" href="#"><i class="fa fa-info"></i></a></td>
        </tr>
        @endforeach
      </table>

    </div>    <div class="box-footer">
      <a href="/jobs">All Jobs</a>
    </div>
  </div>
</div>

@include('partials.pop-up')
@stop