@extends('layouts.admin')

@section('title')
  <h1>Vitrex Web App <small>Admin Dashboard</small></h1>
@stop

@section('content')
<div class="col-xs-6 col-sm-3">
  <div class="small-box bg-light-blue">
    <div class="inner">
      <h3>{{ $jobs->count() }}</h3>
      <p>Total jobs this week</p>
    </div>
    <div class="icon"><i class="glyphicon glyphicon-calendar"></i>
    </div>
  </div>
</div>

<div class="col-xs-6 col-sm-3">
  <div class="small-box bg-green">

    <div class="inner">
      <a href="/admin/reports?date={{ Carbon::now()->startOfWeek()->toDateString() }}+-+{{ Carbon::now()->endOfWeek()->toDateString() }}" class="dash-callout">
        <h3>{{ $completed }}</h3>
        <p>Jobs completed this week</p>
      </a>
    </div>

    <div class="icon"><i class="glyphicon glyphicon-check"></i>
    </div>
  </div>
</div>

<div class="col-xs-6 col-sm-3">
  <div class="small-box bg-red">
    <div class="inner">
      <a href="#" class="dash-callout overdue-callout">
        <h3>{{ $overdue }}</h3>
        <p>Jobs overdue</p>
      </a>
    </div>
    <div class="icon"><i class="glyphicon glyphicon-time"></i>
    </div>
  </div>
</div>

<div class="col-xs-6 col-sm-3">
  <div class="small-box bg-yellow">
    <div class="inner">
      <a href="/admin/reports?date={{ Carbon::now()->startOfYear()->toDateString() }}+-+{{ Carbon::now()->endOfYear()->toDateString() }}&uninvoiced=1" class="dash-callout">
        <h3>{{ $not_invoiced }}</h3>
        <p>Jobs not invoiced</p>
      </a>
    </div>
    <div class="icon"><i class="glyphicon glyphicon-usd"></i>
    </div>
  </div>
</div>

<div class="col-sm-9">
  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">This Week</h3>
    </div>
    <div class="box-body">
      <table class="table table-hover data-tables dt-dash">
        <thead>
          <tr>
            <th>Site</th>
            <th>Type</th>
            <th>Sub-Contractor</th>
            <th>Scheduled</th>
            <th>Status</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          @foreach($jobs as $job)
          <tr>
            <td>
                <a href="{{ route('admin.jobs.show', $job->id) }}">{{ $job->service->site_name or $job->customer->name }}</a>
                @if($job->priority_task)
                    <i class="fa fa-warning text-danger"></i>
                @endif
            </td>
            <td>{{ $job->service->type or 'Custom' }}</td>
            <td>@if ($job->employee){{  $job->employee->fullName() }} @else {{ '<span class="label label-warning">Not assigned</span>' }}@endif</td>
            <td>{{ $job->status('scheduled') }}</td>
            <td>{{ $job->status() }}</td>
            <td><a class="btn btn-circle btn-default pop-up-link" data-id="{{ $job->id }}" data-popup-type="job" href="{{ route('admin.jobs.show', $job->id)}}"><i class="fa fa-info"></i></a></td>
          </tr>
          @endforeach
        </tbody>
      </table>

    </div>
  </div>
</div>

<div class="col-sm-3">
  <div class="box">
    <div class="box-header">
      <h3 class="box-title">Quick Links</h3>
    </div>
    <div class="box-body">

        <a href="{{ route('admin.employees.create') }}" class="btn btn-flat btn-block btn-default">Add Sub-Contractor</a><br/>
        <a href="{{ route('admin.customers.create') }}" class="btn btn-flat btn-block btn-default">Add Customer</a><br/>
        <a href="{{ route('admin.services.create') }}" class="btn btn-flat btn-block btn-default">Add Service</a><br/>
        <a href="{{ route('admin.estimates.create') }}" class="btn btn-flat btn-block btn-default">Add Estimate</a>
     </div>
  </div>
</div>

@include('partials.pop-up')
@stop