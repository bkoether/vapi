@extends('layouts.employee')


@section('title')
    <h1>Time Off</h1>
@stop

@section('bc')
@stop


@section('content')
    <div class="col-sm-12">
        <div class="box box-solid">
            <div class="box-header">
                <h3 class="box-title">Add new Time Off</h3>

                <div class="box-tools pull-right">
                    <button class="btn btn-default btn-sm"
                            data-widget="collapse"><i
                                class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body" id="vacation-box">
                {{ Form::open(['route' => 'time-off.store']) }}
                <div class="row">
                    <div class="form-group col-sm-5">
                        <div class="input-group">
                            <span class="input-group-addon"><i
                                        class="fa fa-calendar"></i></span>
                            {{ Form::text('date', '', ['class' => 'form-control vacation-range']) }}
                        </div>
                    </div>
                    <div class="form-group col-sm-1">
                        {{ Form::submit('Save', ['class' => 'btn btn-default']) }}
                    </div>

                </div>

                {{ Form::close() }}
            </div>
        </div>


        @if(count($vacations))
            <h4>Time Off Entered</h4>
            <div class="col-sm-12">
                    @foreach($vacations as $vac)
                        <div class="row">
                            <p></p>
                            {{ Form::open(['route' => ['time-off.update', $vac->id], 'method' => 'put']) }}
                            <div class="col-sm-5">
                                <div class="input-group">
                                    <span class="input-group-addon"><i
                                                class="fa fa-calendar"></i></span>
                                    {{ Form::text('date', $vac->start_date->toDateString() . ' - ' . $vac->end_date->toDateString(), ['class' => 'form-control vacation-range']) }}
                                </div>
                            </div>
                            <div class="col-sm-1">
                                {{ Form::submit('Update', ['class' => 'btn btn-default']) }}
                            </div>
                            {{ Form::close() }}
                            <div class="col-sm-1">
                                {{ Form::open(['route' => ['time-off.destroy', $vac->id], 'method' => 'delete']) }}

                                        {{ Form::submit('Remove', ['class' => 'btn btn-danger']) }}


                                {{ Form::close() }}
                            </div>
                        </div>

                    @endforeach
            </div>
        @endif


    </div>

@stop