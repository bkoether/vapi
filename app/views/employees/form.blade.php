@extends('layouts.admin')


@section('title')
@if($new)
  <h1>New Sub-Contractor</h1>
@else
  <h1>{{ $employee->user->fullName() }} <small>{{ $employee->company }}</small></h1>
@endif
@stop



@section('bc')
<li>{{ link_to_route('admin.employees.index', 'Sub-Contractors') }}</li>
@if ($new)
  <li class="active">New Sub-Contractor</li>
@else
  <li class="active">{{ link_to_route('admin.employees.show', $employee->company, $employee->id) }}</li>
@endif
@stop


@section('content')
@if($new)
  {{ Form::model($employee, ['route' => 'admin.employees.store']) }}
@else
  {{ Form::model($employee, ['route' => ['admin.employees.update', $employee->id], 'method' => 'put']) }}
@endif
{{ Form::hidden('_destination', URL::previous()) }}

<div class="col-sm-6">
  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">Personal Information</h3>
    </div>
    <div class="box-body">
      <div class="form-group {{ $errors->has('user.first') ? 'has-error' : ''}}">
        {{ Form::label('user[first]', 'First Name') }}
        {{ Form::text('user[first]',  null, ['class' => 'form-control']) }}
      </div>

      <div class="form-group {{ $errors->has('user.last') ? 'has-error' : ''}}">
        {{ Form::label('user[last]', 'Last Name') }}
        {{ Form::text('user[last]', null, ['class' => 'form-control']) }}
      </div>
      <div class="form-group {{ $errors->has('birthday') ? 'has-error' : ''}}">
        {{ Form::label('birthday', 'Date of Birth') }}
        <div class="input-group">
          <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
          {{ Form::text('birthday',  (($employee->birthday && $employee->birthday->timestamp > 0) ? $employee->birthday->toDateString() : ''), ['class' => 'form-control dpick']) }}
        </div>
      </div>
    </div>
  </div>

  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">Work Information</h3>
    </div>
    <div class="box-body">
      <div class="form-group {{ $errors->has('company') ? 'has-error' : ''}}">
        {{ Form::label('company', 'Company Name') }}
        {{ Form::text('company',  null, ['class' => 'form-control']) }}
      </div>
      <div class="form-group {{ $errors->has('default_rate') ? 'has-error' : ''}}">
        {{ Form::label('default_rate', 'Default Rate') }}
        <div class="input-group">
          <span class="input-group-addon">$</span>
          {{ Form::text('default_rate',  null, ['class' => 'form-control']) }}
        </div>
      </div>
        <div class="form-group {{ $errors->has('invoice_option') ? 'has-error' : ''}}">
          {{ Form::label('invoice_option', 'Invoice Option') }}
          {{ Form::select('invoice_option', select_lists('invoice_option') , null , ['class' => 'form-control']) }}
        </div>

      <div class="form-group {{ $errors->has('sin_number') ? 'has-error' : ''}}">
        {{ Form::label('sin_number', 'SIN Number') }}
        {{ Form::text('sin_number',  null, ['class' => 'form-control']) }}
      </div>

      <div class="form-group {{ $errors->has('tax_number') ? 'has-error' : ''}}">
        {{ Form::label('tax_number', 'GST Number') }}
        {{ Form::text('tax_number',  null, ['class' => 'form-control']) }}
      </div>

      <div class="form-group {{ $errors->has('wbc_number') ? 'has-error' : ''}}">
        {{ Form::label('wbc_number', 'WorkSafe BC Number') }}
        {{ Form::text('wbc_number',  null, ['class' => 'form-control']) }}
      </div>
      <div class="form-group {{ $errors->has('start_date') ? 'has-error' : ''}}">
        {{ Form::label('start_date', 'Start Date') }}
        <div class="input-group">
          <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
          {{ Form::text('start_date',  (($employee->start_date && $employee->start_date->timestamp > 0) ? $employee->start_date->toDateString(): ''), ['class' => 'form-control dpick']) }}
        </div>
      </div>

      <div class="form-group {{ $errors->has('end_date') ? 'has-error' : ''}}">
        {{ Form::label('end_date', 'End Date') }}
        <div class="input-group">
          <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
          {{ Form::text('end_date',  (($employee->end_date && $employee->end_date->timestamp > 0) ? $employee->end_date->toDateString(): ''), ['class' => 'form-control dpick']) }}
        </div>
      </div>

    </div>
  </div>

</div>

<div class="col-sm-6">
  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">Contact Information</h3>
    </div>
    <div class="box-body">
      <div class="form-group {{ $errors->has('user.email') ? 'has-error' : ''}}">
        {{ Form::label('user[email]', 'Email') }}
        {{ Form::email('user[email]', null, ['class' => 'form-control']) }}
      </div>

      <div class="form-group {{ $errors->has('phone1') ? 'has-error' : ''}}">
        {{ Form::label('phone1', 'Phone 1') }}
        <div class="input-group">
          <span class="input-group-addon">+1</span>
          {{ Form::text('phone1',  null, ['class' => 'form-control']) }}
        </div>
      </div>

      <div class="form-group {{ $errors->has('phone2') ? 'has-error' : ''}}">
        {{ Form::label('phone2', 'Phone 2') }}
        <div class="input-group">
          <span class="input-group-addon">+1</span>

          {{ Form::text('phone2',  null, ['class' => 'form-control']) }}
        </div>
      </div>
      <div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
        {{ Form::label('address', 'Address') }}
        {{ Form::textarea('address',  null, ['class' => 'form-control']) }}
      </div>
    </div>
  </div>

  <div class="box box-primary col-sm-6">
    <div class="box-header">
      <h3 class="box-title">Web App Account</h3>
    </div>
    <div class="box-body">
      <div class="form-group">
        {{ Form::checkbox('user[status]') }}
        {{ Form::label('user[status]', ' Account Active') }}
      </div>

      <div class="form-group {{ $errors->has('user.password') ? 'has-error' : ''}}">
        {{ Form::label('user[password]', 'Password') }}
        {{ Form::password('user[password]', ['class' => 'form-control']) }}
      </div>
      <div class="form-group">
        {{ Form::label('user[password_confirmation]', 'Repeat Password') }}
        {{ Form::password('user[password_confirmation]', ['class' => 'form-control']) }}
      </div>
    </div>
  </div>
</div>


@if($new)
<div class="col-sm-12">
  {{ Form::submit('Add Sub-Contractor', ['class' => 'btn btn-primary']) }}
</div>
@else
<div class="col-sm-12">
  {{ Form::submit('Save Changes', ['class' => 'btn btn-primary']) }}
  <a class="btn btn-small btn-danger pull-right" href="#" data-toggle="modal" data-target="#delete-user-modal">Delete Account</a>
</div>
@endif

{{ Form::hidden('user[role]', 'employee') }}
{{ Form::close() }}

@if(!$new)
<div id="delete-user-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Delete Sub-Contractor</h4>
      </div>
      <div class="modal-body">
        <p>Are you sure you want to delete this sub-contractor: <strong>{{ $employee->user->fullName() }}</strong>?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        &nbsp;
        {{ Form::open(array('route' => ['admin.employees.destroy', $employee->id], 'class' => 'pull-right'))  }}
        {{ Form::hidden('_method', 'DELETE') }}
        {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
        {{ Form::close() }}
      </div>
    </div>
  </div>
</div>
@endif

@stop
