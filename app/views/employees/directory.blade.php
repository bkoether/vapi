@extends('layouts.employee')


@section('title')
<h1>Directory</h1>
@stop

@section('bc')
@stop


@section('content')
  @foreach($employees as $employee)
    <div class="col-md-6">
      <div class="box box-solid">
        <div class="box-header">
          <h3 class="box-title">{{ $employee->first }} {{ $employee->last }}</h3>
        </div><!-- /.box-header -->

        <div class="box-body">
          <p><i class="fa fa-envelope"></i> <a href="mailto:{{ $employee->email }}">{{ $employee->email }}</a></p>
          <p><i class="fa fa-phone"></i> <a href="tel:{{ $employee->employee->phone1 }}">{{ $employee->employee->phone1 }}</a></p>

        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
  @endforeach

@stop