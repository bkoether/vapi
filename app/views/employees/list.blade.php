@extends('layouts.admin')


@section('title')
<h1>Sub-Contractors</h1>
@stop

@section('bc')
@stop


@section('content')

<div class="col-sm-12">
  <p><a class="btn-sm btn-primary" href="{{ route('admin.employees.create') }}"><i class="fa fa-plus"></i> Add Sub-Contractor</a></p>
  <table class="table table-hover data-tables">
    <thead>
      <tr>
        <th>Name</th>
        <th>Company</th>
        <th>Status</th>
        <th></th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      @foreach($employees as $employee)
      <tr>
        <td>{{ link_to_route('admin.employees.show', $employee->user->first . ' ' . $employee->user->last, [$employee->id]) }}</td>
        <td>{{ $employee->company }}</td>
        <td><span class="label label-{{ $employee->user->status ? 'success' : 'danger' }}">{{ $employee->user->status ? 'active' : 'blocked' }}</span></td>
        <td>
          <a href="/admin/schedule/{{ $employee->id }}"><i class="fa fa-calendar"></i>&nbsp;Schedule</a>
        </td>
        <td>
          <a href="{{ route('admin.employees.edit', [$employee->id]) }}"><i class="fa fa-edit"></i>&nbsp;Edit</a>
        </td>

      </tr>
      @endforeach
    </tbody>
  </table>

</div>

@stop