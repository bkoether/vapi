@extends('layouts.admin')

@section('title')
  <h1>
    @if($employee->user->status == 0)
      <i class="fa fa-lock text-danger"></i>
    @endif
    {{ $employee->company }}
  </h1>
@stop

@section('bc')
<li><a href="{{ route('admin.employees.index') }}">Sub-Contractors</a></li>
@stop


@section('content')
<div class="col-sm-6">

  <div class="box box-solid">
    <div class="box-header">
      <h3 class="box-title">Contact</h3>
      <div class="box-tools pull-right">
        <button class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body">
      <p class="lead">&nbsp;<i class="fa fa-user"></i> {{ $employee->user->fullName() }}</p>
      <p class="lead">&nbsp;<i class="fa fa-phone"></i> <a href="tel:+1{{ $employee->phone1 }}">{{ $employee->phone1 }}</a></p>
      @if(!empty($employee->phone2))
      <p class="lead">&nbsp;<i class="fa fa-phone"></i> <a href="tel:+1{{ $employee->phone2 }}">{{ $employee->phone2 }}</a></p>
      @endif
      <p class="lead">&nbsp;<i class="fa fa-envelope"></i> <a href="mailto:{{ $employee->user->email }}">{{ $employee->user->email }}</a></p>
    </div>
    <div class="box-footer clearfix">
      <a class="pull-right" href="{{ route('admin.employees.edit', [$employee->id]) }}"><i class="fa fa-edit"></i> Edit Sub-Contractor</a>
    </div>
  </div>


  <div class="box box-solid">
    <div class="box-header">
      <h3 class="box-title">Other Information</h3>
      <div class="box-tools pull-right">
        <button class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body">
      <ul class="list-unstyled">
        @if ($employee->birthday->timestamp > 0)
        <li><strong>Date of birth:</strong> {{ $employee->birthday->toFormattedDateString() }}</li>
        @endif

        @if ($employee->start_date->timestamp > 0)
        <li><strong>Start Date:</strong> {{ $employee->start_date->toFormattedDateString() }}</li>
        @endif

        @if ($employee->end_date->timestamp > 0)
        <li><strong>End Date:</strong> {{ $employee->end_date->toFormattedDateString() }}</li>
        @endif

        @if ($employee->tax_number)
        <li><strong>GST:</strong> {{ $employee->tax_number }}</li>
        @endif

        @if ($employee->sin_number)
        <li><strong>SIN:</strong> {{ $employee->wbc_number }}</li>
        @endif

        @if ($employee->wbc_number)
        <li><strong>WorkSafe BC:</strong> {{ $employee->wbc_number }}</li>
        @endif

        @if ($employee->invoice_option)
        <li><strong>Invoice Option:</strong> {{ $employee->invoice_option }}</li>
        @endif
      </ul>
    </div>
  </div>

  <div class="box box-solid">
    <div class="box-header">
      <h3 class="box-title">Address</h3>
      <div class="box-tools pull-right">
        <button class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body clearfix">
      <div id="gmap" class="col-sm-6" data-address="{{ $employee->address }}"></div>
    </div>
    <div class="box-footer">{{ nl2br($employee->address) }}</div>
  </div>


  <div class="box box-solid">
    <div class="box-header">
      <h3 class="box-title">Financial Projections</h3>
      <div class="box-tools pull-right">
        <button class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body no-padding">
      <table class="table table-condensed">
        <tr>
          <th></th>
          <th># of jobs</th>
          <th>Gross</th>
          <th>Net</th>
        </tr>
        <tr>
          <td>This Month</td>
          <td>{{ $projections['this']['count'] }}</td>
          <td>${{ number_format($projections['this']['gross'], 2) }}</td>
          <td>${{ number_format($projections['this']['net'], 2) }}</td>
        </tr>
        <tr>
          <td>{{ Carbon::now()->addMonth()->format('F Y')}}</td>
          <td>{{ $projections['next']['count'] }}</td>
          <td>${{ number_format($projections['next']['gross'], 2) }}</td>
          <td>${{ number_format($projections['next']['net'], 2) }}</td>
        </tr>
      </table>
    </div>

  </div>


</div>

<div class="col-sm-6">

  <div class="box box-solid">
    <div class="box-header">
      <h3 class="box-title">Services</h3>
      <div class="box-tools pull-right">
        <button class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body no-padding">
      <table class="table table-condensed">
        @if (!$employee->services->count())
        <tr>
          <td>No services assigned.</td>
        </tr>
        @endif

        @foreach($services as $service)
        <tr>
          <td><a href="{{ route('admin.services.show', $service->id) }}">&nbsp;{{ $service->site_name }}</a></td>
          <td>{{ $service->type }}</td>
          <td><span class="label label-{{ $service->status ? 'success' : 'danger' }}">{{ $service->status ? 'active' : 'blocked' }}</span></td>
          <td>{{ $service->getFrequencyString() }}</td>
          <td><a class="btn btn-circle btn-default pop-up-link" data-id="{{ $service->id }}" data-popup-type="service" href="#"><i class="fa fa-info"></i></a></td>
        </tr>
        @endforeach
      </table>
    </div>

    <div class="box-footer clearfix">
      Default Rate: <strong>${{ number_format($employee->default_rate, 2) }}</strong>
    </div>

  </div>

  <div class="box box-solid">
    <div class="box-header">
      <h3 class="box-title">Upcoming Jobs</h3>
      <div class="box-tools pull-right">
        <button class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body no-padding">
      <table class="table table-condensed">
        @if (!$employee->jobs->count())
        <tr>
          <td>&nbsp;No upcoming jobs.</td>
        </tr>
        @endif
        @foreach($employee->jobs as $job)
        <tr>
          <td><a href="{{ route('admin.jobs.show', $job->id) }}">&nbsp;{{ $job->service->site_name or $job->customer->name }}</a></td>
          <td>{{ $job->service->type or 'Custom' }}</td>
          <td>
            @if($job->scheduled)
            {{ $job->scheduled_at->toDateString() }}
            @else
            <span class="label label-warning">Not scheduled</span>
            @endif
          </td>
          <td><a class="btn btn-circle btn-default pop-up-link" data-id="{{ $job->id }}" data-popup-type="job" href="#"><i class="fa fa-info"></i></a></td>
        </tr>
        @endforeach
      </table>

    </div>

    <div class="box-footer">
      <a href="{{ action('ReportsController@index', ['employee' => $employee->id])}}"><i class="fa fa-file-text-o"></i> Past Jobs</a>
    </div>
  </div>


</div>



@include('partials.pop-up')
@stop


