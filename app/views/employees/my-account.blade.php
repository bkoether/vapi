@extends('layouts.employee')


@section('title')
<h1>My Account</h1>
@stop



@section('bc')
<li class="active">My Account</li>
@stop




@section('content')

{{ Form::model($employee, ['action' => ['UserEmployeeController@accountUpdate'], 'method' => 'POST']) }}

<div class="col-sm-6">
  <div class="form-group {{ $errors->has('user.first') ? 'has-error' : ''}}">
    {{ Form::label('user[first]', 'First Name') }}
    {{ Form::text('user[first]',  null, ['class' => 'form-control']) }}
  </div>

  <div class="form-group {{ $errors->has('user.last') ? 'has-error' : ''}}">
    {{ Form::label('user[last]', 'Last Name') }}
    {{ Form::text('user[last]', null, ['class' => 'form-control']) }}
  </div>

  <div class="form-group {{ $errors->has('company') ? 'has-error' : ''}}">
    {{ Form::label('company', 'Company') }}
    {{ Form::text('company',  null, ['class' => 'form-control']) }}
  </div>

  <div class="form-group {{ $errors->has('birthday') ? 'has-error' : ''}}">
    {{ Form::label('birthday', 'Date of Birth') }}
    <div class="input-group">
      <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
      {{ Form::text('birthday',  (($employee->birthday && $employee->birthday->timestamp > 0) ? $employee->birthday->toDateString() : ''), ['class' => 'form-control dpick']) }}
    </div>
  </div>

  <div class="form-group {{ $errors->has('sin_number') ? 'has-error' : ''}}">
    {{ Form::label('sin_number', 'SIN Number') }}
    {{ Form::text('sin_number',  null, ['class' => 'form-control']) }}
  </div>

  <div class="form-group {{ $errors->has('tax_number') ? 'has-error' : ''}}">
    {{ Form::label('tax_number', 'GST Number') }}
    {{ Form::text('tax_number',  null, ['class' => 'form-control']) }}
  </div>

  <div class="form-group {{ $errors->has('wbc_number') ? 'has-error' : ''}}">
    {{ Form::label('wbc_number', 'WorkSafe BC') }}
    {{ Form::text('wbc_number',  null, ['class' => 'form-control']) }}
  </div>


</div>

<div class="col-sm-6">
  <div class="form-group {{ $errors->has('user.email') ? 'has-error' : ''}}">
    {{ Form::label('user[email]', 'Email') }}
    {{ Form::email('user[email]', null, ['class' => 'form-control']) }}
  </div>

  <div class="form-group {{ $errors->has('phone1') ? 'has-error' : ''}}">
    {{ Form::label('phone1', 'Phone 1') }}
    <div class="input-group">
      <span class="input-group-addon">+1</span>
      {{ Form::text('phone1',  null, ['class' => 'form-control']) }}
    </div>
  </div>

  <div class="form-group {{ $errors->has('phone2') ? 'has-error' : ''}}">
    {{ Form::label('phone2', 'Phone 2') }}
    <div class="input-group">
      <span class="input-group-addon">+1</span>

      {{ Form::text('phone2',  null, ['class' => 'form-control']) }}
    </div>
  </div>

  <div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
    {{ Form::label('address', 'Address') }}
    {{ Form::textarea('address',  null, ['class' => 'form-control']) }}
  </div>

</div>


<div class="col-sm-12">&nbsp;</div>

<div class="form-group col-sm-6  {{ $errors->has('user.password') ? 'has-error' : ''}}">
  {{ Form::label('user[password]', 'Password') }}
  {{ Form::password('user[password]', ['class' => 'form-control']) }}
</div>
<div class="form-group col-sm-6">
  {{ Form::label('user[password_confirmation]', 'Repeat Password') }}
  {{ Form::password('user[password_confirmation]', ['class' => 'form-control']) }}
</div>


<div class="col-sm-12">
  {{ Form::submit('Save Changes', ['class' => 'btn btn-primary']) }}
</div>

{{ Form::close() }}

@stop
