@extends('layouts.employee')


@section('title')
<h1>New Route</h1>
@stop

@section('bc')
<li>{{ link_to_route('service-routes.index', 'Routes') }}</li>
@stop


@section('content')
<div class="col-sm-12">
  {{ Form::open(['route' => 'service-routes.store', 'method' => 'post']) }}
  	<div class="form-group col-sm-12 {{ $errors->has('name') ? 'has-error' : ''}}">
  	  {{ Form::label('name', 'Route Name') }}
  	  {{ Form::text('name',  null, ['class' => 'form-control']) }}
  	</div>
  	<div class="form-group col-sm-12">
      <table class="table table-striped">
        @foreach($services as $service)
        <tr>
          <td>{{ Form::checkbox('services[]', $service->id, NULL, ['id' => 'service-' . $service->id]) }}</td>
          <td>{{ Form::label('service-' . $service->id, $service->site_name ) }}</td>
          <td>{{ Form::label('service-' . $service->id, $service->type ) }}</td>
          <td>{{ Form::label('service-' . $service->id, $service->address ) }}</td>

        </tr>
        @endforeach
      </table>

      {{ Form::submit('Save', ['class' => 'btn btn-primary']) }}
    </div>



  {{ Form::close() }}
</div>

@stop