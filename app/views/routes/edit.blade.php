@extends('layouts.employee')


@section('title')
<h1>Edit Route</h1>
@stop

@section('bc')
<li>{{ link_to_route('service-routes.index', 'Routes') }}</li>
@stop


@section('content')
<div class="col-sm-12">
  {{ Form::open(['route' => ['service-routes.update', $route->id], 'method' => 'put']) }}
  	<div class="form-group col-sm-12 {{ $errors->has('name') ? 'has-error' : ''}}">
  	  {{ Form::label('name', 'Route Name') }}
  	  {{ Form::text('name',  $route->name, ['class' => 'form-control']) }}
  	</div>
  	<div class="form-group col-sm-12">
      <table class="table table-striped">
        <thead>
          <tr>
            <th colspan="3">Service</th>
            <th>Remove</th>
          </tr>
        </thead>
        @foreach($route->services as $service)
        <tr>
          <td>{{ Form::label('r-service-' . $service->id, $service->site_name ) }}</td>
          <td>{{ Form::label('r-service-' . $service->id, $service->type ) }}</td>
          <td>{{ Form::label('r-service-' . $service->id, $service->address ) }}</td>
          <td>{{ Form::checkbox('remove[]', $service->id, NULL, ['id' => 'r-service-' . $service->id]) }}</td>

        </tr>
        @endforeach
      </table>

    </div>

    <div class="form-group col-sm-12">
      <table class="table table-striped">
        <thead>
          <tr>
            <th>Add</th>
            <th colspan="3">Service</th>
          </tr>
        </thead>
        @foreach($services as $service)
        <tr>
          <td>{{ Form::checkbox('services[]', $service->id, NULL, ['id' => 'service-' . $service->id]) }}</td>
          <td>{{ Form::label('service-' . $service->id, $service->site_name ) }}</td>
          <td>{{ Form::label('service-' . $service->id, $service->type ) }}</td>
          <td>{{ Form::label('service-' . $service->id, $service->address ) }}</td>

        </tr>
        @endforeach
      </table>

    </div>
    {{ Form::submit('Save', ['class' => 'btn btn-primary']) }}
  {{ Form::close() }}

  {{ Form::open(['route' => ['service-routes.destroy', $route->id], 'method' => 'delete']) }}
  	{{ Form::submit('Delete', ['class' => 'btn btn-danger pull-right']) }}
  {{ Form::close() }}
</div>
@stop