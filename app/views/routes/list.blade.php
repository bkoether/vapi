@extends('layouts.employee')


@section('title')
<h1>Routes</h1>
@stop

@section('bc')
@stop


@section('content')
<div class="col-sm-12">
  <div class="pull-right"><a class="btn btn-primary" href="{{ route('service-routes.create') }}"><i class="fa fa-plus"></i> Add New Route</a></div>
</div>
<div class="col-sm-12 clearfix">&nbsp;</div>

<div class="col-sm-12">
  @if($routes->count())
    <div class="box-group" id="accordion">
      @foreach($routes as $index => $route)
        <div class="panel box box-primary">
          <div class="box-header">
            <h4 class="box-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $route->id }}" class="">{{ $route->name }}</a>
            </h4>
            <div class="box-tools pull-right">
              <a class="btn btn-default btn-sm" href="{{ route('service-routes.edit', $route->id) }}">Edit</a>
            </div>
          </div>

          <div id="collapse{{ $route->id }}" class="panel-collapse collapse {{ $index ? "" : "in" }}">
            <div class="box-body">
              <ul>
                @foreach($route->services as $service)
                  <li>{{ $service->site_name }} ({{ $service->type }}) - {{ $service->address }}</li>
                @endforeach
              </ul>
            </div>
          </div>
        </div>
      @endforeach
    </div>
  @else
    <blockquote>
      <h3>No routes created yet.</h3>
    </blockquote>
  @endif

</div>


@stop