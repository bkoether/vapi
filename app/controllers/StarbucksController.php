<?php

use Vitrix\Mailers\AdminMailer;

class StarbucksController extends \BaseController
{

    /**
     * @var Vitrix\Mailers\AdminMailer
     */

    protected $adminMailer;

    function __construct(AdminMailer $adminMailer)
    {
        $this->adminMailer = $adminMailer;
    }

    /**
     * Display a listing of the resource.
     * GET /starbucks
     *
     * @return Response
     */
    public function index()
    {
        $reports        = Report::all();
        $completed_jobs = Job::with('customer', 'service', 'employee')
                             ->whereHas('customer', function ($q) {
                                 $q->where('chain', 'Starbucks');
                             })
                             ->where('report_id', 0)
                             ->where('completed', true)
                             ->orderBy('completed_at')
                             ->get();
        return View::make('reports.starbucks',
          compact('reports', 'completed_jobs'));
    }

    /**
     * Store a newly created resource in storage.
     * POST /starbucks
     *
     * @return Response
     */
    public function store()
    {
        $report            = new Report();
        $report->call_date = Carbon::now();

        $jobs                = Input::get('jobs');
        $report->report_data = $jobs;
        $report->save();

        foreach ($jobs as $jobId) {
            $job = Job::find($jobId);
            $job->report()->associate($report);
            $job->save();
        }

        return Redirect::route('admin.starbucks.show', $report->id);
    }

    /**
     * Display the specified resource.
     * GET /starbucks/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $report = Report::find($id);

        // Check if this is a brand new report or one to edit
        if (isset($report->report_data[0])) {
            $newReportData = array();
            foreach ($report->report_data as $jid) {
                $job                 = Job::with('service', 'employee')
                                          ->find($jid);
                $newReportData[$jid] = [
                  'store_number'     => isset($job->service->store_number) ? $job->service->store_number : '',
                  'location_name'    => isset($job->service->site_name) ? $job->service->site_name : (isset($job->customer->name) ? $job->customer->name : 'No Customer Found'),
                  'service_type'     => isset($job->service->type) ? $job->service->type : 'Custom',
                  'signed_worksheet' => isset($job->service->workorder_required) ? $job->service->workorder_required : false,
                  'completed'        => $job->completed_at->toDateString(),
                  'contact_name'     => '',
                  'contact_position' => '',
                  'call_time'        => '',
                  'comment'          => '',
                ];
            }
            $report->report_data = $newReportData;
        }

        return View::make('reports.starbucks-view', compact('report'));

    }

    public function update($id)
    {
        $data = Input::get('report_data');

        $report              = Report::find($id);
        $report->report_data = $data;
        $report->save();

        return Redirect::back()->with(['msg' => 'Report updated.']);
    }

    public function submit($id)
    {
        $email             = Input::get('email');
        $report            = Report::find($id);
        $report->submitted = true;
        $report->save();
        $this->adminMailer->sendStarbucksReport($email, $report);

        return Redirect::back()
                       ->with(['msg' => 'Report sent to ' . $email . '.']);
    }


    public function export()
    {
        if (Input::has('date')) {
            $date    = Input::get('date');
            $from_to = explode(' - ', $date);
            $from    = Carbon::parse($from_to[0])->toDateString();
            $to      = Carbon::parse($from_to[1])->toDateString();

            $completed_jobs = Job::with('customer', 'service')
                                 ->whereHas('customer', function ($q) {
                                     $q->where('chain', 'Starbucks');
                                 })
                                 ->whereBetween('completed_at', [$from, $to])
                                 ->where('completed', true)
                                 ->where('cancelled', false)
                                 ->orderBy('completed_at')
                                 ->get();

            $output = "Store Number,Service,Date,Sub Total,Tax,Total\n";
            foreach ($completed_jobs as $job) {
                if ($job->service_id) {
                    $store_number = $job->service->store_number;
                } else {
                    $name         = explode(" ", $job->customer->name);
                    $store_number = trim($name[0], '-');
                }
                $row = array(
                  $store_number,
                  $job->service_id ? $job->service->getTypeName() : 'Custom',
                  $job->completed_at->toDateString(),
                  number_format($job->customer_rate, 2),
                  number_format(($job->customer_rate * .05), 2),
                  number_format(($job->customer_rate * 1.05), 2),
                );
                $output .= implode(",", $row) . "\n";
            }

            $headers = array(
              'Content-Type'        => 'text/csv',
              'Content-Disposition' => 'attachment; filename="SB_' . $from . '_' . $to . '.csv"',
            );

            return Response::make(rtrim($output, "\n"), 200, $headers);
        } else {
            return Redirect::action('StarbucksController@index');
        }
    }

}