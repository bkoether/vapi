<?php


class VacationsController extends \BaseController
{

    /**
     * Display a listing of the resource.
     * GET /vacation
     *
     * @return Response
     */
    public function index()
    {
        $vacations = Vacation::where('employee_id', Auth::user()->employee->id)
                             ->where('end_date', '>=',
                               Carbon::now()->toDateString())
                             ->orderBy('start_date')
                             ->get();
        Debugbar::info($vacations);
        return View::make('employees.vacation', compact('vacations'));
    }


    /**
     * Store a newly created resource in storage.
     * POST /vacation
     *
     * @return Response
     */
    public function store()
    {
        if (Input::has('date')) {
            $parts = explode(' - ', Input::get('date'));
            if (count($parts) == 2) {
                $start = Carbon::parse($parts[0]);
                $end   = Carbon::parse($parts[1]);
                if ($start->lte($end)) {
                    $vacation = new Vacation([
                      'employee_id' => Auth::user()->employee->id,
                      'start_date'  => $start->toDateString(),
                      'end_date'    => $end->toDateString()
                    ]);
                    $vacation->save();
                    return Redirect::back()->with(['msg' => 'Time Off added.']);
                }

            }
        }
        return Redirect::back()->withErrors(['Non valid dates supplied.']);
    }


    /**
     * Update the specified resource in storage.
     * PUT /vacation/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        if (Input::has('date')) {
            $parts = explode(' - ', Input::get('date'));
            if (count($parts) == 2) {
                $start = Carbon::parse($parts[0]);
                $end   = Carbon::parse($parts[1]);
                if ($start->lte($end)) {
                    $vacation = Vacation::find($id);

                    $vacation->start_date = $start->toDateString();
                    $vacation->end_date   = $end->toDateString();

                    $vacation->save();
                    return Redirect::back()->with(['msg' => 'Changes saved.']);
                }

            }
        }
        return Redirect::back()->withErrors(['Non valid dates supplied.']);
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /vacation/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $vacation = Vacation::find($id);
        $vacation->delete();

        return Redirect::back()->with(['msg' => 'Time Off removed.']);

    }

}