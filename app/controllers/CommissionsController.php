<?php

class CommissionsController extends \BaseController
{

    /**
     * Display a listing of the resource.
     * GET /commissions
     *
     * @return Response
     */
    public function adminList()
    {

        if (Input::has('date')) {
            $month = Input::get('date');
        } else {
            $month = Carbon::now()->format('Y-m');
        }
        $calc_month = Carbon::parse($month);


        $start = $calc_month->firstOfMonth()->toDateString();
        $end   = $calc_month->lastOfMonth()->toDateString();


        $monthCommissions = Commission::with('employee', 'job', 'job.service')
                                      ->whereBetween('created_at',
                                        [$start, $end])
                                      ->orderBy('created_at')
                                      ->get();

        $monthCommissionsTotal = $monthCommissions->sum(function ($row) {
            return $row->value;
        });


        $services = Service::whereNotNull('commission_employee_id')
                           ->join('customers', 'services.customer_id', '=',
                             'customers.id')
                           ->join('employees',
                             'services.commission_employee_id', '=',
                             'employees.id')
                           ->get([
                             'customers.name',
                             'customers.id AS cid',
                             'services.id AS sid',
                             'services.site_name',
                             'services.type',
                             'services.commission_percentage',
                             'employees.company',
                             'employees.id AS eid'
                           ]);

        $commissions = Commission::groupBy('employee_id')
                                 ->selectRaw('sum(commissions.value) as sum, employees.company, employees.id')
                                 ->join('employees', 'commissions.employee_id',
                                   '=', 'employees.id')
                                 ->get([
                                   'sum',
                                   'employees.company',
                                   'employees.id'
                                 ]);

        return View::make('commissions.adminList',
          compact('services', 'commissions', 'month', 'monthCommissions',
            'monthCommissionsTotal'));
    }


    public function employeeList()
    {
        $employeeId = Auth::user()->employee->id;
        if (Input::has('date')) {
            $month = Input::get('date');
        } else {
            $month = Carbon::now()->format('Y-m');
        }
        $calc_month = Carbon::parse($month);

        $start = $calc_month->firstOfMonth()->toDateString();
        $end   = $calc_month->lastOfMonth()->toDateString();

        $services = Service::with('customer')
                           ->where('commission_employee_id', $employeeId)
                           ->get();

        $monthCommissions = Commission::with('job', 'job.service')
                                      ->where('employee_id', $employeeId)
                                      ->whereBetween('created_at',
                                        [$start, $end])
                                      ->orderBy('created_at')
                                      ->get();

        $monthCommissionsTotal = $monthCommissions->sum(function ($row) {
            return $row->value;
        });

        return View::make('commissions.employeeList',
          compact('services', 'month', 'monthCommissions',
            'monthCommissionsTotal'));

    }


}