<?php

class PopupController extends \BaseController
{

    protected $response = [
      'requestId'     => '',
      'client'        => '',
      'clientId'      => '',
      'serviceId'     => '',
      'jobId'         => 0,
      'site'          => '',
      'address'       => '',
      'phone'         => '',
      'email'         => '',
      'description'   => '',
      'viewLink'      => '',
      'viewText'      => '',
      'editLink'      => '',
      'editText'      => '',
      'calRemove'     => '',
      'priority'      => '',
      'descForm'      => true,
      'completed'     => [],
      'upcoming'      => [],
      'c_rate'        => 0,
      'e_rate'        => 0,
      'showRates'     => false,
      'showPrint'     => false,
      'signatureLink' => false
    ];

    public function job($id, $delete = false)
    {
        $job = Job::with('service', 'customer')
                  ->find($id);
        $this->fillResponse($job->customer, $job->service);
        $this->response['description'] = combine_description($job, 'job');
        $this->response['requestId']   = 'job/' . $id;
        $this->response['jobId']       = $id;


        $this->response['viewText'] = 'Job Details';
        $this->response['viewLink'] = action('UserEmployeeController@jobView',
          $id);
        $this->response['priority'] = $job->priority_task ?: false;

        $this->response['e_rate'] = $job->employee_rate;
        if (Auth::user()->role == 'admin') {
            $this->response['c_rate']   = $job->customer_rate;
            $this->response['descForm'] = false;
            $this->response['viewLink'] = action('JobsController@show', $id);
            $this->response['editText'] = 'Edit Job';
            $this->response['editLink'] = action('JobsController@edit', $id);
        } else {
            if (!$job->document) {
                $this->response['signatureLink'] = action('SignatureController@create',
                  $id);
            }
        }

        if ($delete) {
            $this->response['calRemove'] = $id;
        }

        if (isset($job->service) && $job->service->invoice_type == 'C') {
            $this->response['showPrint'] = true;
        }

        return Response::json($this->response);


    }

    public function service($id)
    {
        $service = Service::with('customer')
                          ->find($id);
        $this->fillResponse($service->customer, $service);
        $this->response['description'] = combine_description($service,
          'service');
        $this->response['requestId']   = 'service/' . $id;

        $this->response['viewText'] = 'Service Details';
        $this->response['viewLink'] = action('UserEmployeeController@serviceView',
          $id);

        $this->response['e_rate'] = $service->employee_rate;
        if (Auth::user()->role == 'admin') {
            $this->response['c_rate']   = $service->customer_rate;
            $this->response['descForm'] = false;
            $this->response['viewLink'] = action('ServicesController@show',
              $id);
            $this->response['editText'] = 'Edit Service';
            $this->response['editLink'] = action('ServicesController@edit',
              $id);
        }

        return Response::json($this->response);
    }

    public function customer($id)
    {
        $customer                      = Customer::find($id);
        $this->response['requestId']   = 'customer/' . $id;
        $this->response['site']        = $customer->name;
        $this->response['client']      = $customer->chain;
        $this->response['address']     = $customer->billing_address;
        $this->response['phone']       = $customer->phone1;
        $this->response['email']       = $customer->email;
        $this->response['description'] = combine_description($customer,
          'customer');

        $completed = Job::with('service')
                        ->orderBy('completed_at', 'DESC')
                        ->where('completed', 1)
                        ->where('cancelled', 0)
                        ->where('customer_id', $id)
                        ->take(3)
                        ->get();
        foreach ($completed as $job) {
            $name                          = !empty($job->service) ? $job->service->type : 'Custom';
            $this->response['completed'][] = $job->completed_at->format('D, M j, Y') . "($name)";
        }

        $upcoming = Job::with('service')
                       ->orderBy('scheduled_at', 'ASC')
                       ->where('completed', 0)
                       ->where('scheduled', 1)
                       ->where('customer_id', $id)
                       ->get();
        foreach ($upcoming as $job) {
            $name                         = !empty($job->service) ? $job->service->type : 'Custom';
            $this->response['upcoming'][] = $job->scheduled_at->format('D, M j, Y') . " ($name)";
        }

        if (Auth::user()->role == 'admin') {
            $this->response['descForm'] = false;
            $this->response['viewText'] = 'Customer Details';
            $this->response['viewLink'] = action('CustomersController@show',
              $id);
            $this->response['editText'] = 'Edit Customer';
            $this->response['editLink'] = action('CustomersController@edit',
              $id);
        }

        return Response::json($this->response);
    }

    private function fillResponse($customer, $service)
    {
        $this->response['showRates'] = true;
        $this->response['client']    = $customer->name;
        $this->response['clientId']  = $customer->id;
        $this->response['address']   = !empty($service) ? $service->address : $customer->billing_address;
        $this->response['phone']     = $customer->phone1;
        $this->response['email']     = $customer->email;
        $this->response['site']      = 'Custom Job';

        if (!empty($service)) {
            $this->response['site']      = $service->site_name . " ($service->type)";
            $this->response['completed'] = [];
            $this->response['upcoming']  = [];
            $this->response['serviceId'] = $service->id;


            $completed = $service->lastCompleted(3);
            foreach ($completed as $job) {
                $this->response['completed'][] = $job->completed_at->format('D, M j, Y');
            }

            $upcoming = Job::orderBy('scheduled_at', 'ASC')
                           ->where('completed', 0)
                           ->where('scheduled', 1)
                           ->where('service_id', $service->id)
                           ->get(['scheduled_at']);
            foreach ($upcoming as $job) {
                $this->response['upcoming'][] = $job->scheduled_at->format('D, M j, Y');
            }
        }
    }

    public function updateDescription($type, $id, $requestType, $requestId)
    {
        $response = array(
          'newText' => ''
        );

        $text = Input::get('text', '');
        if (!empty($text)) {
            $update = '[sc][i]' . Auth::user()->first . ' ' . Auth::user()->last . ' - ' . date('m/d/Y') . '[/i] ' . $text . '[/sc]';

            if ($type == 'client') {
                $customer = Customer::find($id);
                $customer->description .= $update;
                $customer->save();
            } elseif ($type == 'service') {
                $service = Service::find($id);
                $service->description .= $update;
                $service->save();
            }

            switch ($requestType) {
                case 'job':
                    $model               = Job::with('customer', 'service')
                                              ->find($requestId);
                    $response['newText'] = combine_description($model, 'job');
                    break;
                case 'service':
                    $model               = Service::with('customer')
                                                  ->find($requestId);
                    $response['newText'] = combine_description($model,
                      'service');
                    break;
                case 'customer':
                    $model               = Customer::find($requestId);
                    $response['newText'] = combine_description($model,
                      'customer');
                    break;
            }

        }

        return Response::json($response);


    }

}