<?php

class SessionController extends \BaseController
{

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        if (Auth::guest()) {
            return View::make('login.login');
        } else {
            return Redirect::to('/');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input   = Input::all();
        $attempt = Auth::attempt([
          'email'    => $input['email'],
          'password' => $input['password'],
          'status'   => 1,
        ]);
        if ($attempt) {
            switch (Auth::user()->role) {
                case 'admin':
                    return Redirect::intended('admin');
                case 'employee':
                    return Redirect::intended('/');
            }
        } else {
            return Redirect::to('login')
                           ->withInput()
                           ->with(['error' => 'Wrong username/password.']);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy()
    {
        Auth::logout();
        return Redirect::intended('login');
    }

}