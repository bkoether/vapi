<?php

use Vitrix\Transformers\CustomerTransformer;

class CustomersController extends \BaseController
{

    protected $customerTransformer;

    public function __construct(CustomerTransformer $customerTransformer)
    {
        $this->customerTransformer = $customerTransformer;
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $customers = Customer::all();

        return View::make('customers.list', compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $customer         = new Customer();
        $customer->status = 1;
        return View::make('customers.form', [
          'new'      => true,
          'customer' => $customer
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input      = Input::all();
        $validation = Validator::make($input, $this->valRules());

        if ($validation->fails()) {
            return Redirect::route('admin.customers.create')
                           ->withInput()
                           ->withErrors($validation);
        } else {
            $customer = Customer::create($input);

            return form_redirect(Input::get('_destination', false),
              $customer->name . ' has been created.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $customer = Customer::with([
          'services',
          'jobs'      => function ($query) {
              $query->where('completed', '=', '0')->orderBy('due_start', 'ASC');
          },
          'estimates' => function ($query) {
              $query->where('accepted', '=', '0')
                    ->orderBy('expiration_date', 'ASC');
          }
        ])->find($id);


        $services = $customer->services->sortBy(function ($service) {
            return preg_replace('/\s+/', '',
              $service->site_name . $service->type);
        });

        return View::make('customers.view', compact('customer', 'services'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $customer = Customer::find($id);

        return View::make('customers.form', [
          'customer' => $customer,
          'new'      => false
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        $validation = Validator::make(Input::all(), $this->valRules($id));

        if ($validation->fails()) {
            return Redirect::route('admin.customers.edit', $id)
                           ->withInput()
                           ->withErrors($validation);
        } else {
            $input    = Input::all();
            $customer = Customer::find($id);
            $customer->fill($input);
            $customer->status = Input::get('status');
            $customer->save();

            return form_redirect(Input::get('_destination', false),
              $customer->name . ' has been updated.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $customer = Customer::find($id);
        $customer->delete();

        return Redirect::route('admin.customers.index')
                       ->with(['msg' => 'Customer deleted.']);
    }

    private function valRules($id = 0)
    {
        return array(
          'name'            => 'required',
          'email'           => 'email',
          'contact_2_email' => 'email',
          'default_rate'    => 'numeric',
        );
    }


    public function jobs($id)
    {
        return Customer::find($id)->jobs;
    }

    public function services($id)
    {
        return Customer::find($id)->services;
    }

}