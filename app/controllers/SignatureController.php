<?php

use Thujohn\Pdf;

class SignatureController extends \BaseController
{

    /**
     * Show the form for creating a new resource.
     * GET /signature/create
     *
     * @return Response
     */
    public function create($jobId)
    {
        $new = true;
        $job = Job::with('service', 'customer')->find($jobId);
        if (empty($job)) {
            return Redirect::to('/');
        }

        return View::make('signature.form', compact('job', 'new'));
    }

    /**
     * Store a newly created resource in storage.
     * POST /signature
     *
     * @return Response
     */
    public function store()
    {
        $input      = Input::all();
        $jobId      = $input['job_id'];
        $validation = Validator::make($input, $this->valRules());
        if ($validation->fails()) {

            return Redirect::back()
                           ->withErrors($validation)
                           ->withInput();
        } else {
            $document                 = new Document();
            $document->file           = $input['signature'];
            $document->job_id         = $input['job_id'];
            $document->signature_name = $input['signature_name'];
            $document->save();

            return form_redirect(Input::get('_destination', false),
              'Signature saved.');
        }
    }


    public function collect()
    {
        if (Input::has('jobs')) {
            $data = [];
            $jobs = Job::with('service', 'customer', 'document')
                       ->whereIn('jobs.id', Input::get('jobs'))
                       ->get();

            foreach ($jobs as $job) {
                if (!empty($job->document)) {
                    $data[] = [
                      'client'    => $job->customer->name,
                      'site'      => $job->service->site_name ?: 'Custom Job',
                      'service'   => $job->service->getTypeName(),
                      'signed_by' => $job->document->signature_name,
                      'signed_on' => $job->document->created_at->toDayDateTimeString(),
                      'file'      => substr($job->document->file->url(), 1)
                    ];
                }
            }

            $pdf = new Pdf\Pdf();

//            $headers = array('Content-Type' => 'application/pdf');
//            return Response::make($pdf->load(View::make('reports.signatures')->with(['data' => $data]), 'letter')->show(), 200, $headers);

            return View::make('reports.signatures')->with(['data' => $data]);
//            return $pdf->load(View::make('reports.signatures')->with(['data' => $data]), 'letter')->download('signatures_' . Carbon::now()->toDateTimeString());
        }

    }

    private function valRules()
    {
        return array(
          'signature_name' => 'required',
          'signature'      => 'required',
        );
    }

}