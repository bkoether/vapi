<?php
use Vitrix\Transformers\EmployeeTransformer;
use Vitrix\EmployeeFunctions;

class EmployeesController extends \BaseController
{

    protected $employeeTransformer;
    protected $employeeFunctions;

    public function __construct(
      EmployeeTransformer $employeeTransformer,
      EmployeeFunctions $employeeFunctions
    ) {
        $this->employeeTransformer = $employeeTransformer;
        $this->employeeFunctions   = $employeeFunctions;

    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $sortBy    = Request::get('sortBy', 'users.last');
        $sort      = Request::get('sort', 'asc');
        $employees = Employee::with('user')->get();

        return View::make('employees.list', compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     *
     */
    public function create()
    {
        $employee = new Employee();

        return View::make('employees.form', [
          'employee' => $employee,
          'new'      => true
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $validation = Validator::make(Input::all(), $this->valRules());

        if ($validation->fails()) {
            return Redirect::route('admin.employees.create')
                           ->withInput()
                           ->withErrors($validation);
        } else {
            $userInput     = Input::get('user');
            $employeeInput = Input::except('user');

            $user     = User::create($userInput);
            $employee = new Employee($employeeInput);
            $user->employee()->save($employee);

            return form_redirect(Input::get('_destination', false),
              $employee->company . ' has been created.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $employee = Employee::with([
          'user',
          'services',
          'services.customer',
          'jobs' => function ($query) {
              $query->where('completed', '=', '0');
              $query->orderBy('scheduled_at', 'ASC');
          },
          'jobs.customer'
        ])->find($id);

        $projections = [
          'this' => $this->employeeFunctions->getMonthlyProjection(Carbon::now(),
            $employee),
          'next' => $this->employeeFunctions->getMonthlyProjection(Carbon::now()
                                                                         ->addMonth(),
            $employee)
        ];

        $services = $employee->services->sortBy('site_name');

        return View::make('employees.view',
          compact('employee', 'projections', 'services'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $employee = Employee::with('user')->find($id);

        return View::make('employees.form', [
          'employee' => $employee,
          'new'      => false
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        $employee = Employee::find($id);


        $validation = Validator::make(Input::all(),
          $this->valRules($employee->user_id));

        if ($validation->fails()) {
            return Redirect::route('admin.employees.edit', $id)
                           ->withInput()
                           ->withErrors($validation);
        } else {
            $employeeInput = Input::except('user');

            $employee->fill($employeeInput);
            $employee->save();

            $employee->user->first  = Input::get('user.first');
            $employee->user->last   = Input::get('user.last');
            $employee->user->email  = Input::get('user.email');
            $employee->user->status = Input::get('user.status');
            $pwd                    = Input::get('user.password', '');
            if (!empty($pwd)) {
                $employee->user->password = Input::get('user.password');
            }
            $employee->user->save();

            return form_redirect(Input::get('_destination', false),
              $employee->company . ' has been updated.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $employee = Employee::find($id);
        $employee->user->delete();
        $employee->delete();

        return Redirect::route('admin.employees.index')
                       ->with(['msg' => 'Account deleted.']);
    }

    public function jobs($id, $jid = 0)
    {
        if ($jid) {
            return Employee::find($id)->jobs()->find($jid);
        }

        return Employee::find($id)->jobs;

    }

    public function services($id, $sid = 0)
    {
        if ($sid) {
            return Employee::find($id)->services()->find($sid);
        }

        return Employee::find($id)->services;
    }

    private function valRules($id = 0)
    {
        return array(
          'user.first'     => 'required',
          'user.last'      => 'required',
          'user.email'     => 'required|email|unique:users' . ($id ? ',email,' . $id : ',email'),
          'user.password'  => 'confirmed|min:6' . ($id ? '' : '|required'),
          'company'        => 'required',
          'phone1'         => 'required',
          'address'        => 'required',
          'birthday'       => 'date',
          'default_rate'   => 'numeric',
          'invoice_option' => 'required',
        );

    }

}