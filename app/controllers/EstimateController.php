<?php

use Vitrix\Mailers\AdminMailer;

class EstimateController extends \BaseController
{

    protected $adminMailer;

    function __construct(AdminMailer $adminMailer)
    {
        $this->adminMailer = $adminMailer;
    }

    /**
     * Display a listing of the resource.
     * GET /estimate
     *
     * @return Response
     */
    public function index()
    {
        $estimates = Estimate::with('customer')->get();
        return View::make('estimates.list', compact('estimates'));
    }

    /**
     * Show the form for creating a new resource.
     * GET /estimate/create
     *
     * @return Response
     */
    public function create()
    {
        $customers_meta = Customer::where('status', '=', '1')->get();
        JavaScript::put([
          'customerRates'     => $customers_meta->lists('default_rate', 'id'),
          'customerAddresses' => $customers_meta->lists('billing_address',
            'id'),
          'employeeRates'     => []
        ]);


        $estimate = new Estimate();
        if (Input::has('customerId')) {
            $estimate->customer_id = Input::get('customerId');
        }
        $service = new Service();
        return View::make('estimates.form', [
          'new'      => true,
          'estimate' => $estimate,
          'service'  => $service
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * POST /estimate
     *
     * @return Response
     */
    public function store()
    {
        $input      = Input::all();
        $validation = Validator::make($input, $this->valRules());

        if ($validation->fails()) {

            return Redirect::back()->withErrors($validation)->withInput();
        } else {
            $estimate       = new Estimate(Input::except('service'));
            $estimate->data = Input::get('service');
            $estimate->save();

            return Redirect::route('admin.estimates.show', $estimate->id)
                           ->with(['msg' => 'Estimate created']);
        }
    }

    /**
     * Display the specified resource.
     * GET /estimate/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $estimate = Estimate::with('customer')->find($id);
        $service  = new Service();

        $estimate->service = $estimate->data;
        return View::make('estimates.view',
          compact('estimate', 'service', 'groupedServices'));
    }

    /**
     * Show the form for editing the specified resource.
     * GET /estimate/{id}/edit
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $estimate          = Estimate::find($id);
        $estimate->service = $estimate->data;

        $service = new Service();
        return View::make('estimates.form', [
          'new'      => false,
          'estimate' => $estimate,
          'service'  => $service
        ]);
    }

    /**
     * Update the specified resource in storage.
     * PUT /estimate/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        $input      = Input::all();
        $validation = Validator::make($input, $this->valRules());

        if ($validation->fails()) {

            return Redirect::back()->withErrors($validation)->withInput();
        } else {
            $estimate = Estimate::find($id);
            $estimate->fill(Input::except('service'));
            $estimate->data = Input::get('service');
            $estimate->save();

            return Redirect::route('admin.estimates.show', $estimate->id)
                           ->with(['msg' => 'Estimate updated']);
        }
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /estimate/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $estimate = Estimate::find($id);
        $estimate->delete();

        $redirect = Input::get('_destination', false);
        if (strpos($redirect, $id) === false) {
            return form_redirect(Input::get('_destination', false),
              'Estimate deleted.');
        } else {
            return Redirect::route('admin.estimates.index')
                           ->with(['msg' => 'Estimate deleted.']);
        }
    }


    public function sendEstimate($id)
    {
        $estimate = Estimate::with('customer')->find($id);
        $email    = Input::all();

        $estimate->last_sent_date = Carbon::now();
        $estimate->save();

        $this->adminMailer->sendEstimate($email, $estimate);

        return Redirect::back()
                       ->with(['msg' => 'Estimate sent.']);

    }

    public function acceptEstimate($id)
    {
        $estimate             = Estimate::find($id);
        $service              = new Service($estimate->data);
        $service->customer_id = $estimate->customer_id;
        $service->status      = 1;
        $service->save();

        $estimate->accepted_date = Carbon::now();
        $estimate->service_id    = $service->id;
        $estimate->accepted      = true;
        $estimate->save();

        return Redirect::back()
                       ->with(['msg' => 'Estimate accepted.']);

    }


    private function valRules($id = 0)
    {
        $rules = array(
          'estimate_date'         => 'required|date',
          'customer_id'           => 'required',
          'estimate_number'       => 'required',
          'service.customer_rate' => 'numeric|required',
          'service.site_name'     => 'required',
        );


        return $rules;
    }

}