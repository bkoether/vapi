<?php

class UserAdminController extends \BaseController
{

    public function dashboard()
    {
        $from         = Carbon::now()->startOfWeek();
        $to           = Carbon::now()->endOfWeek();
        $jobs         = Job::where('scheduled', true)
                           ->whereBetween('scheduled_at', [$from, $to])
                           ->get();
        $completed    = $jobs->filter(function ($j) { return $j->completed; });
        $overdue      = $jobs->filter(function ($j) {
            if ($j->scheduled_at->lt(Carbon::now()
                                           ->startOfDay()) && !$j->completed
            ) {
                return true;
            }
            return false;
        });
        $not_invoiced = Job::where('invoiced', false)
                           ->where('completed', true)
                           ->count();


        return View::make('dashboards.admin')->with([
          'jobs'         => $jobs,
          'completed'    => $completed->count(),
          'overdue'      => $overdue->count(),
          'not_invoiced' => $not_invoiced
        ]);
    }

}