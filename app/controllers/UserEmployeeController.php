<?php

use Vitrix\Transformers\JobTransformer;
use Vitrix\Mailers\EmployeeMailer;
use Vitrix\EmployeeFunctions;

class UserEmployeeController extends \BaseController
{

    protected $total;
    protected $transformer;
    protected $employeeMailer;

    function __construct(
      JobTransformer $transformer,
      EmployeeMailer $employeeMailer
    ) {
        $this->transformer    = $transformer;
        $this->employeeMailer = $employeeMailer;
    }


    public function dashboard()
    {
        $today = Job::with('customer', 'service')
                    ->upcoming()
                    ->thisUser()
                    ->where('scheduled_at', '<', Carbon::tomorrow())
                    ->orderBy('scheduled_at')
                    ->get();

        $upcoming = Job::with('customer', 'service')
                       ->whereBetween('scheduled_at', [
                         Carbon::tomorrow(),
                         Carbon::now()->addDays(7)
                       ])
                       ->upcoming()
                       ->thisUser()
                       ->orderBy('scheduled_at')
                       ->get();

        return View::make('dashboards.employee')
                   ->with(compact('today', 'upcoming', 'options'));
    }

    public function directory()
    {
        $employees = User::with('employee')
                         ->where('status', 1)
                         ->where('role', 'employee')
                         ->orderBy('last')
                         ->get();

        return View::make('employees.directory')->with(compact('employees'));
    }

    public function submitReport()
    {
        $input           = Input::all();
        $upload_redirect = [];

        $email_data = [];
        foreach ($input['job'] as $id => $data) {
            if (!$data['status']) {
                continue;
            }

            $email_data[$id]['file'] = false;


            if (isset($data['upload'])) {
                $upload_redirect[]       = $id;
                $email_data[$id]['file'] = true;
            }

            switch ($data['status']) {
                case '1':
                    $job = Job::with('service', 'customer', 'document')
                              ->find($id);

                    $job->completed        = true;
                    $job->employee_comment = $data['comment'];
                    $job->completed_at     = $data['date'];
                    $job->save();

                    $email_data[$id]['status'] = 'Completed';

                    if ($job->service->invoice_type == 'A') {
                        $this->employeeMailer->sendCustomerInvoice($job);
                    }
                    break;

                case '2':
                    $job                   = Job::with('service')->find($id);
                    $job->scheduled_at     = $data['date'];
                    $job->employee_comment = $data['comment'];
                    $job->save();

                    $email_data[$id]['status'] = 'Rescheduled';
                    break;

                case '3':
                    $job                   = Job::with('service')->find($id);
                    $job->completed        = true;
                    $job->completed_at     = Carbon::now()->toDateString();
                    $job->employee_comment = $data['comment'];
                    $job->customer_rate    = 0;
                    $job->employee_rate    = 0;
                    $job->cancelled        = true;
                    $job->save();

                    $email_data[$id]['status'] = 'Cancelled';
                    break;
            }
            $email_data[$id]['job'] = $job;
        }
        $this->employeeMailer->sendReport(Auth::user(), $email_data);

        if (empty($upload_redirect)) {
            return Redirect::back()->with(['msg' => 'Report submitted.']);
        } else {
            return Redirect::to('upload?job_ids=' . implode(',',
                $upload_redirect))
                           ->with(['msg' => 'Reports submitted.']);
        }
    }

    public function documentList()
    {
        if (Input::has('job_ids')) {
            $job_ids = explode(',', Input::get('job_ids'));
            $jobs    = Job::whereIn('id', $job_ids)->with('service')->get();

            return View::make('documents.form', compact('jobs'));

        } else {
            return Redirect::to('');
        }
    }

    public function documentUpload()
    {
        foreach (Input::file('documents') as $jId => $doc) {
            if (!is_null($doc)) {
                $document         = new Document();
                $document->file   = $doc;
                $document->job_id = $jId;
                $document->save();
            }
        }

        return Redirect::to('')
                       ->with(['msg' => 'Documents have been successfully uploaded.']);

    }

    public function schedule()
    {
        if (Input::has('updated') || Input::has('done')) {
            Session::flash('msg', 'Schedule updated.');
        }

        $completed_jobs = [];
        $jobs           = Job::thisUser()
                             ->with('service', 'customer')
                             ->where('completed', 1)
                             ->orderBy('due_end', 'desc')
                             ->get();
        foreach ($jobs as $job) {
            $completed_jobs[] = $this->transformer->transform($job, true, true);
        }

        $scheduled_jobs = [];
        $jobs           = Job::thisUser()
                             ->with('service', 'customer')
                             ->where('scheduled', 1)
                             ->where('completed', 0)
                             ->orderBy('due_end', 'desc')
                             ->get();

        foreach ($jobs as $job) {
            $scheduled_jobs[] = $this->transformer->transform($job);
        }

        $vacationList = [];
        $vacations    = Vacation::where('employee_id',
          Auth::user()->employee->id)
                                ->get();
        if (count($vacations)) {
            foreach ($vacations as $vacation) {
                $vacationList[] = [
                  'id'     => $vacation->id,
                  'title'  => 'Time Off',
                  'allDay' => true,
                  'start'  => $vacation->start_date->toDateString(),
                  'end'    => $vacation->end_date->toDateString(),
                ];
            }
        }
        JavaScript::put([
          'jobs'      => $scheduled_jobs,
          'completed' => $completed_jobs,
          'jobsCount' => count($scheduled_jobs),
          'postPath'  => 'jobs',
          'role'      => 'employee',
          'vacations' => $vacationList
        ]);

        $to_schedule = Job::where('jobs.employee_id',
          Auth::user()->employee->id)
                          ->with('service', 'customer')
                          ->leftJoin('services', 'jobs.service_id', '=',
                            'services.id')
                          ->leftJoin('service_routes', 'service_routes.id', '=',
                            'services.service_route_id')
                          ->where('scheduled', 0)
                          ->orderBy('due_end')
                          ->orderBy('services.site_name', 'ASC')
                          ->select('jobs.*', 'service_routes.name')
                          ->get();
        $to_schedule->each(function ($job) {
            if ($job->service) {
                $job->last_completed = $job->service->lastCompleted();
            }
        });

        return View::make('jobs.schedule', compact('to_schedule'));
    }

    public function jobs()
    {
        $jobs = Job::with('customer', 'service')
                   ->orderBy('scheduled', 'desc')
                   ->orderBy('scheduled_at')
                   ->thisUser()
                   ->upcoming()
                   ->get();

        $services = Service::where('employee_id', Auth::user()->employee->id)
                           ->with('customer')
                           ->orderBy('site_name')
                           ->get();

        return View::make('jobs.employee-list')->with(compact('jobs',
          'services'));
    }

    public function jobView($id)
    {
        $job = Job::with('service', 'customer')->thisUser()->find($id);
        if ($job->service) {
            $job->last_completed = $job->service->lastCompleted();
        }
        return View::make('jobs.employee-view', compact('job'));
    }

    public function serviceView($id)
    {
        $service = Service::with('employee', 'customer')->thisUser()->find($id);

        return View::make('services.employee-view', compact('service'));
    }

    public function scheduleJobs()
    {
        $dates = Input::json()->all();
//    Log::alert($dates);
        foreach ($dates as $date) {
            $job               = Job::find($date['id']);
            $job->scheduled    = true;
            $job->scheduled_at = Carbon::parse($date['date'])->toDateString();
            $job->save();
        }

        if (Auth::user()->role == 'employee') {
            $this->employeeMailer->sendSchedule(Auth::user());
        }

    }

    public function reports()
    {
        $employee       = Employee::where('user_id', Auth::user()->id)->first();
        $invoice_option = $employee->invoice_option ?: 'monthly';
        $split          = false;
        $calc           = new EmployeeFunctions();

        if (Input::has('date')) {
            $month      = Input::get('date');
            $calc_month = Carbon::parse($month);

            $date = [
              $calc_month->firstOfMonth()->toDateString(),
              $calc_month->addDays(14)->toDateString(),
              $calc_month->addDay()->toDateString(),
              $calc_month->lastOfMonth()->toDateString()
            ];
        } else {
            $calc_month = Carbon::now();
            $date       = [
              $calc_month->firstOfMonth()->toDateString(),
              $calc_month->addDays(14)->toDateString(),
              $calc_month->addDay()->toDateString(),
              $calc_month->lastOfMonth()->toDateString()
            ];
            $month      = $calc_month->format('Y-m');
        }


        $part1 = array();
        $part2 = array();

        if ($invoice_option == 'semi_monthly') {
            $split = true;
            $part1 = $calc->calculateInvoice($employee, $date[0], $date[1]);
            $part2 = $calc->calculateInvoice($employee, $date[2], $date[3], 2);
        } else {
            $part1 = $calc->calculateInvoice($employee, $date[0], $date[3]);
        }


        return View::make('reports.employee')->with([
          'month'    => $month,
          'employee' => $employee,
          'part1'    => $part1,
          'part2'    => $part2,
          'split'    => $split
        ]);
    }

    public function submitInvoice()
    {
        if (Input::has('month')) {
            $invoice              = new Invoice();
            $invoice->employee_id = Input::get('employee_id');
            $invoice->month       = Carbon::parse(Input::get('month'))
                                          ->format('Y-m');
            $invoice->month_part  = Input::get('month_part');
            $invoice->number      = Input::get('invoice_number');
            $invoice->save();

            $this->employeeMailer->sendInvoice($invoice);

            return Redirect::back()->with(['msg' => 'Invoice submitted.']);
        } else {
            return Redirect::back();
        }


    }

    public function accountView()
    {
        $employee = Employee::with('user')
                            ->where('user_id', Auth::user()->id)
                            ->get()->first();

        return View::make('employees.my-account', ['employee' => $employee]);
    }

    public function accountUpdate()
    {
        $employee = Employee::with('user')
                            ->where('user_id', Auth::user()->id)
                            ->get()->first();


        $validation = Validator::make(Input::all(),
          $this->valRules($employee->user_id));

        if ($validation->fails()) {
            return Redirect::action('UserEmployeeController@accountView')
                           ->withInput()
                           ->withErrors($validation);
        } else {
            $employeeInput = Input::except('user');

            $employee->fill($employeeInput);
            $employee->save();

            $employee->user->first = Input::get('user.first');
            $employee->user->last  = Input::get('user.last');
            $employee->user->email = Input::get('user.email');

            $pwd = Input::get('user.password', '');
            if (!empty($pwd)) {
                $employee->user->password = Input::get('user.password');
            }
            $employee->user->save();

            return Redirect::action('UserEmployeeController@accountView')
                           ->with(['msg' => 'Information updated.']);
        }
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function sendCashInvoice($id)
    {
        if (Input::has('emails')) {
            $emails   = explode(',', Input::get('emails'));
            $toEmails = [];
            foreach ($emails as $email) {
                $email = trim($email);
                if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $toEmails[] = $email;
                } else {
                    return Response::json([
                      'status'  => 'error',
                      'message' => 'One of the emails provided is not valid.'
                    ]);
                }
            }
            $toEmails = array_filter($toEmails);

            $job = Job::with('service', 'employee', 'customer')->find($id);

            $job->cash_invoice_sent = 1;
            $job->invoiced          = 1;
            $job->invoiced_at       = Carbon::now()->toDateString();
            $job->save();

            $this->employeeMailer->sendCashReceipt($job, $toEmails);


            return Response::json(['status' => 'success']);
        } else {
            return Response::json([
              'status'  => 'error',
              'message' => 'Please provide at least one email address.'
            ]);
        }

    }

    private function valRules($id = 0)
    {
        return array(
          'user.first'    => 'required',
          'user.last'     => 'required',
          'user.email'    => 'required|email|unique:users' . ($id ? ',email,' . $id : ',email'),
          'user.password' => 'confirmed|min:6' . ($id ? '' : '|required'),
          'company'       => 'required',
          'phone1'        => 'required',
          'address'       => 'required',
        );

    }


}