<?php

class ServiceRoutesController extends \BaseController
{

    /**
     * Display a listing of the resource.
     * GET /routes
     *
     * @return Response
     */
    public function index()
    {
        $routes = ServiceRoute::where('employee_id', Auth::user()->employee->id)
                              ->with('services')
                              ->get();


        return View::make('routes.list')->with(compact('routes'));
    }

    /**
     * Show the form for creating a new resource.
     * GET /routes/create
     *
     * @return Response
     */
    public function create()
    {
        $new      = true;
        $services = Service::where('employee_id', Auth::user()->employee->id)
                           ->where('service_route_id', null)
                           ->get();


        return View::make('routes.form')->with(compact('services', 'new'));
    }

    /**
     * Store a newly created resource in storage.
     * POST /routes
     *
     * @return Response
     */
    public function store()
    {
        $validation = Validator::make(Input::all(), $this->valRules());

        if ($validation->fails()) {
            return Redirect::route('service-routes.create')
                           ->withInput()
                           ->withErrors($validation);
        } else {
            $route              = new ServiceRoute();
            $route->name        = Input::get('name');
            $route->employee_id = Auth::user()->employee->id;
            $route->save();

            $services = Input::get('services');
            foreach ($services as $sid) {
                $service                   = Service::find($sid);
                $service->service_route_id = $route->id;
                $service->save();
            }

            return Redirect::route('service-routes.index')
                           ->with(['msg' => 'Route created.']);
        }

    }

    /**
     * Display the specified resource.
     * GET /routes/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     * GET /routes/{id}/edit
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $route    = ServiceRoute::with('services')->find($id);
        $services = Service::where('employee_id', Auth::user()->employee->id)
                           ->where('service_route_id', null)
                           ->get();
        return View::make('routes.edit')->with(compact('route', 'services'));

    }

    /**
     * Update the specified resource in storage.
     * PUT /routes/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        $route       = ServiceRoute::find($id);
        $route->name = Input::get('name');

        if (Input::has('services')) {
            $services = Input::get('services');
            foreach ($services as $sid) {
                $service                   = Service::find($sid);
                $service->service_route_id = $route->id;
                $service->save();
            }
        }

        if (Input::has('remove')) {
            $services = Input::get('remove');
            foreach ($services as $sid) {
                $service                   = Service::find($sid);
                $service->service_route_id = null;
                $service->save();
            }
        }

        return Redirect::route('service-routes.index')
                       ->with(['msg' => 'Route updated.']);
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /routes/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $route = ServiceRoute::with('services')->find($id);
        foreach ($route->services as $s) {
            $service                   = Service::find($s->id);
            $service->service_route_id = null;
            $service->save();
        }
        $route->delete();
        return Redirect::route('service-routes.index')
                       ->with(['msg' => 'Route deleted.']);
    }

    private function valRules()
    {
        return array(
          'name' => 'required'
        );
    }

}