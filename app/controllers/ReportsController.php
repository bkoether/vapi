<?php

class ReportsController extends \BaseController
{


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        $date       = Carbon::now()
                            ->firstOfMonth()
                            ->format('F j, Y') . ' - ' . Carbon::now()
                                                               ->lastOfMonth()
                                                               ->format('F j, Y');
        $from       = Carbon::now()->firstOfMonth()->toDateString();
        $to         = Carbon::now()->lastOfMonth()->toDateString();
        $employee   = '';
        $customer   = '';
        $uninvoiced = 0;
        $unfiled    = 0;
        $chain      = Input::get('chain');
        $where      = [];
        $input      = Input::all();

        if (Input::has('date')) {
            $date    = $input['date'];
            $from_to = explode(' - ', $date);
            $from    = Carbon::parse($from_to[0])->toDateString();
            $to      = Carbon::parse($from_to[1])->toDateString();
        }
        if (Input::has('employee') && !empty($input['employee'])) {
            $employee = $input['employee'];
            $where[]  = 'employee_id = ' . $input['employee'];
        }
        if (Input::has('customer') && !empty($input['customer'])) {
            $customer = $input['customer'];
            $where[]  = 'customer_id = ' . $input['customer'];
        }
        if (Input::has('uninvoiced')) {
            $where[]    = 'invoiced = 0';
            $uninvoiced = 1;
        }

        if (Input::has('unfiled')) {
            $where[] = '(filed_at IS NULL OR filed_at = 0000-00-00)';
            $unfiled = 1;
        }


        if (empty($where)) {
            $jobs = Job::with('customer', 'service', 'employee', 'document', 'employee.user')
                       ->whereBetween('completed_at', [$from, $to])
                       ->where('completed', '1')
                       ->get();
        } else {

            $jobs = Job::with('customer', 'service', 'employee', 'document', 'employee.user')
                       ->whereBetween('completed_at', [$from, $to])
                       ->where('completed', '1')
                       ->whereRaw(implode(' AND ', $where))
                       ->get();
        }

        if (Input::has('chain')) {
            $jobs = $jobs->filter(function ($e) {
                return $e->customer->chain == Input::get('chain') ? true : false;
            });
        }


        return View::make('reports.list')->with(
          compact('date', 'jobs', 'customer', 'employee', 'uninvoiced', 'chain',
            'unfiled')
        );
    }

    public function update()
    {
        if (Input::has('jobs') && Input::has('updateType')) {
            $today   = Carbon::now();
            $job_ids = Input::get('jobs');
            $type    = Input::get('updateType');

            foreach ($job_ids as $id) {
                $job = Job::find($id);

                if ($type == 'invoiced') {
                    $job->invoiced    = true;
                    $job->invoiced_at = $today;
                } elseif ($type == 'filed') {
                    $job->filed_at = $today;
                }

                $job->save();
            }

            return Response::json(['status' => 'success']);

        } else {
            return Response::json(['status' => 'error']);
        }
    }

}