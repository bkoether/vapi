<?php

use Vitrix\ConfigWriter;

class ConfigController extends \BaseController
{

    public function index()
    {
        $config = Config::get('vitrex');

        $service_types = "";
        foreach ($config['service_types'] as $id => $name) {
            $service_types .= $id . '|' . $name . "\n";
        }
        $config['service_types'] = $service_types;

        $chains = "";
        foreach ($config['chains'] as $name) {
            $chains .= $name . "\n";
        }

        $config['chains'] = $chains;

        return View::make('config.form', compact('config'));

    }

    public function update()
    {
        $input      = Input::all();
        $validation = Validator::make($input, $this->valRules());
        if ($validation->fails()) {
            return Redirect::action('ConfigController@index')
                           ->withInput()
                           ->withErrors($validation);
        } else {
            $configWriter = new ConfigWriter(new \Illuminate\Filesystem\Filesystem(),
              base_path() . '/app/config');
            $settings     = array(
              'service_types' => [],
              'chains'        => [],
              'emails'        => [
                'reports'   => trim($input['reports_email']),
                'invoices'  => trim($input['invoice_emails']),
                'starbucks' => trim($input['starbucks_email']),
              ],
            );

            $services = explode("\n", trim($input['service_types']));
            foreach ($services as $line) {
                $parts = explode('|', $line);
                if (count($parts) == 2) {
                    $settings['service_types'][$parts[0]] = trim($parts[1]);
                }
            }

            $chains = explode("\n", trim($input['chains']));
            foreach ($chains as $line) {
                $settings['chains'][] = trim($line);
            }


            $configWriter->save($settings, '', 'vitrex');

            return Redirect::action('ConfigController@index')
                           ->with(['msg' => 'Settings saved.']);
        }
    }

    private function valRules()
    {
        return array(
          'service_types'  => 'required',
          'reports_email'  => 'required',
          'invoice_emails' => 'required',
          'chains'         => 'required',
        );
    }

}