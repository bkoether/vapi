<?php

use Vitrix\Transformers\UserTransformer;

class UsersController extends \BaseController
{

    protected $userTransformer;

    function __construct(UserTransformer $userTransformer)
    {
        $this->userTransformer = $userTransformer;
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $sortBy = Request::get('sortBy', 'last');
        $sort   = Request::get('sort', 'asc');

        $admins = User::orderBy($sortBy, $sort)
                      ->where('role', '=', 'admin')
                      ->paginate(10);

        return View::make('users.list', compact('admins'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('users.form', ['new' => true, 'own' => false]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input      = Input::all();
        $validation = Validator::make($input, $this->valRules());

        if ($validation->fails()) {
            return Redirect::route('admin.users.create')
                           ->withInput()
                           ->withErrors($validation);
        } else {
            User::create($input);
            return Redirect::route('admin.users.index')
                           ->with(['msg' => 'Account created.']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        return Redirect::route('admin.users.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return View::make('users.form', [
          'user' => $user,
          'new'  => false,
          'own'  => (Auth::user()->id == $id ? true : false)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        $validation = Validator::make(Input::all(), $this->valRules($id));

        if ($validation->fails()) {
            return Redirect::route('admin.users.edit', $id)
                           ->withInput()
                           ->withErrors($validation);
        } else {
            $user         = User::find($id);
            $user->first  = Input::get('first');
            $user->last   = Input::get('last');
            $user->email  = Input::get('email');
            $user->status = Input::get('status');

            $pwd = Input::get('password', '');
            if (!empty($pwd)) {
                $user->password = Input::get('password');
            }
            $user->save();
            return Redirect::route('admin.users.index')
                           ->with(['msg' => 'Account updated.']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return Redirect::route('admin.users.index')
                       ->with(['msg' => 'Account deleted.']);
    }

    private function valRules($id = 0)
    {
        return array(
          'first'    => 'required',
          'last'     => 'required',
          'email'    => 'required|email|unique:users,email' . (($id ? ',' . $id : '')),
          'password' => 'confirmed|min:6' . ($id ? '' : '|required')
        );
    }


}