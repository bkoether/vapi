<?php

use Vitrix\JobScheduler;

class ServicesController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $services = Service::with('customer', 'employee')->get();

        return View::make('services.list', compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $service = new Service;
        if (Input::has('clone')) {
            $clone = Service::find(Input::get('clone'));
            $service->fill($clone->toArray());
        } else {
            $service->status      = 1;
            $service->frequency   = 'B';
            $service->employee_id = Input::has('employeeId') ? Input::get('employeeId') : '';
            $service->customer_id = Input::has('customerId') ? Input::get('customerId') : 0;
        }
        $service->next_schedule = date('Y-m-d');
        $customers_meta         = Customer::where('status', '=', '1')->get();
        JavaScript::put([
          'employeeRates'     => Employee::all()->lists('default_rate', 'id'),
          'customerRates'     => $customers_meta->lists('default_rate', 'id'),
          'customerAddresses' => $customers_meta->lists('billing_address',
            'id'),
        ]);

        $serviceTypes = Config::get('vitrex.service_types');
        asort($serviceTypes);

        return View::make('services.form', [
          'service' => $service,
          'new'     => true,
          'serviceTypes' => $serviceTypes
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input      = Input::all();
        $validation = Validator::make($input, $this->valRules());
        $validation->sometimes('employee_rate', 'required', function ($input) {
            return $input->employee_id > 0;
        });


        if ($validation->fails()) {
            return Redirect::route('admin.services.create')
                           ->withInput()
                           ->withErrors($validation);
        } else {
            $service = Service::create($input);


            foreach (Input::file('photos') as $photo) {
                if (!is_null($photo)) {
                    $locationImage        = new LocationImage();
                    $locationImage->photo = $photo;
                    $service->locationImages()->save($locationImage);
                }
            }
            return form_redirect(Input::get('_destination', false),
              $service->site_name . ' has been created.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        try {
            $service = Service::with(array(
              'customer',
              'employee',
              'jobs' => function ($query) {
                  $query->where('completed', '=', '0')
                        ->orderBy('due_start', 'ASC');
              }
            ))->findOrFail($id);
            return View::make('services.view', compact('service'));
        } catch (ModelNotFoundException $e) {
            return Redirect::route('admin.services.index');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $customers_meta = Customer::where('status', '=', '1')->get();
        JavaScript::put([
          'employeeRates'     => Employee::all()->lists('default_rate', 'id'),
          'customerRates'     => $customers_meta->lists('default_rate', 'id'),
          'customerAddresses' => $customers_meta->lists('billing_address',
            'id'),
        ]);

        $service = Service::find($id);

        $serviceTypes = Config::get('vitrex.service_types');
        asort($serviceTypes);

      return View::make('services.form', [
          'service' => $service,
          'new'     => false,
          'serviceTypes' => $serviceTypes
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        $validation = Validator::make(Input::all(), $this->valRules($id));
        $validation->sometimes('employee_rate', 'required', function ($input) {
            return $input->employee_id > 0;
        });

        if ($validation->fails()) {
            return Redirect::route('admin.services.edit', $id)
                           ->withInput()
                           ->withErrors($validation);
        } else {
            $input   = Input::all();
            $service = Service::find($id);
            $service->fill($input);
            $service->status = Input::get('status');
            $service->save();

            if (Input::has('delete_image')) {
                LocationImage::destroy(Input::get('delete_image'));
            }

            foreach (Input::file('photos') as $photo) {
                if (!is_null($photo)) {
                    $locationImage        = new LocationImage();
                    $locationImage->photo = $photo;
                    $service->locationImages()->save($locationImage);
                }
            }

            return form_redirect(Input::get('_destination', false),
              $service->site_name . ' has been updated.');

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $service = Service::find($id);
        $service->delete();

        $redirect = Input::get('_destination', false);
        if (strpos($redirect, $id) === false) {
            return form_redirect(Input::get('_destination', false),
              'Service deleted.');
        } else {
            return Redirect::route('admin.services.index')
                           ->with(['msg' => 'Service deleted.']);
        }
    }

    private function valRules($id = 0)
    {
        return array(
          'customer_id'   => 'required',
          'type'          => 'required',
          'customer_rate' => 'numeric',
          'employee_rate' => 'numeric',
          'invoice_type'  => 'required',
          'frequency'
        );
    }


    public function jobs($id)
    {
        return Service::find($id)->jobs;
    }

    public function generateJobs($id)
    {
        $scheduler = new JobScheduler(Carbon::now()->addMonth());
        $scheduler->scheduleService($id);

        return Redirect::route('admin.services.show', $id)
                       ->with(['msg' => 'Processing completed. ' . $scheduler->status]);
    }
}