<?php

use Vitrix\Mailers\EmployeeMailer;
use Vitrix\Transformers\JobTransformer;
use Thujohn\Pdf;

class JobsController extends \BaseController
{

    protected $transformer;
    protected $employeeMailer;

    function __construct(JobTransformer $transformer, EmployeeMailer $employeeMailer)
    {
        $this->transformer = $transformer;
        $this->employeeMailer = $employeeMailer;
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $date        = Carbon::now()
                             ->firstOfMonth()
                             ->format('F j, Y') . ' - ' . Carbon::now()
                                                                ->lastOfMonth()
                                                                ->format('F j, Y');
        $from        = Carbon::now()->firstOfMonth()->toDateString();
        $to          = Carbon::now()->lastOfMonth()->toDateString();
        $employee    = '';
        $customer    = '';
        $chain       = Input::get('chain');
        $date_filter = Input::has('date_filter') ? Input::get('date_filter') : 'scheduled_at';
        $where       = [];
        $input       = Input::all();

        if (Input::has('date')) {
            $date    = $input['date'];
            $from_to = explode(' - ', $date);
            $from    = Carbon::parse($from_to[0])->toDateString();
            $to      = Carbon::parse($from_to[1])->toDateString();
        }
        if (Input::has('employee') && !empty($input['employee'])) {
            $employee = $input['employee'];
            $where[]  = 'employee_id = ' . $input['employee'];
        }
        if (Input::has('customer') && !empty($input['customer'])) {
            $customer = $input['customer'];
            $where[]  = 'customer_id = ' . $input['customer'];
        }

        if (empty($where)) {
            $jobs = Job::with('customer', 'service', 'employee')
                       ->whereBetween("$date_filter", [$from, $to])
                       ->get();
        } else {
            $jobs = Job::with('customer', 'service', 'employee')
                       ->whereBetween($date_filter, [$from, $to])
                       ->whereRaw(implode(' AND ', $where))
                       ->get();
        }

        if (Input::has('chain')) {
            $jobs = $jobs->filter(function ($e) {
                if ($e->customer) {
                    $i = Input::get('chain', '');
                    return $e->customer->chain == $i ? true : false;
                }
                return false;
            });
        }

        return View::make('jobs.list',
          compact('jobs', 'date', 'customer', 'employee', 'chain',
            'date_filter'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        // Create Job for service
        if (Input::has('serviceId')) {
            $service = Service::with('customer', 'employee')
                              ->find(Input::get('serviceId'));
            $job     = new Job();
            $job->preFill($service->toArray(), 'service');

            return View::make('jobs.form', [
              'job'      => $job,
              'service'  => $service,
              'customer' => $service->customer,
              'new'      => true,
              'custom'   => true
            ]);
        } else if (Input::has('customerId')) {
            $customer = Customer::find(Input::get('customerId'));
            $job      = new Job();
            $job->preFill($customer->toArray(), 'customer');

            return View::make('jobs.form', [
              'job'      => $job,
              'customer' => $customer,
              'new'      => true,
              'custom'   => true
            ]);
        } else {
//      return Redirect::back()->withErrors(['Missing reference to customer or service']);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = Input::all();
        if (!isset($input['due_start']) && isset($input['due_end'])) {
            $input['due_start'] = $input['due_end'];
        }

        $validation = Validator::make($input, $this->valRules());

        if ($validation->fails()) {
            $q = '?' . Input::get('qstring');

            return Redirect::to('admin/jobs/create' . $q)
                           ->withErrors($validation)
                           ->withInput();
        } else {
            $job            = Job::create($input);
            $job->invoiced  = !empty($input['invoiced_at']);
            $job->completed = !empty($input['completed_at']);
            $job->scheduled = !empty($input['scheduled_at']);
            $job->save();

            return form_redirect(Input::get('_destination', false),
              'Job created.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $job = Job::with('service', 'employee.user', 'customer')->find($id);
        if (empty($job)) {
            return Redirect::route('admin.jobs.index')
                           ->with(['msg' => 'Job could not be found.']);
        }
        if ($job->service) {
            $job->last_completed = $job->service->lastCompleted();
        }

        return View::make('jobs.view', compact('job'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $job = Job::with('service', 'customer', 'employee')->find($id);

        return View::make('jobs.form', [
          'job'      => $job,
          'service'  => $job->service,
          'customer' => $job->customer,
          'new'      => false
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        $job = Job::find($id);
        // Check if this is a quick edit
        if (Input::has('adminComment')) {
            $job->admin_comment = Input::get('adminComment');
            if (Input::has('markInvoiced')) {
                $inv              = Input::get('markInvoiced');
                $job->invoiced    = $inv ? 1 : 0;
                $job->invoiced_at = $inv ? date('Y-m-d') : null;
            }
            $job->save();
        } // Regular update
        else {
            $input = Input::all();
            if (!isset($input['due_start']) && isset($input['due_end'])) {
                $input['due_start'] = $input['due_end'];
            }
            $validation = Validator::make($input, $this->valRules($id));

            if ($validation->fails()) {
                return Redirect::route('admin.jobs.edit', $id)
                               ->withInput()
                               ->withErrors($validation);
            } else {
                $job->fill($input);
                $job->invoiced  = !empty($input['invoiced_at']);
                $job->completed = !empty($input['completed_at']);
                $job->scheduled = !empty($input['scheduled_at']);

                $job->save();
            }
        }

        return form_redirect(Input::get('_destination', false),
          'Job information updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $job = Job::find($id);
        $job->delete();

        return form_redirect(Input::get('_destination', false), 'Job deleted.');
    }


    public function calendar($id = 0)
    {
        if (Input::has('updated') || Input::has('done')) {
            Session::flash('msg', 'Schedule updated.');
        }

        $employee = Employee::find($id);

        $completed_jobs = [];
        $jobs           = Job::where('employee_id', $id)
                             ->with('service', 'customer')
                             ->where('completed', 1)
                             ->orderBy('due_end', 'desc')
                             ->get();
        foreach ($jobs as $job) {
            $completed_jobs[] = $this->transformer->transform($job, true, true);
        }


        $scheduled_jobs = [];
        $jobs           = Job::where('employee_id', $id)
                             ->with('service', 'customer')
                             ->where('scheduled', 1)
                             ->where('completed', 0)
                             ->orderBy('due_end', 'desc')
                             ->get();

        foreach ($jobs as $job) {
            $scheduled_jobs[] = $this->transformer->transform($job, true);
        }


        $vacationList = [];
        $vacations    = Vacation::where('employee_id', $id)->get();
        if (count($vacations)) {
            foreach ($vacations as $vacation) {
                $vacationList[] = [
                  'id'     => $vacation->id,
                  'title'  => 'Time Off',
                  'allDay' => true,
                  'start'  => $vacation->start_date->toDateString(),
                  'end'    => $vacation->end_date->toDateString(),
                ];
            }
        }


        JavaScript::put([
          'jobs'      => $scheduled_jobs,
          'completed' => $completed_jobs,
          'jobsCount' => count($scheduled_jobs),
          'vacations' => $vacationList,
          'postPath'  => '/admin/schedule',
          'role'      => 'admin'
        ]);

        $to_schedule = Job::where('jobs.employee_id', $id)
                          ->with('service', 'customer')
                          ->leftJoin('services', 'jobs.service_id', '=',
                            'services.id')
                          ->leftJoin('service_routes', 'service_routes.id', '=',
                            'services.service_route_id')
                          ->where('scheduled', 0)
                          ->orderBy('due_end')
                          ->orderBy('services.site_name', 'ASC')
                          ->select('jobs.*', 'service_routes.name')
                          ->get();
        $to_schedule->each(function ($job) {
            if ($job->service) {
                $job->last_completed = $job->service->lastCompleted();
            }
        });

        return View::make('jobs.scheduleAdmin',
          compact('to_schedule', 'employee'));
    }


    public function trash()
    {
        if (!Input::has('jobs')) {
            return Redirect::route('admin.jobs.index');
        }

        $jobIds = Input::get('jobs');

        $jobs = Job::with('service', 'customer')
                   ->whereIn('id', $jobIds)
                   ->get();

        return View::make('jobs.trash', compact('jobs'));

    }

    public function trashConfirm()
    {
        $jobs = Input::get('jobs');

        if (Input::has('delete')) {
            foreach ($jobs as $id) {
                $job = Job::find($id);
                $job->delete();
            }

            return Redirect::route('admin.jobs.index')
                           ->with(['msg' => 'Jobs deleted.']);
        } elseif (Input::has('cancel')) {
            foreach ($jobs as $id) {
                $job                = Job::find($id);
                $job->completed     = true;
                $job->completed_at  = Carbon::now()->toDateString();
                $job->customer_rate = 0;
                $job->employee_rate = 0;
                $job->cancelled     = true;
                $job->save();
            }

            return Redirect::route('admin.jobs.index')
                           ->with(['msg' => 'Jobs cancelled.']);
        }


    }

    public function printCashReceipt($id)
    {
        $job = Job::with('service', 'customer')->find($id);
        if ($job->service->invoice_type != 'C') {
            return Redirect::to('/')
                           ->with(['msg' => 'Print of cash receipt not available for this job.']);
        } else {
            $view = 'emails.employees.receipt_pdf';

            $receiptId = $job->scheduled_at->toDateString() . '-' . $job->id;

            $data = [
              'title'        => 'Cash Receipt',
              'id'           => $receiptId,
              'date'         => $job->scheduled_at->format('F jS, Y'),
              'to_name'      => $job->customer->contact_name,
              'to_company'   => $job->customer->name,
              'to_address'   => nl2br($job->customer->billing_address),
              'site_name'    => $job->service ? $job->service->site_name : '',
              'site_address' => $job->service ? nl2br($job->service->address) : '',
              'jobType'      => $job->service ? $job->service->getTypeName() : 'Custom Work',
              'total'        => number_format($job->customer_rate, 2),
              'tax'          => number_format($job->customer_rate * 0.05, 2),
              'grand_total'  => number_format($job->customer_rate * 1.05, 2),
            ];

            if (Auth::user()->role == 'employee') {
                $this->employeeMailer->sendPrintCopy($data);
            }



            $pdf = new Pdf\Pdf();

            $headers = array('Content-Type' => 'application/pdf');

            return Response::make($pdf->load(View::make($view)->with($data), 'letter')->show(), 200, $headers);
//            return $pdf->load(View::make($view)->with($data), 'letter')->show();
        }
    }


    private function valRules($id = 0)
    {
        return array(
          'due_start'     => 'required|date',
          'due_end'       => 'required|date',
          'customer_rate' => 'required|numeric',
          'employee_rate' => 'required|numeric',
        );
    }

}