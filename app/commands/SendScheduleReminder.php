<?php

use Indatus\Dispatcher\Scheduling\ScheduledCommand;
use Indatus\Dispatcher\Scheduling\Schedulable;
use Indatus\Dispatcher\Drivers\Cron\Scheduler;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Vitrix\Mailers\EmployeeMailer;

class SendScheduleReminder extends ScheduledCommand
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'vitrex:schedule';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send reminders if the schedule has not been completed by the 15th of the month.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * When a command should run
     *
     * @param Scheduler $scheduler
     *
     * @return \Indatus\Dispatcher\Scheduling\Schedulable
     */
    public function schedule(Schedulable $scheduler)
    {
        return $scheduler
          ->daysOfTheMonth('16-31')
          ->hours(12)
          ->minutes(0);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $mailer    = new EmployeeMailer();
        $employees = Employee::with(
          [
            'jobs' => function ($query) {
                $query->where('scheduled', false)
                      ->where('completed', false);
            },
            'user'
          ])
                             ->get();

        foreach ($employees as $employee) {
            if ($employee->jobs->count()) {
                $mailer->sendScheduleReminder($employee->user, $employee->jobs);
            }
        }

    }

}
