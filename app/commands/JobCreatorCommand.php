<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Vitrix\JobScheduler;

class JobCreatorCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'vitrix:job-generator';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates the jobs for a given month.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $month = $this->option('month') ? Carbon::parse($this->option('month')) : Carbon::now()
                                                                                        ->addMonth();
        $scheduler = new JobScheduler($month);

        // Process all
        if ($this->option('all')) {
            $this->info('Starting processing for all services. Starting month: ' . $month->format('Y-m'));
            $scheduler->scheduleAll();
        } elseif ($this->option('service')) {
            $this->info('Starting processing for service #' . $this->option('service') . '. Starting month: ' . $month->format('Y-m'));
            $scheduler->scheduleService($this->option('service'));

        } elseif ($this->option('employee')) {
            $this->info('Starting processing for employee #' . $this->option('employee') . '. Starting month: ' . $month->format('Y-m'));
            $scheduler->scheduleEmployee($this->option('employee'));

        } else {
            $this->error('Not enough options provided.');
            die();
        }
        $this->info($scheduler->status);
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
          array(
            'all',
            null,
            InputOption::VALUE_NONE,
            'Trigger the monthly generation of jobs.',
            null
          ),
          array(
            'month',
            null,
            InputOption::VALUE_OPTIONAL,
            'Sets the month the routine should be started at. Format: yyyy-mm',
            null
          ),
          array(
            'service',
            null,
            InputOption::VALUE_OPTIONAL,
            'Limit the job generation to a specific service.',
            null
          ),
          array(
            'employee',
            null,
            InputOption::VALUE_OPTIONAL,
            'Limit the job generation to a specific employee.',
            null
          ),
        );
    }

}
