<?php

use Carbon\Carbon;
use Indatus\Dispatcher\Scheduling\ScheduledCommand;
use Indatus\Dispatcher\Scheduling\Schedulable;
use Indatus\Dispatcher\Drivers\Cron\Scheduler;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Vitrix\Mailers\EmployeeMailer;

class SendReportReminder extends ScheduledCommand
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'vitrex:reports';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends out reminders for not reported jobs.';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * When a command should run
     *
     * @param Scheduler $scheduler
     *
     * @return \Indatus\Dispatcher\Scheduling\Schedulable
     */
    public function schedule(Schedulable $scheduler)
    {
        return $scheduler
          ->everyWeekday()
          ->hours(6)
          ->minutes(0);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
//    Log::debug('Report Reminder Fired');
        $mailer    = new EmployeeMailer();
        $employees = Employee::with([
          'jobs' => function ($query) {
              $query->where('scheduled', true)
                    ->where('scheduled_at', '<', Carbon::now()->toDateString())
                    ->where('completed', false);
          },
          'user'
        ])
                             ->get();

        foreach ($employees as $employee) {
            if ($employee->jobs->count()) {
                $mailer->sendReportReminder($employee->user, $employee->jobs);
            }
        }
    }

}
