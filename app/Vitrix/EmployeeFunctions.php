<?php
namespace Vitrix;

use Commission;
use Job;
use Employee;
use Carbon;
use Invoice;

class EmployeeFunctions
{

    protected $total;
    protected $cash;

    public function getMonthlyProjection(Carbon $month, Employee $employee)
    {
        $jobs = Job::where('employee_id', $employee->id)
                   ->whereBetween('scheduled_at', [
                     $month->startOfMonth()->toDateString(),
                     $month->endOfMonth()->toDateString()
                   ])
                   ->get();

        $return = [
          'count' => 0,
          'gross' => 0,
          'net'   => 0
        ];

        foreach ($jobs as $job) {
            $return['count']++;
            $return['gross'] = $return['gross'] + $job->customer_rate;
            $return['net']   = $return['net'] + $job->netValue();
        }

        return $return;
    }

    public function calculateInvoice(
      Employee $employee,
      $start,
      $end,
      $part = 1
    ) {
        $month = Carbon::parse($start)->format('Y-m');


        $data         = array();
        $data['from'] = $start;
        $data['to']   = $end;
        $data['jobs'] = Job::with('service', 'customer')
                           ->whereBetween('completed_at', [$start, $end])
                           ->where('employee_id', $employee->id)
                           ->orderBy('completed_at')
                           ->get();


        $data['invoice'] = Invoice::where('month', $month)
                                  ->where('employee_id', $employee->id)
                                  ->where('month_part', $part)
                                  ->first();

        $this->total         = 0;
        $data['tax']         = 0;
        $data['grand_total'] = 0;
        $data['total']       = $data['jobs']->sum(function ($row) {
            return $row->employee_rate;
        });

        $data['commission'] = Commission::whereBetween('created_at',
          [$start, $end])
                                        ->where('employee_id', $employee->id)
                                        ->sum('value');

        $data['total'] = $data['total'] + $data['commission'];
        $data['cash']  = $data['jobs']->sum(function ($row) {
            return $row->cashAmount();
        });


        if ($data['total']) {
            if (!empty($employee->tax_number)) {
                $data['tax'] = number_format(($data['total'] * 0.05), 2);
            }

            if ($data['cash']) {
                $data['cash']        = $data['cash'] * 1.05;
                $data['grand_total'] = number_format(($data['total'] + $data['tax'] - $data['cash']),
                  2);
            } else {
                $data['grand_total'] = number_format(($data['total'] + $data['tax']),
                  2);
            }
        }

        return $data;

    }
}