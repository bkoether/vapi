<?php
/**
 * Created by PhpStorm.
 * User: bkoether
 * Date: 2014-08-17
 * Time: 6:15 AM
 */

namespace Vitrix;

use Vitrix\Mailers\CronMailer;
use Job;

class DailyTasks
{
    protected $cronMailer;

    function __construct(CronMailer $cronMailer)
    {
        $this->cronMailer = $cronMailer;
    }

    public function overdueReminder()
    {
        $overdueJobs = Job::with('Employee')
                          ->where('scheduled', 1)
                          ->where('completed', 0)
                          ->where('scheduled_at', '<', date('Y-m-d'))
                          ->get();


        foreach ($overdueJobs as $job) {

        }


    }


}