<?php

function order_by_link($route, $text, $field)
{
    $sort  = Request::get('sort', 'asc') == 'asc' ? 'desc' : 'asc';
    $class = $sort == 'asc' ? 'fa-angle-down' : 'fa-angle-up';
    $class = Request::get('sortBy', '') == $field ? $class . ' fa' : '';

    return link_to_route($route, ' ' . $text, [
      'sortBy' => $field,
      'sort'   => $sort
    ], ['class' => $class]);
}

function set_active($path, $active = 'active')
{
    return Request::is($path) ? $active : '';
}

function select_lists($type, $emptyTxt = '- Please Select -')
{
    $selectVals = array();
    if (!empty($emptyTxt)) {
        $selectVals[''] = $emptyTxt;
    }

    switch ($type) {
        case 'customers':
            $selectVals += Customer::where('status', '=', '1')
                                   ->orderBy('name', 'ASC')
                                   ->lists('name', 'id');
            break;
        case 'employees':
            $selectVals['0'] = 'Not assigned yet';
            $selectVals += Employee::orderBy('company', 'ASC')
                                   ->get()
                                   ->lists('company', 'id');
            break;

        case 'frequency':
            $selectVals += array(
              '1' => 'Week',
              '2' => 'Month',
              '3' => 'Quarter',
              '4' => 'Year'
            );
            break;

        case 'invoice':
            $selectVals += array(
              'Cash'                     => 'Cash',
              'Invoice by Office'        => 'Invoice by Office',
              'Invoice by Subcontractor' => 'Invoice by Subcontractor'
            );
            break;

        case 'reports_customer':
            $selectVals += Customer::orderBy('name', 'ASC')
                                   ->lists('name', 'id');
            break;

        case 'reports_employees':
            $selectVals += Employee::orderBy('company', 'ASC')
                                   ->lists('company', 'id');
            break;

        case 'reports_chain':
            $chains = Config::get('vitrex.chains');
            $selectVals += array_combine($chains, $chains);
            break;

        case 'date_filter':
            $selectVals += array(
              'due_end'      => 'Due Latest',
              'completed_at' => 'Completed',
              'scheduled_at' => 'Scheduled'
            );
            break;

        case 'invoice_option':
            $selectVals += array(
              'monthly'      => 'Monthly',
              'semi_monthly' => 'Semi Monthly'
            );
            break;

    }

    return $selectVals;
}

function form_redirect($dest, $msg, $delete = false)
{
    if ($dest) {
        return Redirect::to($dest)
                       ->with(['msg' => $msg]);
    } else {
        return Redirect::back()
                       ->with(['msg' => $msg]);
    }
}


function combine_description($model, $type)
{
    $description = "";
    switch ($type) {
        case 'job':
            $description = $model->customer && !empty($model->customer->description) ? "<p><i>C</i>" . $model->customer->description . "</p>" : '';
            $description .= $model->service && !empty($model->service->description) ? "<p><i>S</i>" . $model->service->description . "</p>" : '';
            $description .= $model->description_override ? "<p><i>J</i>" . $model->description_override . "</p>" : '';

            break;

        case 'service':
            $description = $model->customer && !empty($model->customer->description) ? "<p><i>C</i>" . $model->customer->description . "</p>" : '';
            $description .= $model->description ? "<p><i>S</i>" . $model->description . "</p>" : '';

            break;

        case 'customer':
            $description .= $model->description ? "<p><i>C</i>" . $model->description . "</p>" : '';
            break;
    }

    return description_format($description);
}

function description_format($desc)
{
    $search  = array(
      '[sc]',
      '[/sc]',
      '[i]',
      '[/i]',
    );
    $replace = array(
      '<span class="sc-note">',
      '</span>',
      '<em>',
      '</em>'
    );

    $formatted = nl2br($desc);
    $formatted = str_replace($search, $replace, $formatted);

    return '<div class="desc-formatted">' . $formatted . '</div>';

}