<?php

namespace Vitrix;

use Commission;
use Job;
use Log;

class JobObserver
{

    public function updated(Job $job)
    {
        if ($job->completed && $job->service && $job->service->commission_percentage && $job->service->commission_employee_id) {
            if (!count($job->commission)) {
                $commission = new Commission([
                  'job_id'      => $job->id,
                  'employee_id' => $job->service->commission_employee_id,
                  'percentage'  => $job->service->commission_percentage,
                  'value'       => $job->customer_rate / 100 * $job->service->commission_percentage
                ]);
                $commission->save();
            }
        }
    }

}