<?php namespace Vitrix\Transformers;


class UserTransformer extends Transformer
{
    public function transform($user)
    {
        return [
          'id'     => $user['id'],
          'first'  => $user['first'],
          'last'   => $user['last'],
          'email'  => $user['email'],
          'role'   => $user['role'],
          'active' => (boolean)$user['active']
        ];
    }
}