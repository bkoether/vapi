<?php namespace Vitrix\Transformers;


class CustomerTransformer extends Transformer
{
    public function transform($service)
    {
        return [
          'id'           => $service['id'],
          'name'         => $service['name'],
          'chain'        => $service['chain'],
          'contact_name' => $service['contact_name'],
          'email'        => $service['email'],
          'active'       => (boolean)$service['active'],
          'phone1'       => $service['phone1'],
          'phone2'       => $service['phone2'],
          'address'      => $service['address'],
          'default_rate' => $service['default_rate'],
        ];
    }
}


