<?php namespace Vitrix\Transformers;


class EmployeeTransformer extends Transformer
{
    public function transform($service)
    {
        return [
          'id'           => $service['id'],
          'first'        => $service['user']['first'],
          'last'         => $service['user']['last'],
          'email'        => $service['user']['email'],
          'active'       => (boolean)$service['active'],
          'company'      => $service['company'],
          'phone1'       => $service['phone1'],
          'phone2'       => $service['phone2'],
          'address'      => $service['address'],
          'default_rate' => $service['default_rate'],
        ];
    }
}


