<?php
/**
 * Created by PhpStorm.
 * User: bkoether
 * Date: 2014-05-15
 * Time: 9:47 PM
 */

namespace Vitrix\Transformers;


class JobTransformer extends Transformer
{
    public function transform($job, $admin = false, $completed = false)
    {
        $link = $admin ? link_to_action('JobsController@show', 'View Details',
          $job->id) : link_to_action('UserEmployeeController@jobView',
          'View Details', $job->id);

        $start_date = $completed ? $job->completed_at->toDateString() : $job->scheduled_at->toDateString();
        $end_date   = $completed ? $job->completed_at->addDays($job->duration)
                                                     ->toDateString() : $job->scheduled_at->addDays($job->duration)
                                                                                          ->toDateString();
        return [
          'id'          => $job->id,
          'title'       => $job->service ? $job->service->site_name . ' (' . $job->service->type . ')' : $job->customer->name . ' (Custom)',
          'allDay'      => true,
          'start'       => $start_date,
          'end'         => $end_date,
          'description' => '<p>' . ($job->service ? $job->service->type : "Custom") . ' on ' .
                           $job->scheduled_at->toFormattedDateString() . '<br/>' . ($job->service ? $job->service->getFrequencyString() : '') . '</p><p>' .
                           $link . '</p>',
          'cancelled'   => $job->cancelled ?: 0,
          'priority'    => $job->priority_task ?: 0
        ];
    }
}