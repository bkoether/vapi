<?php namespace Vitrix;

use Job;
use Service;
use Carbon;
use Cache;

class JobScheduler
{

    protected $start;
    public $timeBoundaries;
    public $status;

    function __construct(Carbon $start)
    {
        $this->start = $start;

        $this->status = 'Error: No processing triggered.';

        if (Cache::has('time_boundaries')) {
            $this->timeBoundaries = Cache::get('time_boundaries');
        } else {
            $this->setTimeBoundaries(24);
            $expiresAt = Carbon::now()->addMonths(6);
            Cache::put('time_boundaries', $this->timeBoundaries, $expiresAt);
        }
    }

    public function scheduleAll()
    {
        $jobCounter   = 0;
        $services     = Service::whereNotNull('next_schedule')
                               ->where('status', 1)
                               ->where('next_schedule', '<=',
                                 $this->start->format('Y-m-01'))
                               ->where('next_schedule', '<>', '0000-00-00')
                               ->get();
        $this->status = 'Services found: ' . $services->count() . "\n";
        foreach ($services as $service) {
            // Only take 'schedule ahead' into account for certain frequencies.
            if (in_array($service->frequency, ['W', 'B', 'M', 'I'])) {
                for ($i = 0; $service->schedule_ahead > $i; $i++) {
                    $this_month = $this->start->copy()
                                              ->addMonths($i)
                                              ->format('Y-m');
                    $jobCounter += $this->createJobs($service, $this_month);
                }
            } else {
                $jobCounter += $this->createJobs($service,
                  $this->start->format('Y-m'));
            }
        }
        $this->status .= 'Jobs created: ' . $jobCounter;
    }

    public function scheduleService($serviceId)
    {
        $service    = Service::find($serviceId);
        $jobCounter = 0;

        // Only take 'schedule ahead' into account for certain frequencies.
        if (in_array($service->frequency, ['W', 'B', 'M', 'I'])) {
            for ($i = 0; $service->schedule_ahead > $i; $i++) {
                $this_month = $this->start->copy()
                                          ->addMonths($i)
                                          ->format('Y-m');
                $jobCounter += $this->createJobs($service, $this_month);
            }
        } else {
            $jobCounter += $this->createJobs($service,
              $this->start->format('Y-m'));
        }
        $this->status = 'Jobs created: ' . $jobCounter;
    }

    public function scheduleEmployee($employeeId)
    {

    }


    private function createJobs(Service $service, $month)
    {
        $date = Carbon::parse($month);

        $jobValues    = [
          'id'            => $service->id,
          'employee_id'   => $service->employee_id,
          'customer_id'   => $service->customer_id,
          'customer_rate' => $service->customer_rate ?: 0,
          'employee_rate' => $service->employee_rate ?: 0,
          'due_start'     => $this->timeBoundaries[$month]['first_day'],
          'due_end'       => $this->timeBoundaries[$month]['last_day'],
          'duration'      => $service->duration
        ];
        $nextSchedule = $date->copy()->addMonth()->format('Y-m-01');

        switch ($service->frequency) {
            case 'W':
                $repeats = $this->timeBoundaries[$month]['week_count'];
                break;
            case 'B':
                $repeats = $this->timeBoundaries[$month]['week_count'] == 5 ? 3 : 2;
                break;
            case 'M':
                $repeats = 1;
                break;
            case 'I':
                $repeats              = 1;
                $jobValues['due_end'] = $this->timeBoundaries[$date->addMonths(1)
                                                                   ->format('Y-m')]['last_day'];
                $nextSchedule         = $date->addMonth()->toDateString();
                break;
            case 'Q':
                $repeats              = 1;
                $jobValues['due_end'] = $this->timeBoundaries[$date->addMonths(2)
                                                                   ->format('Y-m')]['last_day'];
                $nextSchedule         = $date->addMonth()->toDateString();
                break;
            case 'S':
                $repeats              = 1;
                $jobValues['due_end'] = $this->timeBoundaries[$date->addMonths(5)
                                                                   ->format('Y-m')]['last_day'];
                $nextSchedule         = $date->addMonth()->toDateString();
                break;
            case 'T':
                $repeats              = 1;
                $jobValues['due_end'] = $this->timeBoundaries[$date->addMonths(3)
                                                                   ->format('Y-m')]['last_day'];
                $nextSchedule         = $date->addMonth()->toDateString();
                break;
            case 'Y':
                $repeats              = 1;
                $jobValues['due_end'] = $this->timeBoundaries[$date->addMonths(11)
                                                                   ->format('Y-m')]['last_day'];
                $nextSchedule         = $date->addMonth()->toDateString();
                break;
            default:
                $repeats = 0;
                break;
        }

        for ($i = 0; $i < $repeats; $i++) {
            $job = new Job();
            $job->preFill($jobValues);
            $job->save();
        }

        $service->next_schedule = $nextSchedule;
        $service->save();

        return $repeats;
    }


    private function setTimeBoundaries($length = 1)
    {
        for ($i = 0; $i < $length; $i++) {

            $month   = $this->start->copy()->startOfMonth()->addMonths($i);
            $monthId = $month->format('Y-m');
            $fm      = $month->firstOfMonth(1)->format('j');

            if ($fm > 5) {
                $this->timeBoundaries[$monthId]['first_day'] = $month->copy()
                                                                     ->subWeek()
                                                                     ->toDateString();
            } else {
                $this->timeBoundaries[$monthId]['first_day'] = $month->toDateString();
            }

            $lm = $month->addMonth()->firstOfMonth(1)->format('j');
            if ($lm > 5) {
                $this->timeBoundaries[$monthId]['last_day'] = $month->subWeek()
                                                                    ->subDay()
                                                                    ->toDateString();
            } else {
                $this->timeBoundaries[$monthId]['last_day'] = $month->subDay()
                                                                    ->toDateString();
            }

            $diff                                         = Carbon::parse($this->timeBoundaries[$monthId]['first_day'])
                                                                  ->diffInDays(Carbon::parse($this->timeBoundaries[$monthId]['last_day']));
            $this->timeBoundaries[$monthId]['week_count'] = $diff == 27 ? 4 : 5;
        }
    }


}