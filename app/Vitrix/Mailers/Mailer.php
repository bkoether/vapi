<?php
namespace Vitrix\Mailers;

use Mail;
use Thujohn\Pdf;
use View;


abstract class Mailer
{
    protected $defaultToEmail = 'info@vitrexltd.ca';

    public function sendTo(
      $user,
      $subject,
      $view,
      $data = [],
      $attach = false,
      $fileName = "invoice.pdf",
      $pdfView = ''
    ) {

        $email = is_object($user) ? $user->email : explode(',', $user);

        $pdfView = $pdfView ?: $view . '_pdf';

        Mail::queue($view, $data,
          function ($message) use (
            $email,
            $subject,
            $attach,
            $view,
            $data,
            $fileName,
            $pdfView
          ) {
              if ($attach) {
//                  $pdf_view = $view . '_pdf';
                  $pdf  = new Pdf\Pdf();
                  $file = $pdf->load(View::make($pdfView)->with($data),
                    'letter')
                              ->output();

                  $message->to($email);
                  $message->subject($subject);
                  $message->attachData($file, $fileName);
              } else {
                  $message->to($email)->subject($subject);
              }

          });
    }
}