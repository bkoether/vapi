<?php

namespace Vitrix\Mailers;

use Carbon\Carbon;
use Employee;
use Invoice;
use Job;
use Log;
use User;
use Vitrix\EmployeeFunctions;
use Barryvdh\Debugbar\Facade as Debugbar;

class EmployeeMailer extends Mailer
{

    public function sendInvoice(Invoice $invoice)
    {
        $calc           = new EmployeeFunctions();
        $employee       = Employee::with('user')->find($invoice->employee_id);
        $invoice_option = $employee->invoice_option ?: 'monthly';

        $month = Carbon::parse($invoice->month);
        $date  = [
          $month->firstOfMonth()->toDateString(),
          $month->lastOfMonth()->toDateString()
        ];
        if ($invoice_option == 'semi_monthly') {
            if ($invoice->month_part == 1) {
                $date[1] = $month->firstOfMonth()->addDays(14)->toDateString();
            } else {
                $date[0] = $month->firstOfMonth()->addDays(15)->toDateString();
            }
        }

        $data = $calc->calculateInvoice($employee, $date[0], $date[1],
          $invoice->month_part);


        $job_rows = [];
        foreach ($data['jobs'] as $job) {
            $job_rows[] = [
              'name'      => $job->service ? $job->service->site_name : $job->customer->name,
              'type'      => $job->service ? $job->service->type : 'Custom',
              'completed' => $job->completed_at->toDateString(),
              'rate'      => $job->employee_rate,
              'cash'      => $job->cashAmount()
            ];
        }


        $view    = 'emails.employees.invoice';
        $subject = $employee->company . ' - Invoice ' . $month->format('F Y');

        $email_content = [
          'title'       => 'Invoice - ' . Carbon::parse($date[0])
                                                ->format('F j') .
                           ' - ' . Carbon::parse($date[1])->format('j Y'),
          'jobs'        => $job_rows,
          'total'       => $data['total'] ? number_format($data['total'],
            2) : 0,
          'tax'         => $data['tax'] ? $data['tax'] : 0,
          'grand_total' => $data['grand_total'] ? $data['grand_total'] : 0,
          'cash'        => $data['cash'] ? $data['cash'] : 0,
          'company'     => $employee->company ?: $employee->user->fullName(),
          'name'        => $employee->company ? $employee->user->fullName() : '',
          'address'     => nl2br($employee->address),
          'tax_number'  => $employee->tax_number,
          'today'       => Carbon::now()->format('F j, Y'),
          'inv_number'  => $invoice->number,
          'commission'  => $data['commission']
        ];

        $toEmail = str_replace(' ', '', \Config::get('vitrex.emails.invoices')) . ',' . $employee->user->email;
        $this->sendTo($toEmail, $subject, $view, $email_content, true);


    }

    public function sendReport(User $user, $data = [])
    {
        $view    = 'emails.employees.report';
        $subject = 'Work report from ' . $user->fullName();


        $email_data = [];
        foreach ($data as $job) {
            $date = '';
            if (!$job['job']->cancelled) {
                if ($job['job']->completed) {
                    $date = $job['job']->completed_at->toDateString();
                } else {
                    $date = $job['job']->scheduled_at->toDateString();
                }
            }
            $email_data[] = [
              'url'     => route('admin.jobs.show', $job['job']->id),
              'title'   => isset($job['job']->service) ? $job['job']->service->site_name : $job['job']->customer->name,
              'type'    => isset($job['job']->service) ? $job['job']->service->type : 'Custom',
              'status'  => $job['status'],
              'date'    => $date,
              'comment' => $job['job']->employee_comment,
              'file'    => $job['file'] ? 'Yes' : 'No'
            ];
        }


        $email_content = [
          'title' => $subject,
          'jobs'  => $email_data
        ];

        $toEmail = str_replace(' ', '', \Config::get('vitrex.emails.reports'));
        $this->sendTo($toEmail, $subject, $view, $email_content);

    }

    public function sendSchedule(User $user)
    {
        $employeeId = $user->employee->id;

        $remainingJobs = Job::where('employee_id', $employeeId)
                            ->where('completed', 0)
                            ->where('scheduled', 0)
                            ->where('cancelled', 0)->count();

        if (!$remainingJobs) {
            $view    = 'emails.employees.schedule';
            $subject = 'Upcoming Schedule Completed: ' . $user->fullName();

            $email_content = [
              'title' => $subject,
              'name'  => $user->fullName(),
              'url'   => action('JobsController@calendar', $user->employee->id),
            ];

            $toEmail = str_replace(' ', '', \Config::get('vitrex.emails.reports'));
            $this->sendTo($toEmail, $subject, $view,
              $email_content);
        } else {
        }


    }

    public function sendReportReminder(User $user, $jobs)
    {
        $view    = 'emails.employees.reportReminder';
        $subject = 'Outstanding Job Reports';

        $email_content = [
          'title' => $subject,
          'url'   => url('/'),
          'count' => count($jobs),
          'name'  => $user->first
        ];

        $this->sendTo($user->email, $subject, $view, $email_content);

    }

    public function sendScheduleReminder(User $user, $jobs)
    {
        $view    = 'emails.employees.scheduleReminder';
        $subject = 'Incomplete Schedule';

        $job_data = [];
        foreach ($jobs as $job) {
            $job->service;
            if (isset($job->service)) {
                $job_data[] = $job->service->site_name . ' (' . $job->service->type . ')';
            } else {
                $job_data[] = $job->customer->name . ' (Custom)';
            }
        }

        $email_content = [
          'title' => $subject,
          'url'   => action('UserEmployeeController@schedule'),
          'jobs'  => $job_data,
          'count' => count($jobs),
          'name'  => $user->first
        ];

        $this->sendTo($user->email, $subject, $view, $email_content);

    }

    public function sendCashReceipt(Job $job, $emails)
    {

        $view      = 'emails.employees.receipt';
        $receiptId = $job->scheduled_at->toDateString() . '-' . $job->id;
        $subject   = 'Vitrex Cash Receipt for ' . $job->customer->name . '  -  #' . $receiptId;

        $email_content = [
          'subj'         => $subject,
          'id'           => $receiptId,
          'title'        => 'Cash Receipt',
          'jobType'      => $job->service ? $job->service->getTypeName() : 'Custom Work',
          'date'         => $job->scheduled_at->format('F jS, Y'),
          'total'        => number_format($job->customer_rate, 2),
          'tax'          => number_format($job->customer_rate * 0.05, 2),
          'grand_total'  => number_format($job->customer_rate * 1.05, 2),
          'to_name'      => $job->customer->contact_name,
          'to_company'   => $job->customer->name,
          'to_address'   => nl2br($job->customer->billing_address),
          'site_name'    => $job->service ? $job->service->site_name : '',
          'site_address' => $job->service ? nl2br($job->service->address) : '',
        ];



        $defaultEmails = explode(',', \Config::get('vitrex.emails.invoices'));
        $otherEmails = array_merge($defaultEmails, $emails);
        $toEmail  = str_replace(' ', '', implode(',', $otherEmails));
        $filename = $receiptId . '__' . $job->customer->name . '.pdf';
        $this->sendTo($toEmail, $subject, $view, $email_content, true,
          $filename);

    }

    public function sendPrintCopy($data) {
      $view      = 'emails.employees.receipt_copy';

      $subject   = 'Copy of printed Cash Receipt for ' . $data['to_company'] . '  -  #' . $data['id'];

      $data['subj'] = $subject;
      $data['employee'] = \Auth::user()->employee->fullName();
      $data['printDate'] = Carbon::now()->toDateTimeString();

      $toEmail  = str_replace(' ', '', \Config::get('vitrex.emails.invoices'));
      $filename = $data['id'] . '__' . $data['to_company'] . '.pdf';

      $this->sendTo($toEmail, $subject, $view, $data, true, $filename, 'emails.employees.receipt_pdf');
    }

    public function sendCustomerInvoice(Job $job)
    {
        $view      = 'emails.employees.customer_invoice';
        $receiptId = Carbon::now()->toDateString() . '-' . $job->id;
        $subject   = 'Vitrex Invoice for ' . $job->customer->name . '  -  #' . $receiptId;

        $email_content = [
          'subj'         => $subject,
          'id'           => $receiptId,
          'title'        => 'Invoice',
          'jobType'      => $job->service ? $job->service->getTypeName() : 'Custom Work',
          'date'         => Carbon::now()->format('F jS, Y'),
          'total'        => number_format($job->customer_rate, 2),
          'tax'          => number_format($job->customer_rate * 0.05, 2),
          'grand_total'  => number_format($job->customer_rate * 1.05, 2),
          'to_name'      => $job->customer->contact_name,
          'to_company'   => $job->customer->name,
          'to_address'   => nl2br($job->customer->billing_address),
          'site_name'    => $job->service ? $job->service->site_name : '',
          'site_address' => $job->service ? nl2br($job->service->address) : '',

        ];
        if (!empty($job->document)) {
            $email_content['signed_by'] = $job->document->signature_name;
            $email_content['signed_on'] = $job->document->created_at->toDayDateTimeString();
            $email_content['file']      = substr($job->document->file->url(), 1);
        }

        $customerEmail = $job->customer->email ?: '';


        $toEmail  = str_replace(' ', '', \Config::get('vitrex.emails.invoices')) . ',' . $customerEmail;
        $filename = $receiptId . '__' . $job->customer->name . '.pdf';
        $this->sendTo($toEmail, $subject, $view, $email_content, true,
          $filename);
    }

}