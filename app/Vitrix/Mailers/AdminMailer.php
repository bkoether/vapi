<?php
/**
 * Created by PhpStorm.
 * User: bkoether
 * Date: 2014-08-17
 * Time: 5:50 AM
 */

namespace Vitrix\Mailers;

use Carbon\Carbon;


class AdminMailer extends Mailer
{
    public function sendStarbucksReport($email, $report)
    {

        $date = Carbon::parse($report->call_date);

        $view    = 'emails.admin.starbucks';
        $subject = 'Vitrex Work Report ' . $date->format('l, F jS Y');

        $email_data = [];
        foreach ($report->report_data as $job) {
            $email_data[] = [
              'store_number' => !empty($job['store_number']) ? '#' . $job['store_number'] . " " : '',
              'store'        => $job['location_name'] . ' (' . $job['service_type'] . ')',
              'worksheet'    => $job['signed_worksheet'],
              'contact'      => $job['signed_worksheet'] ? '' : $job['call_time'] . ' - ' . $job['contact_name'] . ' (' . $job['contact_position'] . ')',
              'comment'      => $job['signed_worksheet'] ? '' : $job['comment'],
              'completed'    => isset($job['completed']) ? 'Completed: ' . $job['completed'] : '',
            ];
        }

        $email_content = [
          'title' => $subject,
          'jobs'  => $email_data
        ];

        $toEmail  = str_replace(' ', '', $email);
        $this->sendTo($toEmail, $subject, $view, $email_content);


    }


    public function sendEstimate($email, $estimate)
    {
        $service       = new \Service();
        $service->type = $estimate->data['type'];
        $description   = 'Type of service: ' . $service->getTypeName() . ' | ';
        $description .= 'Frequency: ' . $service->frequencyOptions[$estimate->data['frequency']] . "<br>";
        $description .= nl2br($estimate->data['description']);

        $view          = 'emails.admin.estimate';
        $email_content = [
          'text'            => nl2br($email['email_text']),
          'date'            => $estimate->estimate_date->toDateString(),
          'id'              => $estimate->estimate_number,
          'expiration_date' => $estimate->expiration_date->toDateString(),
          'to_name'         => $estimate->customer->contact_name,
          'to_company'      => $estimate->customer->name,
          'to_address'      => nl2br($estimate->customer->billing_address),
          'header_text'     => nl2br($estimate->header_note),
          'job_description' => $description,
          'total'           => number_format($estimate->data['customer_rate'],
            2),
          'tax'             => number_format(($estimate->data['customer_rate'] * 0.05),
            2),
          'grand_total'     => number_format(($estimate->data['customer_rate'] * 1.05),
            2),
          'footer_text'     => nl2br($estimate->footer_note),
          'valid_days'      => $estimate->estimate_date->diffInDays($estimate->expiration_date)
        ];


        $toEmail  = str_replace(' ', '', $email['email_to']);
        $this->sendTo($toEmail, $email['email_subject'], $view,
          $email_content, true, "quote.pdf");


    }

}