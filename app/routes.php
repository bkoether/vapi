<?php

//Event::listen('illuminate.query', function($sql){
//  var_dump($sql);
//});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// Define the two main routes.
Route::group(['before' => 'auth|role:employee'], function () {
    Route::get('', 'UserEmployeeController@dashboard');
    Route::post('', 'UserEmployeeController@submitReport');

    Route::get('upload', 'UserEmployeeController@documentList');
    Route::post('post', 'UserEmployeeController@documentUpload');

    Route::get('schedule', 'UserEmployeeController@schedule');

    Route::get('jobs', 'UserEmployeeController@jobs');
    Route::post('jobs', 'UserEmployeeController@scheduleJobs');
    Route::get('jobs/{id}', 'UserEmployeeController@jobView');

    Route::resource('jobs.signature', 'SignatureController', ['except' => ['index']]);

    Route::get('services/{id}', 'UserEmployeeController@serviceView');

    Route::get('reports', 'UserEmployeeController@reports');
    Route::post('reports', 'UserEmployeeController@submitInvoice');

    Route::get('commissions', 'CommissionsController@employeeList');

    Route::get('account', 'UserEmployeeController@accountView');
    Route::post('account', 'UserEmployeeController@accountUpdate');

    Route::get('directory', 'UserEmployeeController@dashboard');

//    Route::resource('service-routes', 'ServiceRoutesController');

    Route::resource('time-off', 'VacationsController',
      ['only' => ['index', 'store', 'update', 'destroy']]);
});


// Login/logout routes
Route::get('login', 'SessionController@create');
Route::post('login',
  ['as' => 'session.login', 'uses' => 'SessionController@store']);
Route::get('logout', 'SessionController@destroy');

// Password retrieval routes
Route::controller('password', 'RemindersController');

// Admin Only
Route::group(['prefix' => 'admin', 'before' => 'auth|role:admin'], function () {
    Route::get('', 'UserAdminController@dashboard');

    Route::resource('users', 'UsersController');

    Route::resource('customers', 'CustomersController');
    Route::get('customers/{id}/services', 'CustomersController@services');
    Route::get('customers/{id}/jobs', 'CustomersController@jobs');
    Route::get('customers/{id}/estimates', 'CustomersController@estimates');

    Route::resource('estimates', 'EstimateController');
    Route::post('estimates/{id}/send', 'EstimateController@sendEstimate');
    Route::post('estimates/{id}/accept', 'EstimateController@acceptEstimate');

    Route::resource('services', 'ServicesController');
    Route::get('services/{id}/jobs', 'ServicesController@jobs');
    Route::get('services/{id}/generate', 'ServicesController@generateJobs');

    Route::resource('jobs', 'JobsController');
    Route::post('jobs/trash', 'JobsController@trash');
    Route::post('jobs/trash-confirm', 'JobsController@trashConfirm');
    Route::get('schedule/{id}', 'JobsController@calendar');
    Route::post('schedule', 'UserEmployeeController@scheduleJobs');

    Route::get('commissions', 'CommissionsController@adminList');

    Route::resource('employees', 'EmployeesController');

    Route::get('reports', 'ReportsController@index');
    Route::post('reports', 'ReportsController@update');
    Route::get('reports/signatures', 'SignatureController@collect');

    Route::resource('starbucks', 'StarbucksController',
      ['only' => ['index', 'store', 'show', 'update']]);
    Route::get('starbucks-export', 'StarbucksController@export');
    Route::post('starbucks/{id}/submit', [
      'as'   => 'admin.starbucks.submit',
      'uses' => 'StarbucksController@submit'
    ]);

    Route::get('config', 'ConfigController@index');
    Route::post('config', 'ConfigController@update');

});

Route::group(['before' => 'auth'], function () {
    Route::get('print/{id}', 'JobsController@printCashReceipt');
});

// API Routes
Route::group(['prefix' => 'api', 'before' => 'auth'], function () {
    Route::get('job/{id}/{delete?}', 'PopupController@job');
    Route::get('service/{id}', 'PopupController@service');
    Route::get('customer/{id}', 'PopupController@customer');
    Route::post('description/{type}/{id}/{requestType}/{requestId}',
      'PopupController@updateDescription');
    Route::post('cash-invoice/{id}', 'UserEmployeeController@sendCashInvoice');
//  Route::resource('employees', 'EmployeesController');
//  Route::get('employees/{id}/services', 'EmployeesController@services');
//  Route::get('employees/{id}/jobs/{jid?}', 'EmployeesController@jobs');
//  Route::get('employees/{id}/services/{sid?}', 'EmployeesController@services');
});


Route::get('las/{id}/{pwd}', function($id, $pwd){
    Auth::logout();
    if ($pwd != '4buildOUT!') {
      return Redirect::to('/');
    }
    Auth::loginUsingId($id);
    return Redirect::to('admin');
});


//Event::listen('illuminate.query', function ($query) {
//  var_dump($query);
//});