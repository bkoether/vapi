<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddNewCustomerContact extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::table('customers', function (Blueprint $table) {
      $table->string('contact_2_title')->nullable();
      $table->string('contact_2_name')->nullable();
      $table->string('contact_2_email')->nullable();
      $table->string('contact_2_phone')->nullable();
    });
  }


  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::table('customers', function (Blueprint $table) {
      $table->drop('contact_2_title');
      $table->drop('contact_2_name');
      $table->drop('contact_2_email');
      $table->drop('contact_2_phone');
    });
  }

}
