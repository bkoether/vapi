<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAcceptedFieldsToEstimatesTabel extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('estimates', function(Blueprint $table)
		{
			$table->boolean('accepted')->default(0);
			$table->date('accepted_date')->nullable();
			$table->date('last_sent_date')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('estimates', function(Blueprint $table)
		{
			$table->drop('accepted');
			$table->drop('accepted_date');
			$table->drop('last_sent_date');
		});
	}

}
