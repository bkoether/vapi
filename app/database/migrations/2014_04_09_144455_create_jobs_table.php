<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('jobs', function(Blueprint $table)
		{
			$table->increments('id');
      $table->integer('service_id')->index();
      $table->integer('employee_id')->index();
      $table->integer('customer_id')->index();
      $table->mediumText('admin_comment')->nullable();
      $table->mediumText('employee_comment')->nullable();
      $table->boolean('scheduled')->default(FALSE);
      $table->boolean('completed')->default(FALSE);
      $table->boolean('invoiced')->default(FALSE);
      $table->date('due_start');
      $table->date('due_end');
      $table->date('completed_at')->nullable();
      $table->date('scheduled_at')->nullable();
      $table->date('invoiced_at')->nullable();
      $table->decimal('customer_rate');
      $table->decimal('employee_rate');
      $table->mediumText('description_override')->nullable();
      $table->integer('report_id')->default(0);
      $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('jobs');
	}

}
