<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('customers', function(Blueprint $table)
		{
			$table->increments('id');
      $table->string('name');
      $table->string('contact_name');
      $table->string('chain')->nullable();
      $table->string('phone1');
      $table->string('phone2')->nullable();
      $table->string('email');
      $table->string('billing_address');
      $table->decimal('default_rate')->nullable();
      $table->boolean('status');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('customers');
	}

}
