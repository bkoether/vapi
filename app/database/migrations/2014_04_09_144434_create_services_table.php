<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('services', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('customer_id')->index();
      $table->integer('employee_id')->index();
      $table->integer('store_number')->nullable();
      $table->string('site_name');
      $table->string('address');
      $table->string('type');
      $table->longText('description')->nullable();
      $table->decimal('customer_rate');
      $table->decimal('employee_rate')->nullable();
      $table->string('invoice_type');
      $table->boolean('workorder_required');
      $table->boolean('signature_required');
      $table->string('frequency');
      $table->smallInteger('frequency_factor');
      $table->integer('schedule_ahead');
      $table->date('next_schedule')->nullable();
      $table->boolean('status');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::drop('services');
  }

}
