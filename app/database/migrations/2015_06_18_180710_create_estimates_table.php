<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEstimatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('estimates', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('customer_id');
            $table->string('estimate_number');
            $table->date('estimate_date');
            $table->date('expiration_date');
            $table->text('header_note')->nullable();
            $table->mediumText('data');
            $table->text('footer_note')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('estimates');
	}

}
