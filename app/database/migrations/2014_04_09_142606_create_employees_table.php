<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('employees', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('user_id')->index();
      $table->date('birthday')->nullable();
      $table->date('start_date')->nullable();
      $table->date('end_date')->nullable();
      $table->text('sin_number');
      $table->text('tax_number');
      $table->text('wbc_number');
      $table->string('company');
      $table->string('phone1');
      $table->string('phone2')->nullable();
      $table->string('address');
      $table->decimal('default_rate');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::drop('employees');
  }

}
