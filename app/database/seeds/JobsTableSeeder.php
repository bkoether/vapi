<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class JobsTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 10) as $index)
		{
      // Past
      $em = $faker->randomNumber(2, 11);
      $cu = $faker->randomNumber(1, 20);
      $se = $faker->randomNumber(1, 25);
      $in = $faker->boolean();
      $day = $faker->dateTimeBetween('-100 days', '-10 days')->format('Y-m-d');
			Job::create([
        'service_id' => $se,
        'employee_id' => $em,
        'customer_id' => $cu,
        'admin_comment' => $faker->optional()->sentence(),
        'employee_comment' => $faker->optional()->sentence(),
        'scheduled' => TRUE,
        'completed' => TRUE,
        'invoiced' => $in,
        'due_start' => $day,
        'due_end' => $day,
        'completed_at' => $day,
        'scheduled_at' => $day,
        'invoiced_at' => $in ? $faker->dateTimeBetween('-30 days', '-5 days')->format('Y-m-d') : NULL,
        'customer_rate' => $faker->randomFloat(2, 55, 65),
        'employee_rate' => $faker->randomFloat(2, 35, 45),
			]);

      // Scheduled
      $em = $faker->randomNumber(2, 11);
      $cu = $faker->randomNumber(1, 20);
      $se = $faker->randomNumber(1, 25);
      Job::create([
        'service_id' => $se,
        'employee_id' => $em,
        'customer_id' => $cu,
        'scheduled' => TRUE,
        'completed' => FALSE,
        'invoiced' => FALSE,
        'due_start' => $faker->dateTime('first day of this month')->format('Y-m-d'),
        'due_end' => $faker->dateTime('last day of this month')->format('Y-m-d'),
        'scheduled_at' => $faker->dateTimeBetween('today', '+14 days')->format('Y-m-d'),
        'customer_rate' => $faker->randomFloat(2, 55, 65),
        'employee_rate' => $faker->randomFloat(2, 35, 45),
      ]);

      Job::create([
        'service_id' => 1,
        'employee_id' => 1,
        'customer_id' => 1,
        'scheduled' => TRUE,
        'completed' => FALSE,
        'invoiced' => FALSE,
        'due_start' => $faker->dateTimeBetween('-30 days', 'today')->format('Y-m-d'),
        'due_end' => $faker->dateTimeBetween('today', '+30 days')->format('Y-m-d'),
        'scheduled_at' => $faker->dateTimeBetween('today', '+14 days')->format('Y-m-d'),
        'customer_rate' => $faker->randomFloat(2, 55, 65),
        'employee_rate' => $faker->randomFloat(2, 35, 45),
      ]);

      // Unscheduled
      $em = $faker->randomNumber(2, 11);
      $cu = $faker->randomNumber(1, 20);
      $se = $faker->randomNumber(1, 25);
      $start = $faker->dateTimeBetween('+20 days', '+90 days')->getTimestamp();
      $end = $start + $faker->randomNumber(604800, 7776000);
      Job::create([
        'service_id' => $se,
        'employee_id' => $em,
        'customer_id' => $cu,
        'scheduled' => FALSE,
        'completed' => FALSE,
        'invoiced' => FALSE,
        'due_start' => date('Y-m-d', $start),
        'due_end' => date('Y-m-d', $end),
        'customer_rate' => $faker->randomFloat(2, 55, 65),
        'employee_rate' => $faker->randomFloat(2, 35, 45),
      ]);
    }

    foreach(range(1, 5) as $index)
    {
      Job::create([
        'service_id' => 1,
        'employee_id' => 1,
        'customer_id' => 1,
        'scheduled' => FALSE,
        'completed' => FALSE,
        'invoiced' => FALSE,
        'due_start' => Carbon::now()->addMonths(2)->firstOfMonth()->toDateString(),
        'due_end' => Carbon::now()->addMonths(2)->lastOfMonth()->toDateString(),
        'customer_rate' => $faker->randomFloat(2, 55, 65),
        'employee_rate' => $faker->randomFloat(2, 35, 45),
      ]);
      $sDate = Carbon::now()->subDays($faker->randomNumber(5,30))->toDateString();

      Job::create([
        'service_id' => 1,
        'employee_id' => 1,
        'customer_id' => 1,
        'scheduled' => TRUE,
        'completed' => TRUE,
        'invoiced' => FALSE,
        'scheduled_at' => $sDate,
        'completed_at' => $sDate,
        'employee_comment' => $faker->optional(.3)->text(300),
        'admin_comment' => $faker->optional(.3)->text(300),
        'due_start' => Carbon::now()->subMonths(2)->toDateString(),
        'due_end' => Carbon::now()->subMonths(1)->toDateString(),
        'customer_rate' => $faker->randomFloat(2, 55, 65),
        'employee_rate' => $faker->randomFloat(2, 35, 45),
      ]);
    }
	}

}