<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

    Job::truncate();
    Service::truncate();
    Customer::truncate();
    Employee::truncate();
    User::truncate();


    $this->call('UsersTableSeeder');
    $this->call('EmployeesTableSeeder');
    $this->call('CustomersTableSeeder');
    $this->call('ServicesTableSeeder');
    $this->call('JobsTableSeeder');
	}

}