<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class ServicesTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 25) as $index)
		{
			Service::create([
        'customer_id' => $faker->randomNumber(1,20),
        'employee_id' => $faker->randomNumber(2,11),
        'store_number' => $faker->optional()->randomNumber(10, 999),
        'site_name' => $faker->streetName,
        'address' => $faker->address,
        'type' => $faker->randomElement(array('W', 'PW', 'QIC', 'HD', 'CHAIR')),
        'description' => $faker->paragraph(),
        'customer_rate' => $faker->randomFloat(2, 55, 65),
        'employee_rate' => $faker->randomFloat(2, 35, 45),
        'invoice_type' => $faker->randomElement(array('C', 'O', 'S')),
        'workorder_required' => $faker->boolean(),
        'signature_required' => $faker->boolean(),
        'frequency' => $faker->randomElement(array('W', 'M', 'Q', 'Y', 'R', 'U')),
        'frequency_factor' => $faker->randomNumber(1,5),
        'schedule_ahead' => $faker->randomNumber(1,6),
        'status' => TRUE,
			]);
		}
    $service = Service::find(1);
    $service->customer_id = 1;
    $service->employee_id = 1;
    $service->save();
	}

}