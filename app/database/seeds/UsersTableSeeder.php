<?php
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder {

  public function run()
  {
    $faker = Faker::create();

    User::create([
      'email' => 'ben@koether.ca',
      'first' => 'Ben',
      'last' => 'Koether',
      'password' => 'admin',
      'role' => 'admin',
      'status' => TRUE
    ]);
    foreach (range(2, 11) as $index)
    {
      User::create([
        'email' => $faker->email(),
        'first' => $faker->firstName,
        'last' => $faker->lastName,
        'password' => 'test',
        'role' => 'employee',
        'status' => TRUE
      ]);
    }
    $testEmployee = User::find(2);
    $testEmployee->email = 'test@test.com';
    $testEmployee->save();

    User::create([
      'email' => 'rexshinasl@gmail.com',
      'first' => 'Rex',
      'last' => 'Shin',
      'password' => 'admin',
      'role' => 'admin',
      'status' => TRUE
    ]);
  }
}