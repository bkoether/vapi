<?php

use Faker\Factory as Faker;

class EmployeesTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(2, 11) as $index)
		{
			Employee::create([
        'user_id' => $index,
        'company' => $faker->company,
        'birthday' => $faker->dateTimeBetween('-50 years', '-18 years')->format('Y-m-d'),
        'start_date' => $faker->dateTimeBetween('-7 years', '-1 years')->format('Y-m-d'),
        'sin_number' => $faker->numerify('###-####-###-##'),
        'tax_number' => $faker->numerify('###-####-###-##'),
        'wbc_number' => $faker->numerify('###-####-###-##'),
        'phone1' => $faker->phoneNumber,
        'phone2' => $faker->phoneNumber,
        'address' => $faker->address,
        'default_rate' => $faker->randomFloat(2, 35, 45),
			]);
		}
	}

}