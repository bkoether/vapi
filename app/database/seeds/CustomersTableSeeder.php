<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class CustomersTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

    $chain = array('Starbucks', '');
		foreach(range(1, 20) as $index)
		{
			Customer::create([
        'name' => $faker->company,
        'contact_name' => $faker->name,
        'chain' => $chain[array_rand($chain)],
        'phone1' => $faker->phoneNumber,
        'phone2' => $faker->phoneNumber,
        'email' => $faker->companyEmail,
        'billing_address' => $faker->address,
        'default_rate' => $faker->randomFloat(2, 55, 65),
        'status' => TRUE
			]);
		}
	}

}