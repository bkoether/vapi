<?php

class Employee extends \Eloquent
{
    protected $fillable = [
      'user_id',
      'company',
      'phone1',
      'phone2',
      'address',
      'birthday',
      'sin_number',
      'tax_number',
      'wbc_number',
      'invoice_option',
      'default_rate',
      'start_date',
      'end_date',
      'status',
    ];

    public function getDates()
    {
        return [
          'start_date',
          'end_date',
          'birthday',
          'created_at',
          'updated_at'
        ];
    }


    public function services()
    {
        return $this->hasMany('Service');
    }

    public function jobs()
    {
        return $this->hasMany('Job');
    }

    public function commissions()
    {
        return $this->hasMany('Commission');
    }

    public function user()
    {
        return $this->belongsTo('User');
    }

    public function invoices()
    {
        return $this->hasMany('Invoice');
    }

    public function routes()
    {
        return $this->hasMany('ServiceRoute');
    }

    public function vacations()
    {
        return $this->hasMany('Vacation');
    }

    public function fullName()
    {
        return $this->user->fullName();
    }

}