<?php
use Codesleeve\Stapler\ORM\EloquentTrait;
use Codesleeve\Stapler\ORM\StaplerableInterface;

class LocationImage extends \Eloquent implements StaplerableInterface
{
    use EloquentTrait;

    public function __construct(array $attributes = array())
    {
        $this->hasAttachedFile('photo', [
          'styles' => [
            'preview'   => '65x65#',
            'thumbnail' => '200x200#',
            'large'     => '800x800'
          ],
          'url'    => '/:attachment/:id_partition/:style/:filename',
          'path'   => ':app_root/www:url'
        ]);

        parent::__construct($attributes);
    }

// A profile picture belongs to a user.
    public function service()
    {
        return $this->belongsTo('Service');
    }

}