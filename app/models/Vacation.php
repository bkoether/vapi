<?php

class Vacation extends \Eloquent
{
    protected $fillable = [
      'employee_id',
      'start_date',
      'end_date'
    ];

    public function getDates()
    {
        return [
          'start_date',
          'end_date',
          'created_at',
          'updated_at'
        ];
    }

    public function employee()
    {
        return $this->belongsTo('Employee');
    }

}