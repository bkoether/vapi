<?php

class Report extends \Eloquent
{
    protected $fillable = ['call_date', 'report_data', 'submitted'];

    public function getDates()
    {
        return [
          'call_date',
          'created_at',
          'updated_at'
        ];
    }

    public function getReportDataAttribute($value)
    {
        return unserialize($value);
    }

    public function setReportDataAttribute($value)
    {
        $this->attributes['report_data'] = serialize($value);
    }

    public function job()
    {
        return $this->hasMany('Job');
    }
}