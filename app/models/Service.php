<?php

class Service extends \Eloquent
{
    protected $fillable = [
      'customer_id',
      'employee_id',
      'site_name',
      'address',
      'store_number',
      'type',
      'description',
      'customer_rate',
      'employee_rate',
      'invoice_type',
      'workorder_required',
      'signature_required',
      'frequency',
      'frequency_factor',
      'schedule_ahead',
      'next_schedule',
      'status',
      'duration',
      'commission_employee_id',
      'commission_percentage'
    ];

    public function getDates()
    {
        return [
          'next_schedule',
          'created_at',
          'updated_at'
        ];
    }

    public static function boot()
    {
        parent::boot();

        static::deleted(function ($service) {
            $service->jobs()->delete();
        });
    }


    public function jobs()
    {
        return $this->hasMany('Job');
    }

    public function employee()
    {
        return $this->belongsTo('Employee');
    }

    public function customer()
    {
        return $this->belongsTo('Customer');
    }

    public function locationImages()
    {
        return $this->hasMany('LocationImage');
    }

    public function serviceRoute()
    {
        return $this->belongsTo('ServiceRoute');
    }


    public function scopeThisUser($query)
    {
        return $query->where('employee_id', Auth::user()->employee->id);
    }


    public $typeOptions = [
      'PW'    => 'Pressure Washing',
      'W'     => 'Window Cleaning',
      'QIC'   => 'Interior Cleaning',
      'HD'    => 'High Dusting',
      'CHAIR' => 'Upholstery Cleaning',
      'SC'    => 'Sign Cleaning',
      'CA'    => 'Canvass Awning Cleaning',
      'GA'    => 'Glass Awning Cleaning',
      'WR'    => 'Washroom Service',
      'G'     => 'Gutter Cleaning',
      'J'     => 'Janitorial'
    ];

    public function getTypeName()
    {
        $options = Config::get('vitrex.service_types');
        if (isset($options[$this->type])) {
            return $this->type ? $options[$this->type] : 'Custom';
        } else {
            return 'INVALID SERVICE TYPE';
        }
    }

    public function getTypeCode()
    {
        $options = Config::get('vitrex.service_types');
        if (isset($options[$this->type])) {
            return $this->type;
        } else {
            return 'XX-' . $this->type;
        }

    }


    public $invoiceOptions = [
      'O' => 'Invoice by Office',
      'C' => 'Cash',
      'S' => 'Invoice by Subcontractor',
      'A' => 'Automatic Invoice'
    ];

    public function getInvoiceOption()
    {
        return $this->invoiceOptions[$this->invoice_type];
    }


    public $frequencyOptions = [
      'W' => 'Weekly',
      'B' => 'Bi-Weekly',
      'M' => 'Monthly',
      'I' => 'Bi-Monthly',
      'Q' => 'Quarterly',
      'T' => 'Tri-Annually',
      'S' => 'Semi-Annually',
      'Y' => 'Annually',
      'R' => 'Random',
      'U' => 'Upon Request'
    ];

    public function getFrequencyString()
    {
        return $this->frequencyOptions[$this->frequency];
    }

    public function lastCompleted($count = 1)
    {
        $jobs = Job::orderBy('completed_at', 'DESC')
                   ->where('completed', 1)
                   ->where('cancelled', 0)
                   ->where('service_id', $this->id)
                   ->take($count)
                   ->get(['completed_at']);

        if ($count > 1) {
            return $jobs;
        } else {
            return $jobs->count() ? $jobs->first()->completed_at : false;
        }
    }

}