<?php

use Vitrix\JobObserver;

class Job extends \Eloquent
{
    protected $fillable = [
      'service_id',
      'employee_id',
      'customer_id',
      'admin_comment',
      'employee_comment',
      'scheduled',
      'completed',
      'invoiced',
      'due_start',
      'due_end',
      'completed_at',
      'scheduled_at',
      'invoiced_at',
      'customer_rate',
      'employee_rate',
      'description_override',
      'report_id',
      'cancelled',
      'priority_task',
      'duration',
      'filed_at'
    ];

    public static function boot()
    {
        parent::boot();

        Job::observe(new JobObserver());
    }

    public function getDates()
    {
        return [
          'due_start',
          'due_end',
          'completed_at',
          'scheduled_at',
          'invoiced_at',
          'created_at',
          'updated_at',
          'filed_at'
        ];
    }

    public function service()
    {
        return $this->belongsTo('Service');
    }

    public function customer()
    {
        return $this->belongsTo('Customer');
    }

    public function employee()
    {
        return $this->belongsTo('Employee');
    }

    public function report()
    {
        return $this->belongsTo('Report');
    }

    public function document()
    {
        return $this->hasOne('Document');
    }

    public function commission()
    {
        return $this->hasOne('Commission');
    }


    public function scopeUpcoming($query)
    {
        return $query->where('completed', 0)->where('scheduled', 1);
    }

    public function scopeThisUser($query)
    {
        return $query->where('employee_id', Auth::user()->employee->id);
    }


    public function netValue()
    {
        return number_format($this->customer_rate - $this->employee_rate, 2);
    }

    public function invoiceAmount()
    {
        if ($this->service && $this->service->invoice_type == 'C') {
            return number_format(($this->employee_rate - $this->customer_rate),
              '2');
        } else {
            return number_format($this->employee_rate, 2);
        }
    }

    public function cashAmount()
    {
        if ($this->service && $this->service->invoice_type == 'C') {
            return number_format($this->customer_rate, '2');
        } else {
            return 0;
        }
    }

    public function status($type = 'completed')
    {
        $status = '';

        switch ($type) {
            case 'scheduled':
                if ($this->scheduled) {
                    $status = $this->scheduled_at->toDateString();
                } else {
                    $status = '<span class="label label-warning">Not Scheduled</span>';
                }
                break;

            case 'completed':
            default:
                if ($this->completed) {
                    if ($this->isCancelled()) {
                        $status = '<span class="label label-warning">Cancelled</span>';
                    } else {
                        $status = $this->completed_at->toDateString();
                    }
                } else {
                    if ($this->scheduled) {
                        if ($this->scheduled_at->isPast() && !$this->scheduled_at->isSameDay(Carbon::now())) {
                            $status = '<span class="label label-danger">Overdue</span>';
                        }
                    } else {
                        if (!$this->due_end->isFuture() && $this->due_end->isSameDay(Carbon::now())) {
                            $status = '<span class="label label-danger">Overdue</span>';
                        }
                    }

                }
                break;
        }

        return $status;
    }


    public function isCancelled()
    {
        return $this->cancelled;
    }

    public function preFill($values, $type = 'auto')
    {
        if ($type == 'service' || $type == 'auto') {
            $this->service_id    = $values['id'];
            $this->employee_id   = $values['employee_id'];
            $this->customer_id   = $values['customer_id'];
            $this->customer_rate = $values['customer_rate'];
            $this->employee_rate = $values['employee_rate'];
            $this->duration      = $values['duration'];
        }

        if ($type == 'auto') {
            $this->due_start = $values['due_start'];
            $this->due_end   = $values['due_end'];
        }

        if ($type == 'customer') {
            $this->service_id    = 0;
            $this->employee_id   = 0;
            $this->customer_id   = $values['id'];
            $this->customer_rate = $values['default_rate'];
        }
    }

}