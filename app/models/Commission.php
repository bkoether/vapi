<?php

class Commission extends \Eloquent
{
    protected $fillable = ['job_id', 'employee_id', 'percentage', 'value'];

    public function employee()
    {
        return $this->belongsTo('Employee');
    }

    public function job()
    {
        return $this->belongsTo('Job');
    }
}