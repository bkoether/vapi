<?php

class ServiceRoute extends \Eloquent
{
    protected $fillable = [
      'employee_id',
      'name'
    ];

    public function employee()
    {
        return $this->belongsTo('Employee');
    }

    public function services()
    {
        return $this->hasMany('Service');
    }

}