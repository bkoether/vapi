<?php

class Invoice extends \Eloquent
{
    protected $fillable = ['employee_id', 'month'];

    public function employee()
    {
        return $this->belongsTo('Employee');
    }


}