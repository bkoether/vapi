<?php

class Estimate extends \Eloquent
{
    protected $fillable = [
      'customer_id',
      'estimate_number',
      'estimate_date',
      'expiration_date',
      'accepted_date',
      'accepted',
      'header_note',
      'data',
      'footer_note',
    ];

    public function getDates()
    {
        return [
          'estimate_date',
          'expiration_date',
          'accepted_date',
          'created_at',
          'updated_at',
          'last_sent_date'
        ];
    }

    public function customer()
    {
        return $this->belongsTo('Customer');
    }


    public function setDataAttribute($value)
    {
        $this->attributes['data'] = serialize($value);
    }

    public function getDataAttribute($value)
    {
        return unserialize($value);

    }

}