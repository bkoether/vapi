<?php

class Customer extends \Eloquent
{
    protected $fillable = [
      'name',
      'contact_name',
      'chain',
      'phone1',
      'phone2',
      'email',
      'billing_address',
      'default_rate',
      'status',
      'description',
      'contact_2_title',
      'contact_2_name',
      'contact_2_email',
      'contact_2_phone'
    ];

    public static function boot()
    {
        parent::boot();

        static::deleting(function ($customer) {
            $customer->jobs()->delete();
            $customer->services()->delete();
        });
    }

    public function services()
    {
        return $this->hasMany('Service');
    }

    public function jobs()
    {
        return $this->hasMany('Job');
    }

    public function estimates()
    {
        return $this->hasMany('Estimate');
    }

    public function getJobAddress()
    {
        if (empty($this->job_address)) {
            return $this->billing_address;
        } else {
            return $this->job_address;
        }
    }


}