<?php
use Codesleeve\Stapler\ORM\EloquentTrait;
use Codesleeve\Stapler\ORM\StaplerableInterface;

class Document extends \Eloquent implements StaplerableInterface
{
    use EloquentTrait;

    protected $fillable = [];

    public function __construct(array $attributes = array())
    {
        $this->hasAttachedFile('file', [
          'url'  => '/:attachment/:id_partition/:filename',
          'path' => ':app_root/www:url'
        ]);

        parent::__construct($attributes);
    }


    public function job()
    {
        return $this->belongsTo('Job');
    }

}