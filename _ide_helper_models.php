<?php
/**
 * An helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace {
/**
 * Customer
 *
 * @property integer $id
 * @property string $name
 * @property integer $store_number
 * @property string $contact_name
 * @property string $chain
 * @property string $phone1
 * @property string $phone2
 * @property string $email
 * @property string $billing_address
 * @property string $job_address
 * @property float $default_rate
 * @property boolean $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Service[] $services
 * @property-read \Illuminate\Database\Eloquent\Collection|\Job[] $jobs
 */
	class Customer {}
}

namespace {
/**
 * Employee
 *
 * @property integer $id
 * @property integer $user_id
 * @property \Carbon\Carbon $birthday
 * @property \Carbon\Carbon $start_date
 * @property \Carbon\Carbon $end_date
 * @property string $sin_number
 * @property string $tax_number
 * @property string $wbc_number
 * @property string $company
 * @property string $phone1
 * @property string $phone2
 * @property string $address
 * @property float $default_rate
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Service[] $services
 * @property-read \Illuminate\Database\Eloquent\Collection|\Job[] $jobs
 * @property-read \User $user
 */
	class Employee {}
}

namespace {
/**
 * Job
 *
 * @property integer $id
 * @property integer $service_id
 * @property integer $employee_id
 * @property integer $customer_id
 * @property string $admin_comment
 * @property string $employee_comment
 * @property boolean $scheduled
 * @property boolean $completed
 * @property boolean $invoiced
 * @property \Carbon\Carbon $due_start
 * @property \Carbon\Carbon $due_end
 * @property \Carbon\Carbon $completed_at
 * @property \Carbon\Carbon $scheduled_at
 * @property \Carbon\Carbon $invoiced_at
 * @property float $customer_rate
 * @property float $employee_rate
 * @property string $description_override
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Service $service
 * @property-read \Customer $customer
 * @property-read \Employee $employee
 * @property-read \Report $report
 * @method static \Job upcoming() 
 * @method static \Job thisUser() 
 */
	class Job {}
}

namespace {
/**
 * LocationImage
 *
 * @property integer $id
 * @property integer $service_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $photo_file_name
 * @property integer $photo_file_size
 * @property string $photo_content_type
 * @property string $photo_updated_at
 * @property-read \Service $service
 */
	class LocationImage {}
}

namespace {
/**
 * Report
 *
 * @property integer $id
 * @property integer $job_id
 * @property \Carbon\Carbon $call_date
 * @property string $name
 * @property string $position
 * @property string $comment
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Job $job
 */
	class Report {}
}

namespace {
/**
 * Service
 *
 * @property integer $id
 * @property integer $customer_id
 * @property integer $employee_id
 * @property string $type
 * @property string $description
 * @property float $customer_rate
 * @property float $employee_rate
 * @property string $invoice_type
 * @property boolean $signed_workorder
 * @property string $frequency
 * @property integer $schedule_ahead
 * @property \Carbon\Carbon $next_schedule
 * @property boolean $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Job[] $jobs
 * @property-read \Employee $employee
 * @property-read \Customer $customer
 * @property-read \Illuminate\Database\Eloquent\Collection|\LocationImage[] $locationImages
 * @method static \Service thisUser() 
 */
	class Service {}
}

namespace {
/**
 * User
 *
 * @property integer $id
 * @property string $first
 * @property string $last
 * @property string $email
 * @property string $password
 * @property string $role
 * @property boolean $status
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Employee $employee
 */
	class User {}
}

